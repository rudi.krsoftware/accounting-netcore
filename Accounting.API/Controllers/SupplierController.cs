/*
***************************************************************************************************************
*  GENEREATE BY			:	K.CODERENDERER
*  CREATED BY			:	kurnia.wirawan@gmail.com
*  CREATED DATE			:	Saturday, April 20, 2019
*  PROJECT				:	MRP-ACCOUNTING
*  PROJECT ID			:	
*  PROJECT TYPE			:	NetCore
*  SOLUTION NAME		:	Accounting.API
***************************************************************************************************************
*  kurnia.wirawan@gmail.com CONFIDENTIAL
*  
*  kurnia.wirawan@gmail.com 2014 All Rights Reserved
*  NOTICE:  All information contained herein is, and remains the property of kurnia.wirawan@gmail.com and its suppliers,
*  if any.  The intellectual and technical concepts contained 
*  herein are proprietary to kurnia.wirawan@gmail.com and its suppliers and may be covered by Indonesia and Foreign Patents
*  patents in process, and are protected by trade secret or copyright law.
*  Dissemination of this information or reproduction of this material
*  is strictly forbidden unless prior written permission is obtained from kurnia.wirawan@gmail.com
***************************************************************************************************************
*/

using System.Threading.Tasks;
using Accounting.API.Common.API;
using KCore.Common.Response;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace Accounting.API.Controllers
{
    [Produces("application/json")]
    [ApiController]
    public class SupplierController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IConfiguration _configuration;
        public SupplierController(IMediator mediator, IConfiguration configuration)
        {
            _mediator = mediator;
            _configuration = configuration;
        }

        [Route("api/accounting/supplier/listpaging")]
        [Authorize]
        [HttpGet]
        public async Task<IActionResult> ListPaging([FromQuery]Features.Supplier.ListPaging.Request request)
        {
            return ActionResultMapper.ToActionResult(await _mediator.Send(request));
        }

        [Route("api/accounting/supplier/list")]
        [Authorize]
        [HttpGet]
        public async Task<IActionResult> List([FromQuery]Features.Supplier.List.Request request)
        {
            return ActionResultMapper.ToActionResult(await _mediator.Send(request));
        }

        [Route("api/accounting/supplier/get")]
        [Authorize]
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery]Features.Supplier.Get.Request request)
        {
            return ActionResultMapper.ToActionResult(await _mediator.Send(request));
        }

        [Route("api/accounting/supplier/submit")]
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Submit([FromBody]Features.Supplier.Submit.Request request)
        {
            return ActionResultMapper.ToActionResult(await _mediator.Send(request));
        }

        [Route("api/accounting/supplier/delete")]
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Delete([FromBody]Features.Supplier.Delete.Request request)
        {
            return ActionResultMapper.ToActionResult(await _mediator.Send(request));
        }
    }
}
    
    
