﻿using Accounting.API.Common.Context;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace Accounting.API.DataContext
{
    public class CustomerShipment : BaseContext
    {
        [Required]
        [JsonProperty(PropertyName = "CustomerId")]
        public Guid CustomerId { get; set; }

        [Required]
        [JsonProperty(PropertyName = "IsDefaultShipment")]
        public bool IsDefaultShipment { get; set; }

        [StringLength(256)]
        [JsonProperty(PropertyName = "Carrier")]
        public string Carrier { get; set; }

        [StringLength(256)]
        [JsonProperty(PropertyName = "Consignee")]
        public string Consignee { get; set; }

        [StringLength(256)]
        [JsonProperty(PropertyName = "PortOfOrigin")]
        public string PortOfOrigin { get; set; }

        [StringLength(256)]
        [JsonProperty(PropertyName = "PortOfDestination")]
        public string PortOfDestination { get; set; }

        [Required]
        [StringLength(128)]
        [JsonProperty(PropertyName = "Country")]
        public string Country { get; set; }

        [Required]
        [StringLength(128)]
        [JsonProperty(PropertyName = "Province")]
        public string Province { get; set; }

        [Required]
        [StringLength(128)]
        [JsonProperty(PropertyName = "City")]
        public string City { get; set; }

        [Required]
        [StringLength(256)]
        [JsonProperty(PropertyName = "Address")]
        public string Address { get; set; }

        [Required]
        [StringLength(24)]
        [JsonProperty(PropertyName = "ZipCode")]
        public string ZipCode { get; set; }

        [StringLength(512)]
        [JsonProperty(PropertyName = "Notes")]
        public string Notes { get; set; }

        public virtual Customer Customer { get; set; }
    }
}
