﻿using Accounting.API.Common.Context;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Accounting.API.DataContext
{
    public class ReportBalanceSheet : BaseContext
    {
        [Required]
        [JsonProperty(PropertyName = "SessionId")]
        public Guid SessionId { get; set; }

        [Required]
        [JsonProperty(PropertyName = "LineNumber")]
        public int LineNumber { get; set; }

        [Required]
        [StringLength(24)]
        [JsonProperty(PropertyName = "AccountCode")]
        public string AccountCode { get; set; }

        [Required]
        [StringLength(64)]
        [JsonProperty(PropertyName = "AccountName")]
        public string AccountName { get; set; }

        [Required]
        [JsonProperty(PropertyName = "AccountLevel")]
        public int AccountLevel { get; set; }

        [Required]
        [JsonProperty(PropertyName = "IsAccountDetail")]
        public bool IsAccountDetail { get; set; }

        [Required]
        [Column(TypeName = "decimal(24, 2)")]
        [JsonProperty(PropertyName = "Debet")]
        public decimal Debet { get; set; }

        [Required]
        [Column(TypeName = "decimal(24, 2)")]
        [JsonProperty(PropertyName = "Credit")]
        public decimal Credit { get; set; }

        [Required]
        [Column(TypeName = "decimal(24, 2)")]
        [JsonProperty(PropertyName = "Saldo")]
        public decimal Saldo { get; set; }
    }
}
