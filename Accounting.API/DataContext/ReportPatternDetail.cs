﻿using Accounting.API.Common.Context;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace Accounting.API.DataContext
{
    public class ReportPatternDetail : BaseContext
    {
        [Required]
        [JsonProperty(PropertyName = "ReportPatternId")]
        public Guid ReportPatternId { get; set; }

        [Required]
        [JsonProperty(PropertyName = "LineNumber")]
        public int LineNumber { get; set; }

        [StringLength(24)]
        [JsonProperty(PropertyName = "AccountCode")]
        public string AccountCode { get; set; }

        [Required]
        [StringLength(64)]
        [JsonProperty(PropertyName = "AccountName")]
        public string AccountName { get; set; }

        [Required]
        [JsonProperty(PropertyName = "AccountLevel")]
        public int AccountLevel { get; set; }

        [Required]
        [JsonProperty(PropertyName = "IsAccountDetail")]
        public bool IsAccountDetail { get; set; }

        [Required]
        [StringLength(64)]
        [JsonProperty(PropertyName = "FunctionValue")]
        public string FunctionValue { get; set; }

        [StringLength(64)]
        [JsonProperty(PropertyName = "FunctionAccount")]
        public string FunctionAccount { get; set; }

        [JsonProperty(PropertyName = "FunctionFormula")]
        public string FunctionFormula { get; set; }

        public virtual ReportPattern ReportPattern { get; set; }
    }
}
