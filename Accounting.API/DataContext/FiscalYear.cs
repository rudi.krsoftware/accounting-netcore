﻿using Accounting.API.Common.Context;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.ComponentModel.DataAnnotations;

namespace Accounting.API.DataContext
{
    public class FiscalYear : BaseContext
    {
        [Required]
        [StringLength(24)]
        [JsonProperty(PropertyName = "FiscalYearCode")]
        public string FiscalYearCode { get; set; }

        [Required]
        [StringLength(64)]
        [JsonProperty(PropertyName = "FiscalYearName")]
        public string FiscalYearName { get; set; }

        [Required]
        [JsonProperty(PropertyName = "StartDate")]
        [JsonConverter(typeof(JavaScriptDateTimeConverter))]
        public DateTime StartDate { get; set; }

        [Required]
        [JsonProperty(PropertyName = "EndDate")]
        [JsonConverter(typeof(JavaScriptDateTimeConverter))]
        public DateTime EndDate { get; set; }

        [Required]
        [JsonProperty(PropertyName = "ClosingType")]
        public short ClosingType { get; set; }

        [StringLength(512)]
        [JsonProperty(PropertyName = "Notes")]
        public string Notes { get; set; }
    }
}
