﻿using Accounting.API.Common.Context;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Accounting.API.DataContext
{
    public class Currency : BaseContext
    {
        [Required]
        [StringLength(24)]
        [JsonProperty(PropertyName = "CurrencyCode")]
        public string CurrencyCode { get; set; }

        [Required]
        [StringLength(64)]
        [JsonProperty(PropertyName = "CurrencyName")]
        public string CurrencyName { get; set; }

        [Required]
        [JsonProperty(PropertyName = "ExchangeDate")]
        [JsonConverter(typeof(JavaScriptDateTimeConverter))]
        public DateTime ExchangeDate { get; set; }

        [Required]
        [Column(TypeName = "decimal(24, 2)")]
        [JsonProperty(PropertyName = "BaseValue")]
        public decimal BaseValue { get; set; }

        [Required]
        [Column(TypeName = "decimal(24, 2)")]
        [JsonProperty(PropertyName = "BaseRate")]
        public decimal BaseRate { get; set; }

        [StringLength(512)]
        [JsonProperty(PropertyName = "Notes")]
        public string Notes { get; set; }

    }
}
