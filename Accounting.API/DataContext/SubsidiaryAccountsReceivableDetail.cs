﻿using Accounting.API.Common.Context;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Accounting.API.DataContext
{
    public class SubsidiaryAccountsReceivableDetail : BaseContext
    {
        [Required]
        [JsonProperty(PropertyName = "SubsidiaryAccountsReceivableId")]
        public Guid SubsidiaryAccountsReceivableId { get; set; }

        [Required]
        [JsonProperty(PropertyName = "TransactionDate")]
        [JsonConverter(typeof(JavaScriptDateTimeConverter))]
        public DateTime TransactionDate { get; set; }

        [Required]
        [StringLength(24)]
        [JsonProperty(PropertyName = "DocumentType")]
        public string DocumentType { get; set; }

        [Required]
        [StringLength(24)]
        [JsonProperty(PropertyName = "DocumentNo")]
        public string DocumentNo { get; set; }

        [Required]
        [StringLength(512)]
        [JsonProperty(PropertyName = "Description")]
        public string Description { get; set; }

        [Required]
        [Column(TypeName = "decimal(24, 2)")]
        [JsonProperty(PropertyName = "Debet")]
        public decimal Debet { get; set; }

        [Required]
        [Column(TypeName = "decimal(24, 2)")]
        [JsonProperty(PropertyName = "Credit")]
        public decimal Credit { get; set; }

        [Required]
        [JsonProperty(PropertyName = "Balance")]
        public decimal Balance { get; set; }

        public virtual SubsidiaryAccountsReceivable SubsidiaryAccountsReceivable { get; set; }
    }
}
