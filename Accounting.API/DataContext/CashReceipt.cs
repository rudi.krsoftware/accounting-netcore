﻿using Accounting.API.Common.Context;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Accounting.API.DataContext
{
    public class CashReceipt : BaseApproveContext
    {
        [Required]
        [StringLength(24)]
        [JsonProperty(PropertyName = "CashReceiptNo")]
        public string CashReceiptNo { get; set; }

        [Required]
        [JsonProperty(PropertyName = "CashReceiptDate")]
        [JsonConverter(typeof(JavaScriptDateTimeConverter))]
        public DateTime CashReceiptDate { get; set; }

        [Required]
        [StringLength(24)]
        [JsonProperty(PropertyName = "CashReceiptStatus")]
        public string CashReceiptStatus { get; set; }

        [StringLength(24)]
        [JsonProperty(PropertyName = "ReceiptDocumentNo")]
        public string ReceiptDocumentNo { get; set; }

        [StringLength(24)]
        [JsonProperty(PropertyName = "ReceiptDocumentType")]
        public string ReceiptDocumentType { get; set; }

        [JsonProperty(PropertyName = "ReceiptDocumentEffectiveDate")]
        [JsonConverter(typeof(JavaScriptDateTimeConverter))]
        public DateTime? ReceiptDocumentEffectiveDate { get; set; }

        [JsonProperty(PropertyName = "ReceiptDocumentExpiredDate")]
        [JsonConverter(typeof(JavaScriptDateTimeConverter))]
        public DateTime? ReceiptDocumentExpiredDate { get; set; }

        [StringLength(128)]
        [JsonProperty(PropertyName = "ReceiptDocumentRemark")]
        public string ReceiptDocumentRemark { get; set; }

        [Required]
        [StringLength(24)]
        [JsonProperty(PropertyName = "PayerType")]
        public string PayerType { get; set; }

        [StringLength(24)]
        [JsonProperty(PropertyName = "PayerCode")]
        public string PayerCode { get; set; }

        [Required]
        [StringLength(64)]
        [JsonProperty(PropertyName = "PayerName")]
        public string PayerName { get; set; }

        [StringLength(24)]
        [JsonProperty(PropertyName = "BankCode")]
        public string BankCode { get; set; }

        [StringLength(64)]
        [JsonProperty(PropertyName = "BankName")]
        public string BankName { get; set; }

        [StringLength(24)]
        [JsonProperty(PropertyName = "RefDocumentType")]
        public string RefDocumentType { get; set; }

        [StringLength(24)]
        [JsonProperty(PropertyName = "RefDocumentNo")]
        public string RefDocumentNo { get; set; }

        [JsonProperty(PropertyName = "RefDocumentDate")]
        [JsonConverter(typeof(JavaScriptDateTimeConverter))]
        public DateTime? RefDocumentDate { get; set; }

        [Required]
        [StringLength(512)]
        [JsonProperty(PropertyName = "Description")]
        public string Description { get; set; }

        [Required]
        [StringLength(24)]
        [JsonProperty(PropertyName = "CurrencyCode")]
        public string CurrencyCode { get; set; }

        [Required]
        [Column(TypeName = "decimal(24, 2)")]
        [JsonProperty(PropertyName = "TotalPayment")]
        public decimal TotalPayment { get; set; }

        [StringLength(512)]
        [JsonProperty(PropertyName = "Notes")]
        public string Notes { get; set; }
    }
}
