﻿using Accounting.API.Common.Context;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Accounting.API.DataContext
{
    public class CashPayment : BaseApproveContext
    {
        [Required]
        [StringLength(24)]
        [JsonProperty(PropertyName = "CashPaymentNo")]
        public string CashPaymentNo { get; set; }

        [Required]
        [JsonProperty(PropertyName = "CashPaymentDate")]
        [JsonConverter(typeof(JavaScriptDateTimeConverter))]
        public DateTime CashPaymentDate { get; set; }

        [Required]
        [StringLength(24)]
        [JsonProperty(PropertyName = "CashPaymentStatus")]
        public string CashPaymentStatus { get; set; }

        [StringLength(24)]
        [JsonProperty(PropertyName = "PaymentDocumentNo")]
        public string PaymentDocumentNo { get; set; }

        [StringLength(24)]
        [JsonProperty(PropertyName = "PaymentDocumentType")]
        public string PaymentDocumentType { get; set; }

        [JsonProperty(PropertyName = "PaymentDocumentEffectiveDate")]
        [JsonConverter(typeof(JavaScriptDateTimeConverter))]
        public DateTime? PaymentDocumentEffectiveDate { get; set; }

        [JsonProperty(PropertyName = "PaymentDocumentExpiredDate")]
        [JsonConverter(typeof(JavaScriptDateTimeConverter))]
        public DateTime? PaymentDocumentExpiredDate { get; set; }

        [StringLength(128)]
        [JsonProperty(PropertyName = "PaymentDocumentRemark")]
        public string PaymentDocumentRemark { get; set; }

        [Required]
        [StringLength(24)]
        [JsonProperty(PropertyName = "ReceiverType")]
        public string ReceiverType { get; set; }

        [StringLength(24)]
        [JsonProperty(PropertyName = "ReceiverCode")]
        public string ReceiverCode { get; set; }

        [Required]
        [StringLength(64)]
        [JsonProperty(PropertyName = "ReceiverName")]
        public string ReceiverName { get; set; }

        [StringLength(24)]
        [JsonProperty(PropertyName = "BankCode")]
        public string BankCode { get; set; }

        [StringLength(64)]
        [JsonProperty(PropertyName = "BankName")]
        public string BankName { get; set; }

        [StringLength(24)]
        [JsonProperty(PropertyName = "RefDocumentType")]
        public string RefDocumentType { get; set; }

        [StringLength(24)]
        [JsonProperty(PropertyName = "RefDocumentNo")]
        public string RefDocumentNo { get; set; }

        [JsonProperty(PropertyName = "RefDocumentDate")]
        [JsonConverter(typeof(JavaScriptDateTimeConverter))]
        public DateTime? RefDocumentDate { get; set; }

        [Required]
        [StringLength(512)]
        [JsonProperty(PropertyName = "Description")]
        public string Description { get; set; }

        [Required]
        [StringLength(24)]
        [JsonProperty(PropertyName = "CurrencyCode")]
        public string CurrencyCode { get; set; }

        [Required]
        [Column(TypeName = "decimal(24, 2)")]
        [JsonProperty(PropertyName = "TotalPayment")]
        public decimal TotalPayment { get; set; }

        [StringLength(512)]
        [JsonProperty(PropertyName = "Notes")]
        public string Notes { get; set; }
    }
}
