﻿using Microsoft.EntityFrameworkCore;

namespace Accounting.API.DataContext
{
    public class AccountingDataContext : DbContext
    {
        public AccountingDataContext(DbContextOptions<AccountingDataContext> options) : base(options)
        {

        }

        public virtual DbSet<AccountType> AccountTypes { get; set; }
        public virtual DbSet<Bank> Banks { get; set; }
        public virtual DbSet<BankAccount> BankAccounts { get; set; }
        public virtual DbSet<CashPayment> CashPayments { get; set; }
        public virtual DbSet<CategoryAccount> CategoryAccounts { get; set; }
        public virtual DbSet<CashReceipt> CashReceipts { get; set; }
        public virtual DbSet<CategoryCustomer> CategoryCustomers { get; set; }
        public virtual DbSet<CategorySupplier> CategorySuppliers { get; set; }
        public virtual DbSet<ChartOfAccount> ChartOfAccounts { get; set; }
        public virtual DbSet<Currency> Currencys { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<CustomerShipment> CustomerShipments { get; set; }
        public virtual DbSet<FiscalYear> FiscalYears { get; set; }
        public virtual DbSet<GeneralLedger> GeneralLedgers { get; set; }
        public virtual DbSet<GeneralLedgerDetail> GeneralLedgerDetails { get; set; }
        public virtual DbSet<ReportBalanceSheet> ReportBalanceSheets { get; set; }
        public virtual DbSet<ReportIncomeStatement> ReportIncomeStatements { get; set; }
        public virtual DbSet<ReportPattern> ReportPatterns { get; set; }
        public virtual DbSet<ReportPatternDetail> ReportPatternDetails { get; set; }
        public virtual DbSet<SubsidiaryAccountsPayable> SubsidiaryAccountsPayables { get; set; }
        public virtual DbSet<SubsidiaryAccountsPayableDetail> SubsidiaryAccountsPayableDetails { get; set; }
        public virtual DbSet<SubsidiaryAccountsReceivable> SubsidiaryAccountsReceivables { get; set; }
        public virtual DbSet<SubsidiaryAccountsReceivableDetail> SubsidiaryAccountsReceivableDetails { get; set; }
        public virtual DbSet<Supplier> Suppliers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseNpgsql("Server=localhost;Port=5432;Database=MRP_ACCOUNTING;User Id=postgres;Password=getdown;");
            }
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<CategoryAccount>(entity => { entity.HasIndex(e => e.AccountCategoryCode).IsUnique(); });
            builder.Entity<AccountType>(entity => { entity.HasIndex(e => e.AccountTypeCode).IsUnique(); });
            builder.Entity<Bank>(entity => { entity.HasIndex(e => e.BankCode).IsUnique(); });
            builder.Entity<BankAccount>(entity => { entity.HasIndex(e => e.AccountNumber).IsUnique(); });
            builder.Entity<CashPayment>(entity => { entity.HasIndex(e => e.CashPaymentNo).IsUnique(); });
            builder.Entity<CashReceipt>(entity => { entity.HasIndex(e => e.CashReceiptNo).IsUnique(); });
            builder.Entity<CategoryCustomer>(entity => { entity.HasIndex(e => e.CategoryName).IsUnique(); });
            builder.Entity<CategorySupplier>(entity => { entity.HasIndex(e => e.CategoryName).IsUnique(); });
            builder.Entity<ChartOfAccount>(entity => { entity.HasIndex(e => e.AccountCode).IsUnique(); });
            builder.Entity<Currency>(entity => { entity.HasIndex(e => e.CurrencyCode).IsUnique(); });
            builder.Entity<Customer>(entity => { entity.HasIndex(e => e.CustomerCode).IsUnique(); });
            builder.Entity<FiscalYear>(entity => { entity.HasIndex(e => e.FiscalYearCode).IsUnique(); });
            builder.Entity<GeneralLedger>(entity => { entity.HasIndex(e => e.GeneralLedgerNo).IsUnique(); });
            builder.Entity<ReportPattern>(entity => { entity.HasIndex(e => e.ReportName).IsUnique(); });
            builder.Entity<Supplier>(entity => { entity.HasIndex(e => e.SupplierCode).IsUnique(); });
        }
    }
}
