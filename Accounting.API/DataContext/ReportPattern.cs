﻿using Accounting.API.Common.Context;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Accounting.API.DataContext
{
    public class ReportPattern : BaseContext
    {
        public ReportPattern()
        {
            ReportPatternDetails = new HashSet<ReportPatternDetail>();
        }

        public ICollection<ReportPatternDetail> ReportPatternDetails { get; set; }

        [Required]
        [StringLength(64)]
        [JsonProperty(PropertyName = "ReportName")]
        public string ReportName { get; set; }

        [StringLength(512)]
        [JsonProperty(PropertyName = "Notes")]
        public string Notes { get; set; }
    }
}
