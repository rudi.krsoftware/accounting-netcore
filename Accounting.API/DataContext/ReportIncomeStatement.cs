﻿using Accounting.API.Common.Context;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Accounting.API.DataContext
{
    public class ReportIncomeStatement : BaseContext
    {
        [Required]
        [JsonProperty(PropertyName = "SessionId")]
        public Guid SessionId { get; set; }

        [Required]
        [JsonProperty(PropertyName = "LineNumber")]
        public int LineNumber { get; set; }

        [Required]
        [JsonProperty(PropertyName = "LineLevel")]
        public int LineLevel { get; set; }

        [Required]
        [StringLength(128)]
        [JsonProperty(PropertyName = "Description")]
        public string Description { get; set; }

        [Required]
        [JsonProperty(PropertyName = "IsDetail")]
        public bool IsDetail { get; set; }

        [Required]
        [Column(TypeName = "decimal(24, 2)")]
        [JsonProperty(PropertyName = "Saldo")]
        public decimal Saldo { get; set; }
    }
}
