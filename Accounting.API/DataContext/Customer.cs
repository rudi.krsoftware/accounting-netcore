﻿using Accounting.API.Common.Context;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Accounting.API.DataContext
{
    public class Customer : BaseContext
    {
        public Customer()
        {
            CustomerShipments = new HashSet<CustomerShipment>();
        }

        public ICollection<CustomerShipment> CustomerShipments { get; set; }

        [Required]
        [JsonProperty(PropertyName = "CategoryCustomerId")]
        public Guid CategoryCustomerId { get; set; }

        [Required]
        [StringLength(24)]
        [JsonProperty(PropertyName = "CustomerCode")]
        public string CustomerCode { get; set; }

        [Required]
        [StringLength(128)]
        [JsonProperty(PropertyName = "CustomerName")]
        public string CustomerName { get; set; }

        [Required]
        [JsonProperty(PropertyName = "MinimumDownPayment")]
        public double MinimumDownPayment { get; set; }

        [Required]
        [StringLength(128)]
        [JsonProperty(PropertyName = "Country")]
        public string Country { get; set; }

        [Required]
        [StringLength(128)]
        [JsonProperty(PropertyName = "Province")]
        public string Province { get; set; }

        [Required]
        [StringLength(128)]
        [JsonProperty(PropertyName = "City")]
        public string City { get; set; }

        [Required]
        [StringLength(256)]
        [JsonProperty(PropertyName = "Address")]
        public string Address { get; set; }

        [Required]
        [StringLength(24)]
        [JsonProperty(PropertyName = "ZipCode")]
        public string ZipCode { get; set; }

        [Required]
        [StringLength(24)]
        [JsonProperty(PropertyName = "Phone")]
        public string Phone { get; set; }

        [StringLength(24)]
        [JsonProperty(PropertyName = "Phone2")]
        public string Phone2 { get; set; }

        [StringLength(64)]
        [JsonProperty(PropertyName = "Fax")]
        public string Fax { get; set; }

        [Required]
        [StringLength(256)]
        [DataType(DataType.EmailAddress)]
        [JsonProperty(PropertyName = "Email")]
        public string Email { get; set; }

        [StringLength(256)]
        [JsonProperty(PropertyName = "WebAddress")]
        public string WebAddress { get; set; }

        [StringLength(64)]
        [JsonProperty(PropertyName = "ContactPersonName")]
        public string ContactPersonName { get; set; }

        [StringLength(24)]
        [JsonProperty(PropertyName = "ContactPersonPhone")]
        public string ContactPersonPhone { get; set; }

        [Required]
        [JsonProperty(PropertyName = "IsActive")]
        public bool IsActive { get; set; }

        [StringLength(512)]
        [JsonProperty(PropertyName = "Notes")]
        public string Notes { get; set; }

        public virtual CategoryCustomer CategoryCustomer { get; set; }
    }
}
