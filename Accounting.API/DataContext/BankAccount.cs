﻿using Accounting.API.Common.Context;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace Accounting.API.DataContext
{
    public class BankAccount : BaseContext
    {
        [Required]
        [JsonProperty(PropertyName = "BankId")]
        public Guid BankId { get; set; }

        [Required]
        [StringLength(64)]
        [JsonProperty(PropertyName = "AccountNumber")]
        public string AccountNumber { get; set; }

        [Required]
        [StringLength(128)]
        [JsonProperty(PropertyName = "AccountName")]
        public string AccountName { get; set; }

        [Required]
        [JsonProperty(PropertyName = "IsActive")]
        public bool IsActive { get; set; }

        [StringLength(512)]
        [JsonProperty(PropertyName = "Notes")]
        public string Notes { get; set; }

        public virtual Bank Bank { get; set; }
    }
}
