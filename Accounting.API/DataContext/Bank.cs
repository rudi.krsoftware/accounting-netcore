﻿using Accounting.API.Common.Context;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Accounting.API.DataContext
{
    public class Bank : BaseContext
    {
        public Bank()
        {
            BankAccounts = new HashSet<BankAccount>();
        }

        public ICollection<BankAccount> BankAccounts { get; set; }

        [Required]
        [StringLength(24)]
        [JsonProperty(PropertyName = "BankCode")]
        public string BankCode { get; set; }

        [Required]
        [StringLength(64)]
        [JsonProperty(PropertyName = "BankName")]
        public string BankName { get; set; }

        [Required]
        [StringLength(128)]
        [JsonProperty(PropertyName = "Country")]
        public string Country { get; set; }

        [Required]
        [StringLength(128)]
        [JsonProperty(PropertyName = "Province")]
        public string Province { get; set; }

        [Required]
        [StringLength(128)]
        [JsonProperty(PropertyName = "City")]
        public string City { get; set; }

        [Required]
        [StringLength(256)]
        [JsonProperty(PropertyName = "Address")]
        public string Address { get; set; }

        [Required]
        [StringLength(24)]
        [JsonProperty(PropertyName = "ZipCode")]
        public string ZipCode { get; set; }

        [Required]
        [StringLength(24)]
        [JsonProperty(PropertyName = "Phone")]
        public string Phone { get; set; }

        [StringLength(24)]
        [JsonProperty(PropertyName = "Phone2")]
        public string Phone2 { get; set; }

        [StringLength(64)]
        [JsonProperty(PropertyName = "Fax")]
        public string Fax { get; set; }

        [Required]
        [StringLength(256)]
        [DataType(DataType.EmailAddress)]
        [JsonProperty(PropertyName = "Email")]
        public string Email { get; set; }

        [StringLength(256)]
        [JsonProperty(PropertyName = "WebAddress")]
        public string WebAddress { get; set; }

        [StringLength(64)]
        [JsonProperty(PropertyName = "ContactPersonName")]
        public string ContactPersonName { get; set; }

        [StringLength(24)]
        [JsonProperty(PropertyName = "ContactPersonPhone")]
        public string ContactPersonPhone { get; set; }
    }
}
