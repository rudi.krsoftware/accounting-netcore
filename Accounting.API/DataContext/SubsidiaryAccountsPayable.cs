﻿using Accounting.API.Common.Context;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Accounting.API.DataContext
{
    public class SubsidiaryAccountsPayable : BaseContext
    {
        public SubsidiaryAccountsPayable()
        {
            SubsidiaryAccountsPayableDetails = new HashSet<SubsidiaryAccountsPayableDetail>();
        }

        public ICollection<SubsidiaryAccountsPayableDetail> SubsidiaryAccountsPayableDetails { get; set; }

        [Required]
        [StringLength(24)]
        [JsonProperty(PropertyName = "SupplierCode")]
        public string SupplierCode { get; set; }

        [Required]
        [StringLength(128)]
        [JsonProperty(PropertyName = "SupplierName")]
        public string SupplierName { get; set; }
    }
}
