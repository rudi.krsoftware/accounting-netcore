﻿using Accounting.API.Common.Context;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Accounting.API.DataContext
{
    public class Supplier : BaseContext
    {
        [Required]
        [JsonProperty(PropertyName = "CategorySupplierId")]
        public Guid CategorySupplierId { get; set; }

        [Required]
        [StringLength(24)]
        [JsonProperty(PropertyName = "SupplierCode")]
        public string SupplierCode { get; set; }

        [Required]
        [StringLength(128)]
        [JsonProperty(PropertyName = "SupplierName")]
        public string SupplierName { get; set; }

        [Required]
        [StringLength(128)]
        [JsonProperty(PropertyName = "Country")]
        public string Country { get; set; }

        [Required]
        [StringLength(128)]
        [JsonProperty(PropertyName = "Province")]
        public string Province { get; set; }

        [Required]
        [StringLength(128)]
        [JsonProperty(PropertyName = "City")]
        public string City { get; set; }

        [Required]
        [StringLength(256)]
        [JsonProperty(PropertyName = "Address")]
        public string Address { get; set; }

        [Required]
        [StringLength(24)]
        [JsonProperty(PropertyName = "ZipCode")]
        public string ZipCode { get; set; }

        [Required]
        [StringLength(24)]
        [JsonProperty(PropertyName = "Phone")]
        public string Phone { get; set; }

        [StringLength(24)]
        [JsonProperty(PropertyName = "Phone2")]
        public string Phone2 { get; set; }

        [StringLength(64)]
        [JsonProperty(PropertyName = "Fax")]
        public string Fax { get; set; }

        [Required]
        [StringLength(256)]
        [DataType(DataType.EmailAddress)]
        [JsonProperty(PropertyName = "Email")]
        public string Email { get; set; }

        [StringLength(256)]
        [JsonProperty(PropertyName = "WebAddress")]
        public string WebAddress { get; set; }

        [StringLength(64)]
        [JsonProperty(PropertyName = "ContactPersonName")]
        public string ContactPersonName { get; set; }

        [StringLength(24)]
        [JsonProperty(PropertyName = "ContactPersonPhone")]
        public string ContactPersonPhone { get; set; }

        [Required]
        [StringLength(24)]
        [JsonProperty(PropertyName = "DefaultCurrency")]
        public string DefaultCurrency { get; set; }

        [Required]
        [StringLength(24)]
        [JsonProperty(PropertyName = "DefaultPaymentType")]
        public string DefaultPaymentType { get; set; }

        [Column(TypeName = "decimal(24, 2)")]
        [JsonProperty(PropertyName = "DefaultVAT")]
        public decimal DefaultVAT { get; set; }

        [Column(TypeName = "decimal(24, 2)")]
        [JsonProperty(PropertyName = "DefaultOtherTax")]
        public decimal DefaultOtherTax { get; set; }

        [StringLength(128)]
        [JsonProperty(PropertyName = "DefaultOtherTaxRemark")]
        public string DefaultOtherTaxRemark { get; set; }

        [Column(TypeName = "decimal(24, 2)")]
        [JsonProperty(PropertyName = "DefaultFreightCost")]
        public decimal DefaultFreightCost { get; set; }

        [Column(TypeName = "decimal(24, 2)")]
        [JsonProperty(PropertyName = "DefaultOtherCost")]
        public decimal DefaultOtherCost { get; set; }

        [StringLength(128)]
        [JsonProperty(PropertyName = "DefaultOtherCostRemark")]
        public string DefaultOtherCostRemark { get; set; }

        [Required]
        [JsonProperty(PropertyName = "IsActive")]
        public bool IsActive { get; set; }

        [StringLength(512)]
        [JsonProperty(PropertyName = "Notes")]
        public string Notes { get; set; }

        public virtual CategorySupplier CategorySupplier { get; set; }
    }
}
