﻿using Accounting.API.Common.Context;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Accounting.API.DataContext
{
    public class SubsidiaryAccountsReceivable : BaseContext
    {
        public SubsidiaryAccountsReceivable()
        {
            SubsidiaryAccountsReceivableDetails = new HashSet<SubsidiaryAccountsReceivableDetail>();
        }

        public ICollection<SubsidiaryAccountsReceivableDetail> SubsidiaryAccountsReceivableDetails { get; set; }

        [Required]
        [StringLength(24)]
        [JsonProperty(PropertyName = "CustomerCode")]
        public string CustomerCode { get; set; }

        [Required]
        [StringLength(128)]
        [JsonProperty(PropertyName = "CustomerName")]
        public string CustomerName { get; set; }
    }
}
