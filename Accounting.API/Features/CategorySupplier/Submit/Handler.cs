/*
***************************************************************************************************************
*  GENEREATE BY			:	K.CODERENDERER
*  CREATED BY			:	kurnia.wirawan@gmail.com
*  CREATED DATE			:	Saturday, April 20, 2019
*  PROJECT				:	MRP-ACCOUNTING
*  PROJECT ID			:	
*  PROJECT TYPE			:	NetCore
*  SOLUTION NAME		:	Accounting.API
***************************************************************************************************************
*  kurnia.wirawan@gmail.com CONFIDENTIAL
*  
*  kurnia.wirawan@gmail.com 2014 All Rights Reserved
*  NOTICE:  All information contained herein is, and remains the property of kurnia.wirawan@gmail.com and its suppliers,
*  if any.  The intellectual and technical concepts contained 
*  herein are proprietary to kurnia.wirawan@gmail.com and its suppliers and may be covered by Indonesia and Foreign Patents
*  patents in process, and are protected by trade secret or copyright law.
*  Dissemination of this information or reproduction of this material
*  is strictly forbidden unless prior written permission is obtained from kurnia.wirawan@gmail.com
***************************************************************************************************************
*/

using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;
using Accounting.API.Common.API;
using Accounting.API.DataContext;
using KCore.Common.Response;

namespace Accounting.API.Features.CategorySupplier.Submit
{
    public class Handler : IRequestHandler<Request, ApiResult<Response>>
    {
        private readonly AccountingDataContext _dbContext;
        public Handler(AccountingDataContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        public async Task<ApiResult<Response>> Handle(Request request, CancellationToken cancellationToken)
        {
            try
            {
                using (var scope = _dbContext.Database.BeginTransaction())
                {   
                     var id = Guid.NewGuid();
                    if (!string.IsNullOrEmpty(request.Id))
                    {
                        id = new Guid(request.Id);
                    }
                    var model = await _dbContext.CategorySuppliers.FirstOrDefaultAsync(c => c.Id == id && c.RowStatus == 0);
                    if (model != null)
                    {
                        
						model.ModifiedBy = request.User;
						model.ModifiedDate = DateTime.Now;
						model.CategoryName = request.CategoryName;
						model.Priority = request.Priority;
						model.Notes = request.Notes;
                        _dbContext.CategorySuppliers.Update(model);
                        await _dbContext.SaveChangesAsync();
                    }
                    else
                    {
                        var newModel = new DataContext.CategorySupplier
                        {
                            Id = id,
                            RowStatus = 0,
                            RowVersion = Convert.FromBase64String("AAAAAAAElAs="),
                            CreatedBy = request.User,
                            CreatedDate = DateTime.Now,
                            ModifiedBy = request.User,
                            ModifiedDate = DateTime.Now,
							CategoryName = request.CategoryName,
							Priority = request.Priority,
							Notes = request.Notes,
                        };
                        _dbContext.CategorySuppliers.Add(newModel);
                        await _dbContext.SaveChangesAsync();
                    }
                        
                    scope.Commit();

                    return ApiResult<Response>.Ok(new Response
                    {
                        msg = "Submit Category Supplier Success",
                        success = true,
                        result = true,
                    });
                }
            }
            catch (Exception ex)
            {
                return ApiResult<Response>.ErrorDuringProcessing(ex.Message);
            }
        }
    }
}
