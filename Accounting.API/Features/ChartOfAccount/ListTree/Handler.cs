/*
***************************************************************************************************************
*  GENEREATE BY			:	K.CODERENDERER
*  CREATED BY			:	kurnia.wirawan@gmail.com
*  CREATED DATE			:	Monday, April 15, 2019
*  PROJECT				:	MRP-ACCOUNTING
*  PROJECT ID			:	
*  PROJECT TYPE			:	NetCore
*  SOLUTION NAME		:	Accounting.API
***************************************************************************************************************
*  kurnia.wirawan@gmail.com CONFIDENTIAL
*  
*  kurnia.wirawan@gmail.com 2014 All Rights Reserved
*  NOTICE:  All information contained herein is, and remains the property of kurnia.wirawan@gmail.com and its suppliers,
*  if any.  The intellectual and technical concepts contained 
*  herein are proprietary to kurnia.wirawan@gmail.com and its suppliers and may be covered by Indonesia and Foreign Patents
*  patents in process, and are protected by trade secret or copyright law.
*  Dissemination of this information or reproduction of this material
*  is strictly forbidden unless prior written permission is obtained from kurnia.wirawan@gmail.com
***************************************************************************************************************
*/

using MediatR;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading;
using System.Threading.Tasks;
using Accounting.API.Common.Manager;
using Accounting.API.Common.Parameter;
using Accounting.API.Common.API;
using Accounting.API.DataContext;
using Accounting.API.Models;
using Accounting.API.Common.Data;
using KCore.Common.Response;

namespace Accounting.API.Features.ChartOfAccount.ListTree
{
    public class Handler : BaseDataManager, IRequestHandler<Request, ApiResult<Response>>
    {
        private readonly AccountingDataContext _dbContext;
        public Handler(AccountingDataContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<ApiResult<Response>> Handle(Request request, CancellationToken cancellationToken)
        {
            var whereTermList = ListParameter.GetListParameter(request.Params);
            var whereterm = GetQueryParameterLinqV02(whereTermList.ToArray());


            List<ChartOfAccountTreeModel> result = new List<ChartOfAccountTreeModel>();

            var chartOfAccountModels = await _dbContext.ChartOfAccounts.Select(a => new ChartOfAccountModel
            {
                Id = a.Id,
                RowStatus = a.RowStatus,
                RowVersion = a.RowVersion,
                CreatedBy = a.CreatedBy,
                CreatedDate = a.CreatedDate,
                ModifiedBy = a.ModifiedBy,
                ModifiedDate = a.ModifiedDate,
                SchemaCode = a.SchemaCode,
                ParentId = a.ParentId,
                IsAccountDetail = a.IsAccountDetail,
                CategoryAccountId = a.CategoryAccountId,
                AccountCategory = a.CategoryAccount.AccountCategoryName,
                AccountTypeId = a.AccountTypeId,
                AccountType = a.AccountType.AccountTypeName,
                AccountCode = a.AccountCode,
                AccountName = a.AccountName,
                AccountLevel = a.AccountLevel,
                DefaultValue = a.DefaultValue,
                Notes = a.Notes,
            }).Where(c => c.AccountLevel == 0 && c.RowStatus == 0).ToListAsync();
            foreach (var chartOfAccountModel in chartOfAccountModels)
            {
                var chartOfAccountTreeModel = new ChartOfAccountTreeModel
                {
                    Id = chartOfAccountModel.Id,
                    Text = "ROOT",
                    Level = chartOfAccountModel.AccountLevel,
                    CategoryAccountId = chartOfAccountModel.CategoryAccountId,
                    AccountCategory = chartOfAccountModel.AccountCategory,
                    AccountTypeId = chartOfAccountModel.AccountTypeId,
                    AccountType = chartOfAccountModel.AccountType,
                    IsLeaf = chartOfAccountModel.IsAccountDetail,
                    AccountLevel = chartOfAccountModel.AccountLevel,
                    DefaultValue = chartOfAccountModel.DefaultValue,
                    CreatedBy = chartOfAccountModel.CreatedBy,
                    CreatedDate = chartOfAccountModel.CreatedDate,
                    ModifiedBy = chartOfAccountModel.ModifiedBy,
                    ModifiedDate = chartOfAccountModel.ModifiedDate
                };
                var models = await _dbContext.ChartOfAccounts.Select(a => new ChartOfAccountModel
                {
                    Id = a.Id,
                    RowStatus = a.RowStatus,
                    RowVersion = a.RowVersion,
                    CreatedBy = a.CreatedBy,
                    CreatedDate = a.CreatedDate,
                    ModifiedBy = a.ModifiedBy,
                    ModifiedDate = a.ModifiedDate,
                    SchemaCode = a.SchemaCode,
                    ParentId = a.ParentId,
                    IsAccountDetail = a.IsAccountDetail,
                    CategoryAccountId = a.CategoryAccountId,
                    AccountCategory = a.CategoryAccount.AccountCategoryName,
                    AccountTypeId = a.AccountTypeId,
                    AccountType = a.AccountType.AccountTypeName,
                    AccountCode = a.AccountCode,
                    AccountName = a.AccountName,
                    AccountLevel = a.AccountLevel,
                    DefaultValue = a.DefaultValue,
                    Notes = a.Notes,
                }).Where(c => c.ParentId == chartOfAccountTreeModel.Id && c.RowStatus == 0).OrderBy(c => c.AccountCode).ToListAsync();
                if (models.Count > 0)
                    await PopulateChildAccount(_dbContext, chartOfAccountTreeModel, 1, models);
                result.Add(chartOfAccountTreeModel);
            }

            return ApiResult<Response>.Ok(new Response
            {
                ListChartOfAccount = result,
                msg = "Success",
                success = true,
                result = true,
            });
        }

        private async Task PopulateChildAccount(AccountingDataContext dbContext, ChartOfAccountTreeModel parent, int level, List<ChartOfAccountModel> childersModels)
        {
            foreach (var chartOfAccountModel in childersModels)
            {
                var tree = ChartOfAccountTreeModel.New(chartOfAccountModel);
                var models = await dbContext.ChartOfAccounts.Select(a => new ChartOfAccountModel
                {
                    Id = a.Id,
                    RowStatus = a.RowStatus,
                    RowVersion = a.RowVersion,
                    CreatedBy = a.CreatedBy,
                    CreatedDate = a.CreatedDate,
                    ModifiedBy = a.ModifiedBy,
                    ModifiedDate = a.ModifiedDate,
                    SchemaCode = a.SchemaCode,
                    ParentId = a.ParentId,
                    IsAccountDetail = a.IsAccountDetail,
                    CategoryAccountId = a.CategoryAccountId,
                    AccountCategory = a.CategoryAccount.AccountCategoryName,
                    AccountTypeId = a.AccountTypeId,
                    AccountType = a.AccountType.AccountTypeName,
                    AccountCode = a.AccountCode,
                    AccountName = a.AccountName,
                    AccountLevel = a.AccountLevel,
                    DefaultValue = a.DefaultValue,
                    Notes = a.Notes,
                }).Where(c => c.ParentId == chartOfAccountModel.Id && c.RowStatus == 0).OrderBy(c => c.AccountCode).ToListAsync();
                if (models.Count > 0)
                    await PopulateChildAccount(dbContext, tree, level + 1, models);
                parent.Children.Add(tree);
            }
        }
    }
}
