/*
***************************************************************************************************************
*  GENEREATE BY			:	K.CODERENDERER
*  CREATED BY			:	kurnia.wirawan@gmail.com
*  CREATED DATE			:	Monday, April 15, 2019
*  PROJECT				:	MRP-ACCOUNTING
*  PROJECT ID			:	
*  PROJECT TYPE			:	NetCore
*  SOLUTION NAME		:	Accounting.API
***************************************************************************************************************
*  kurnia.wirawan@gmail.com CONFIDENTIAL
*  
*  kurnia.wirawan@gmail.com 2014 All Rights Reserved
*  NOTICE:  All information contained herein is, and remains the property of kurnia.wirawan@gmail.com and its suppliers,
*  if any.  The intellectual and technical concepts contained 
*  herein are proprietary to kurnia.wirawan@gmail.com and its suppliers and may be covered by Indonesia and Foreign Patents
*  patents in process, and are protected by trade secret or copyright law.
*  Dissemination of this information or reproduction of this material
*  is strictly forbidden unless prior written permission is obtained from kurnia.wirawan@gmail.com
***************************************************************************************************************
*/

using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;
using Accounting.API.Common.API;
using Accounting.API.DataContext;
using KCore.Common.Response;

namespace Accounting.API.Features.ChartOfAccount.Submit
{
    public class Handler : IRequestHandler<Request, ApiResult<Response>>
    {
        private readonly AccountingDataContext _dbContext;
        public Handler(AccountingDataContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        public async Task<ApiResult<Response>> Handle(Request request, CancellationToken cancellationToken)
        {
            try
            {
                using (var scope = _dbContext.Database.BeginTransaction())
                {   
                     var id = Guid.NewGuid();
                    if (!string.IsNullOrEmpty(request.Id))
                    {
                        id = new Guid(request.Id);
                    }
                    var model = await _dbContext.ChartOfAccounts.FirstOrDefaultAsync(c => c.Id == id && c.RowStatus == 0);
                    if (model != null)
                    {
                        
						model.ModifiedBy = request.User;
						model.ModifiedDate = DateTime.Now;
						model.SchemaCode = request.SchemaCode;
						model.ParentId = request.ParentId;
						model.IsAccountDetail = request.IsAccountDetail;
						model.AccountTypeId = request.AccountTypeId;
						model.AccountCode = request.AccountCode;
						model.AccountName = request.AccountName;
						model.AccountLevel = request.AccountLevel;
						model.DefaultValue = request.DefaultValue;
						model.Notes = request.Notes;
						model.CategoryAccountId = request.CategoryAccountId;
                        _dbContext.ChartOfAccounts.Update(model);
                        await _dbContext.SaveChangesAsync();
                    }
                    else
                    {
                        var newModel = new DataContext.ChartOfAccount
                        {
                            Id = id,
                            RowStatus = 0,
                            RowVersion = Convert.FromBase64String("AAAAAAAElAs="),
                            CreatedBy = request.User,
                            CreatedDate = DateTime.Now,
                            ModifiedBy = request.User,
                            ModifiedDate = DateTime.Now,
							SchemaCode = request.SchemaCode,
							ParentId = request.ParentId,
							IsAccountDetail = request.IsAccountDetail,
							AccountTypeId = request.AccountTypeId,
							AccountCode = request.AccountCode,
							AccountName = request.AccountName,
							AccountLevel = request.AccountLevel,
							DefaultValue = request.DefaultValue,
							Notes = request.Notes,
							CategoryAccountId = request.CategoryAccountId,
                        };
                        _dbContext.ChartOfAccounts.Add(newModel);
                        await _dbContext.SaveChangesAsync();
                    }

                    await PopulateAccountParent(request.ParentId, request.User, _dbContext);
                    scope.Commit();

                    return ApiResult<Response>.Ok(new Response
                    {
                        msg = "Submit Chart Of Account Success",
                        success = true,
                        result = true,
                    });
                }
            }
            catch (Exception ex)
            {
                return ApiResult<Response>.ErrorDuringProcessing(ex.Message);
            }
        }

        private async Task PopulateAccountParent(Guid? parentId, string user, AccountingDataContext dataContext)
        {
            if (parentId != null)
            {
                var accountParentModel = await dataContext.ChartOfAccounts.FirstOrDefaultAsync(c=>c.Id == parentId && c.RowStatus == 0);
                if (accountParentModel == null)
                {
                    throw new Exception("Data Parent Chart Of Account not found!");
                }
                accountParentModel.IsAccountDetail = false;
                accountParentModel.ModifiedBy = user;
                accountParentModel.ModifiedDate = DateTime.Now;
                _dbContext.ChartOfAccounts.Update(accountParentModel);
                await _dbContext.SaveChangesAsync();
            }
        }
    }
}
