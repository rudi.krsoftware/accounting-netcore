/*
***************************************************************************************************************
*  GENEREATE BY			:	K.CODERENDERER
*  CREATED BY			:	kurnia.wirawan@gmail.com
*  CREATED DATE			:	Monday, April 15, 2019
*  PROJECT				:	MRP-ACCOUNTING
*  PROJECT ID			:	
*  PROJECT TYPE			:	NetCore
*  SOLUTION NAME		:	Accounting.API
***************************************************************************************************************
*  kurnia.wirawan@gmail.com CONFIDENTIAL
*  
*  kurnia.wirawan@gmail.com 2014 All Rights Reserved
*  NOTICE:  All information contained herein is, and remains the property of kurnia.wirawan@gmail.com and its suppliers,
*  if any.  The intellectual and technical concepts contained 
*  herein are proprietary to kurnia.wirawan@gmail.com and its suppliers and may be covered by Indonesia and Foreign Patents
*  patents in process, and are protected by trade secret or copyright law.
*  Dissemination of this information or reproduction of this material
*  is strictly forbidden unless prior written permission is obtained from kurnia.wirawan@gmail.com
***************************************************************************************************************
*/

using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Accounting.API.Common.API;
using Accounting.API.DataContext;
using Accounting.API.Models;
using KCore.Common.Response;

namespace Accounting.API.Features.Customer.Submit
{
    public class Handler : IRequestHandler<Request, ApiResult<Response>>
    {
        private readonly AccountingDataContext _dbContext;
        public Handler(AccountingDataContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        public async Task<ApiResult<Response>> Handle(Request request, CancellationToken cancellationToken)
        {
            try
            {
                using (var scope = _dbContext.Database.BeginTransaction())
                {   
                    var id = Guid.NewGuid();
                    if (!string.IsNullOrEmpty(request.Id))
                    {
                        id = new Guid(request.Id);
                    }
                    var model = await _dbContext.Customers.FirstOrDefaultAsync(c => c.Id == id && c.RowStatus == 0);
                    if (model != null)
                    {
                        
						model.ModifiedBy = request.User;
						model.ModifiedDate = DateTime.Now;
						model.CategoryCustomerId = request.CategoryCustomerId;
						//model.CustomerCode = request.CustomerCode;
						model.CustomerName = request.CustomerName;
						model.MinimumDownPayment = request.MinimumDownPayment;
						model.Country = request.Country;
						model.Province = request.Province;
						model.City = request.City;
						model.Address = request.Address;
						model.ZipCode = request.ZipCode;
						model.Phone = request.Phone;
						model.Phone2 = request.Phone2;
						model.Fax = request.Fax;
						model.Email = request.Email;
						model.WebAddress = request.WebAddress;
						model.ContactPersonName = request.ContactPersonName;
						model.ContactPersonPhone = request.ContactPersonPhone;
						model.IsActive = request.IsActive;
						model.Notes = request.Notes;
                        _dbContext.Customers.Update(model);
                        await _dbContext.SaveChangesAsync();
                    }
                    else
                    {
                        var newModel = new DataContext.Customer
                        {
                            Id = id,
                            RowStatus = 0,
                            RowVersion = Convert.FromBase64String("AAAAAAAElAs="),
                            CreatedBy = request.User,
                            CreatedDate = DateTime.Now,
                            ModifiedBy = request.User,
                            ModifiedDate = DateTime.Now,
							CategoryCustomerId = request.CategoryCustomerId,
							CustomerCode = request.CustomerCode,
							CustomerName = request.CustomerName,
							MinimumDownPayment = request.MinimumDownPayment,
							Country = request.Country,
							Province = request.Province,
							City = request.City,
							Address = request.Address,
							ZipCode = request.ZipCode,
							Phone = request.Phone,
							Phone2 = request.Phone2,
							Fax = request.Fax,
							Email = request.Email,
							WebAddress = request.WebAddress,
							ContactPersonName = request.ContactPersonName,
							ContactPersonPhone = request.ContactPersonPhone,
							IsActive = request.IsActive,
							Notes = request.Notes,
                        };
                        _dbContext.Customers.Add(newModel);
                        await _dbContext.SaveChangesAsync();
                    }
                        
                    scope.Commit();

                    var customer = await _dbContext.Customers.Select(a => new CustomerModel
                    {
                        Id = a.Id,
                        RowStatus = a.RowStatus,
                        RowVersion = a.RowVersion,
                        CreatedBy = a.CreatedBy,
                        CreatedDate = a.CreatedDate,
                        ModifiedBy = a.ModifiedBy,
                        ModifiedDate = a.ModifiedDate,
                        CategoryCustomerId = a.CategoryCustomerId,
                        CustomerCode = a.CustomerCode,
                        CustomerName = a.CustomerName,
                        CategoryName = a.CategoryCustomer.CategoryName,
                        MinimumDownPayment = a.MinimumDownPayment,
                        Phone = a.Phone,
                        Phone2 = a.Phone2,
                        Fax = a.Fax,
                        Address = a.Address,
                        ZipCode = a.ZipCode,
                        City = a.City,
                        Province = a.Province,
                        Country = a.Country,
                        ContactPersonName = a.ContactPersonName,
                        ContactPersonPhone = a.ContactPersonPhone,
                        Email = a.Email,
                        Notes = a.Notes,
                        WebAddress = a.WebAddress,
                        IsActive = a.IsActive

                    }).FirstOrDefaultAsync(c => c.Id == id && c.RowStatus == 0);
                    if (customer == null)
                    {
                        return ApiResult<Response>.NotFound("Data Customer not found!");
                    }

                    return ApiResult<Response>.Ok(new Response
                    {
                        msg = "Submit Customer Success",
                        CustomerModel = customer,
                        success = true,
                        result = true,
                    });
                }
            }
            catch (Exception ex)
            {
                return ApiResult<Response>.ErrorDuringProcessing(ex.Message);
            }
        }
    }
}
