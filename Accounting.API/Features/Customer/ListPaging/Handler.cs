/*
***************************************************************************************************************
*  GENEREATE BY			:	K.CODERENDERER
*  CREATED BY			:	kurnia.wirawan@gmail.com
*  CREATED DATE			:	Monday, April 15, 2019
*  PROJECT				:	MRP-ACCOUNTING
*  PROJECT ID			:	
*  PROJECT TYPE			:	NetCore
*  SOLUTION NAME		:	Accounting.API
***************************************************************************************************************
*  kurnia.wirawan@gmail.com CONFIDENTIAL
*  
*  kurnia.wirawan@gmail.com 2014 All Rights Reserved
*  NOTICE:  All information contained herein is, and remains the property of kurnia.wirawan@gmail.com and its suppliers,
*  if any.  The intellectual and technical concepts contained 
*  herein are proprietary to kurnia.wirawan@gmail.com and its suppliers and may be covered by Indonesia and Foreign Patents
*  patents in process, and are protected by trade secret or copyright law.
*  Dissemination of this information or reproduction of this material
*  is strictly forbidden unless prior written permission is obtained from kurnia.wirawan@gmail.com
***************************************************************************************************************
*/

using MediatR;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading;
using System.Threading.Tasks;
using Accounting.API.Common.Manager;
using Accounting.API.Common.Parameter;
using Accounting.API.Common.API;
using Accounting.API.DataContext;
using Accounting.API.Models;
using KCore.Common.Response;

namespace Accounting.API.Features.Customer.ListPaging
{
    public class Handler : BaseDataManager, IRequestHandler<Request, ApiResult<Response>>
    {
        private readonly AccountingDataContext _dbContext;
        public Handler(AccountingDataContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        public async Task<ApiResult<Response>> Handle(Request request, CancellationToken cancellationToken)
        {
            var javascriptPagings = JsonConvert.DeserializeObject<List<JavascriptPaging>>(request.Sorters ?? "");
            var whereTermList = ListParameter.GetListParameter(request.Params);
            var whereterm = GetQueryParameterLinqV02(whereTermList.ToArray());
            var pagingCollection = PagingCollection.GetPagingCollection(Paging.Instance(request.Start, request.Limit, Paging.DEFAULT_SORT_DIRECTION, request.Sort), javascriptPagings);
            
            var Customers = await _dbContext.Customers.Select(a => new CustomerModel
            {
                Id = a.Id,
                RowStatus = a.RowStatus,
                RowVersion = a.RowVersion,
                CreatedBy = a.CreatedBy,
                CreatedDate = a.CreatedDate,
                ModifiedBy = a.ModifiedBy,
                ModifiedDate = a.ModifiedDate,
                CategoryCustomerId = a.CategoryCustomerId,
                CategoryName = a.CategoryCustomer.CategoryName,
                CustomerCode = a.CustomerCode,
                CustomerName = a.CustomerName,
                MinimumDownPayment = a.MinimumDownPayment,
                Phone = a.Phone,
                Phone2 = a.Phone2,
                Fax = a.Fax,
                Address = a.Address,
                ZipCode = a.ZipCode,
                City = a.City,
                Province = a.Province,
                Country = a.Country,
                ContactPersonName = a.ContactPersonName,
                ContactPersonPhone = a.ContactPersonPhone,
                Email = a.Email,
                Notes = a.Notes,
                WebAddress = a.WebAddress,
                IsActive = a.IsActive

            }).Where(whereterm, ListValue.ToArray()).OrderBy(PagingCollection.LinqPaging(pagingCollection)).Skip(pagingCollection.Default.Start).Take(pagingCollection.Default.Limit).ToListAsync();
            var totalCount = await _dbContext.Customers.Where(whereterm, ListValue.ToArray()).CountAsync();
            
            return ApiResult<Response>.Ok(new Response
            {
                ListCustomer = Customers,
                msg = "Success",
                totalCount = totalCount,
                success = true,
                result = true,
            });
        }
    }
}
