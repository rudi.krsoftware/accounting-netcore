/*
***************************************************************************************************************
*  GENEREATE BY			:	K.CODERENDERER
*  CREATED BY			:	kurnia.wirawan@gmail.com
*  CREATED DATE			:	Monday, April 15, 2019
*  PROJECT				:	MRP-ACCOUNTING
*  PROJECT ID			:	
*  PROJECT TYPE			:	NetCore
*  SOLUTION NAME		:	Accounting.API
***************************************************************************************************************
*  kurnia.wirawan@gmail.com CONFIDENTIAL
*  
*  kurnia.wirawan@gmail.com 2014 All Rights Reserved
*  NOTICE:  All information contained herein is, and remains the property of kurnia.wirawan@gmail.com and its suppliers,
*  if any.  The intellectual and technical concepts contained 
*  herein are proprietary to kurnia.wirawan@gmail.com and its suppliers and may be covered by Indonesia and Foreign Patents
*  patents in process, and are protected by trade secret or copyright law.
*  Dissemination of this information or reproduction of this material
*  is strictly forbidden unless prior written permission is obtained from kurnia.wirawan@gmail.com
***************************************************************************************************************
*/

using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;
using Accounting.API.Common.API;
using Accounting.API.DataContext;
using System.Linq;
using Accounting.API.Models;
using KCore.Common.Response;

namespace Accounting.API.Features.Customer.Get
{
    public class Handler : IRequestHandler<Request, ApiResult<Response>>
    {
        private readonly AccountingDataContext _dbContext;
        public Handler(AccountingDataContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        public async Task<ApiResult<Response>> Handle(Request request, CancellationToken cancellationToken)
        {
            var customer = await _dbContext.Customers.Select(model => new CustomerModel
            {
                Id = model.Id,
                RowStatus = model.RowStatus,
                RowVersion = model.RowVersion,
                CreatedBy = model.CreatedBy,
                CreatedDate = model.CreatedDate,
                ModifiedBy = model.ModifiedBy,
                ModifiedDate = model.ModifiedDate,
                CategoryCustomerId = model.CategoryCustomerId,
                CategoryName = model.CategoryCustomer.CategoryName,
                CustomerCode = model.CustomerCode,
                CustomerName = model.CustomerName,
                MinimumDownPayment = model.MinimumDownPayment,
                Phone = model.Phone,
                Phone2 = model.Phone2,
                Fax = model.Fax,
                Address = model.Address,
                ZipCode = model.ZipCode,
                City = model.City,
                Province = model.Province,
                Country = model.Country,
                ContactPersonName = model.ContactPersonName,
                ContactPersonPhone = model.ContactPersonPhone,
                Email = model.Email,
                Notes = model.Notes,
                WebAddress = model.WebAddress,
                IsActive = model.IsActive

            }).FirstOrDefaultAsync(c => c.Id == new Guid(request.Id) && c.RowStatus == 0);
            if (customer == null)
            {
                return ApiResult<Response>.NotFound("Data Customer not found!");
            }

            return ApiResult<Response>.Ok(new Response
            {
                CustomerModel = customer,
                success = true,
                result = true,
            });
        }
    }
}
