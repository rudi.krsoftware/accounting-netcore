/*
***************************************************************************************************************
*  GENEREATE BY			:	K.CODERENDERER
*  CREATED BY			:	kurnia.wirawan@gmail.com
*  CREATED DATE			:	Saturday, April 20, 2019
*  PROJECT				:	MRP-ACCOUNTING
*  PROJECT ID			:	
*  PROJECT TYPE			:	NetCore
*  SOLUTION NAME		:	Accounting.API
***************************************************************************************************************
*  kurnia.wirawan@gmail.com CONFIDENTIAL
*  
*  kurnia.wirawan@gmail.com 2014 All Rights Reserved
*  NOTICE:  All information contained herein is, and remains the property of kurnia.wirawan@gmail.com and its suppliers,
*  if any.  The intellectual and technical concepts contained 
*  herein are proprietary to kurnia.wirawan@gmail.com and its suppliers and may be covered by Indonesia and Foreign Patents
*  patents in process, and are protected by trade secret or copyright law.
*  Dissemination of this information or reproduction of this material
*  is strictly forbidden unless prior written permission is obtained from kurnia.wirawan@gmail.com
***************************************************************************************************************
*/

using System;
using MediatR;
using Accounting.API.Common.API;
using KCore.Common.Response;

namespace Accounting.API.Features.Supplier.Submit
{
    public class Request : BaseSubmitRequest, IRequest<ApiResult<Response>>
    {
		public Guid CategorySupplierId { get; set; }
		public string SupplierCode { get; set; }
		public string SupplierName { get; set; }
		public string Country { get; set; }
		public string Province { get; set; }
		public string City { get; set; }
		public string Address { get; set; }
		public string ZipCode { get; set; }
		public string Phone { get; set; }
		public string Phone2 { get; set; }
		public string Fax { get; set; }
		public string Email { get; set; }
		public string WebAddress { get; set; }
		public string ContactPersonName { get; set; }
		public string ContactPersonPhone { get; set; }
		public string DefaultCurrency { get; set; }
		public string DefaultPaymentType { get; set; }
		public decimal DefaultVAT { get; set; }
		public decimal DefaultOtherTax { get; set; }
		public string DefaultOtherTaxRemark { get; set; }
		public decimal DefaultFreightCost { get; set; }
		public decimal DefaultOtherCost { get; set; }
		public string DefaultOtherCostRemark { get; set; }
		public bool IsActive { get; set; }
		public string Notes { get; set; }
    }
}

