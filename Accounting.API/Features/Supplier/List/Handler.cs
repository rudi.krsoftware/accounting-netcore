/*
***************************************************************************************************************
*  GENEREATE BY			:	K.CODERENDERER
*  CREATED BY			:	kurnia.wirawan@gmail.com
*  CREATED DATE			:	Saturday, April 20, 2019
*  PROJECT				:	MRP-ACCOUNTING
*  PROJECT ID			:	
*  PROJECT TYPE			:	NetCore
*  SOLUTION NAME		:	Accounting.API
***************************************************************************************************************
*  kurnia.wirawan@gmail.com CONFIDENTIAL
*  
*  kurnia.wirawan@gmail.com 2014 All Rights Reserved
*  NOTICE:  All information contained herein is, and remains the property of kurnia.wirawan@gmail.com and its suppliers,
*  if any.  The intellectual and technical concepts contained 
*  herein are proprietary to kurnia.wirawan@gmail.com and its suppliers and may be covered by Indonesia and Foreign Patents
*  patents in process, and are protected by trade secret or copyright law.
*  Dissemination of this information or reproduction of this material
*  is strictly forbidden unless prior written permission is obtained from kurnia.wirawan@gmail.com
***************************************************************************************************************
*/

using MediatR;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading;
using System.Threading.Tasks;
using Accounting.API.Common.Manager;
using Accounting.API.Common.Parameter;
using Accounting.API.Common.API;
using Accounting.API.DataContext;
using Accounting.API.Models;
using KCore.Common.Response;

namespace Accounting.API.Features.Supplier.List
{
    public class Handler : BaseDataManager, IRequestHandler<Request, ApiResult<Response>>
    {
        private readonly AccountingDataContext _dbContext;
        public Handler(AccountingDataContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        public async Task<ApiResult<Response>> Handle(Request request, CancellationToken cancellationToken)
        {
            var whereTermList = ListParameter.GetListParameter(request.Params);
            var whereterm = GetQueryParameterLinqV02(whereTermList.ToArray());
            
            var Suppliers = await _dbContext.Suppliers.Select(a => new SupplierModel
            {
                Id = a.Id,
                RowStatus = a.RowStatus,
                RowVersion = a.RowVersion,
                SupplierCode = a.SupplierCode,
                SupplierName = a.SupplierName,
                CategorySupplierId = a.CategorySupplierId,
                CategorySupplierName = a.CategorySupplier.CategoryName,
                Country = a.Country,
                Province = a.Province,
                City = a.City,
                Address = a.Address,
                ZipCode = a.ZipCode,
                Phone = a.Phone,
                Fax = a.Fax,
                Email = a.Email,
                WebAddress = a.WebAddress,
                ContactPersonName = a.ContactPersonName,
                ContactPersonPhone = a.ContactPersonPhone,
                DefaultCurrency = a.DefaultCurrency,
                DefaultPaymentType = a.DefaultPaymentType,
                DefaultVAT = a.DefaultVAT,
                DefaultOtherTax = a.DefaultOtherTax,
                DefaultOtherTaxRemark = a.DefaultOtherTaxRemark,
                DefaultFreightCost = a.DefaultFreightCost,
                DefaultOtherCost = a.DefaultOtherCost,
                DefaultOtherCostRemark = a.DefaultOtherCostRemark,
                CreatedBy = a.CreatedBy,
                CreatedDate = a.CreatedDate,
                ModifiedBy = a.ModifiedBy,
                ModifiedDate = a.ModifiedDate,
            }).Where(whereterm, ListValue.ToArray()).ToListAsync();
            
            return ApiResult<Response>.Ok(new Response
            {
                ListSupplier = Suppliers,
                msg = "Success",
                success = true,
                result = true,
            });
        }
    }
}
