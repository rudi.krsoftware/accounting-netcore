﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.API.Models
{
    public class SupplierModel
    {
        [Key]
        [JsonProperty(PropertyName = "Id")]
        public Guid Id { get; set; }

        [Required]
        [JsonProperty(PropertyName = "RowStatus")]
        public byte RowStatus { get; set; }

        [Required]
        [JsonProperty(PropertyName = "RowVersion")]
        public byte[] RowVersion { get; set; }

        [Required]
        [StringLength(24)]
        [JsonProperty(PropertyName = "CreatedBy")]
        public string CreatedBy { get; set; }

        [Required]
        [JsonProperty(PropertyName = "CreatedDate")]
        [JsonConverter(typeof(JavaScriptDateTimeConverter))]
        public DateTime CreatedDate { get; set; }

        [StringLength(24)]
        [JsonProperty(PropertyName = "ModifiedBy")]
        public string ModifiedBy { get; set; }

        [JsonProperty(PropertyName = "ModifiedDate")]
        [JsonConverter(typeof(JavaScriptDateTimeConverter))]
        public DateTime? ModifiedDate { get; set; }


        [Required]
        [JsonProperty(PropertyName = "CategorySupplierId")]
        public Guid CategorySupplierId { get; set; }

        [JsonProperty(PropertyName = "CategorySupplierName")]
        public string CategorySupplierName { get; set; }

        [Required]
        [StringLength(24)]
        [JsonProperty(PropertyName = "SupplierCode")]
        public string SupplierCode { get; set; }

        [Required]
        [StringLength(128)]
        [JsonProperty(PropertyName = "SupplierName")]
        public string SupplierName { get; set; }

        [Required]
        [StringLength(128)]
        [JsonProperty(PropertyName = "Country")]
        public string Country { get; set; }

        [Required]
        [StringLength(128)]
        [JsonProperty(PropertyName = "Province")]
        public string Province { get; set; }

        [Required]
        [StringLength(128)]
        [JsonProperty(PropertyName = "City")]
        public string City { get; set; }

        [Required]
        [StringLength(256)]
        [JsonProperty(PropertyName = "Address")]
        public string Address { get; set; }

        [Required]
        [StringLength(24)]
        [JsonProperty(PropertyName = "ZipCode")]
        public string ZipCode { get; set; }

        [Required]
        [StringLength(24)]
        [JsonProperty(PropertyName = "Phone")]
        public string Phone { get; set; }

        [StringLength(24)]
        [JsonProperty(PropertyName = "Phone2")]
        public string Phone2 { get; set; }

        [StringLength(64)]
        [JsonProperty(PropertyName = "Fax")]
        public string Fax { get; set; }

        [Required]
        [StringLength(256)]
        [DataType(DataType.EmailAddress)]
        [JsonProperty(PropertyName = "Email")]
        public string Email { get; set; }

        [StringLength(256)]
        [JsonProperty(PropertyName = "WebAddress")]
        public string WebAddress { get; set; }

        [StringLength(64)]
        [JsonProperty(PropertyName = "ContactPersonName")]
        public string ContactPersonName { get; set; }

        [StringLength(24)]
        [JsonProperty(PropertyName = "ContactPersonPhone")]
        public string ContactPersonPhone { get; set; }

        [Required]
        [StringLength(24)]
        [JsonProperty(PropertyName = "DefaultCurrency")]
        public string DefaultCurrency { get; set; }

        [Required]
        [StringLength(24)]
        [JsonProperty(PropertyName = "DefaultPaymentType")]
        public string DefaultPaymentType { get; set; }

        [Column(TypeName = "decimal(24, 2)")]
        [JsonProperty(PropertyName = "DefaultVAT")]
        public decimal DefaultVAT { get; set; }

        [Column(TypeName = "decimal(24, 2)")]
        [JsonProperty(PropertyName = "DefaultOtherTax")]
        public decimal DefaultOtherTax { get; set; }

        [StringLength(128)]
        [JsonProperty(PropertyName = "DefaultOtherTaxRemark")]
        public string DefaultOtherTaxRemark { get; set; }

        [Column(TypeName = "decimal(24, 2)")]
        [JsonProperty(PropertyName = "DefaultFreightCost")]
        public decimal DefaultFreightCost { get; set; }

        [Column(TypeName = "decimal(24, 2)")]
        [JsonProperty(PropertyName = "DefaultOtherCost")]
        public decimal DefaultOtherCost { get; set; }

        [StringLength(128)]
        [JsonProperty(PropertyName = "DefaultOtherCostRemark")]
        public string DefaultOtherCostRemark { get; set; }

        [Required]
        [JsonProperty(PropertyName = "IsActive")]
        public bool IsActive { get; set; }

        [StringLength(512)]
        [JsonProperty(PropertyName = "Notes")]
        public string Notes { get; set; }
    }
}
