﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.API.Models
{
    public class ChartOfAccountModel
    {
        [Key]
        [JsonProperty(PropertyName = "Id")]
        public Guid Id { get; set; }

        [Required]
        [JsonProperty(PropertyName = "RowStatus")]
        public byte RowStatus { get; set; }

        [Required]
        [JsonProperty(PropertyName = "RowVersion")]
        public byte[] RowVersion { get; set; }

        [Required]
        [StringLength(24)]
        [JsonProperty(PropertyName = "CreatedBy")]
        public string CreatedBy { get; set; }

        [Required]
        [JsonProperty(PropertyName = "CreatedDate")]
        [JsonConverter(typeof(JavaScriptDateTimeConverter))]
        public DateTime CreatedDate { get; set; }

        [StringLength(24)]
        [JsonProperty(PropertyName = "ModifiedBy")]
        public string ModifiedBy { get; set; }

        [JsonProperty(PropertyName = "ModifiedDate")]
        [JsonConverter(typeof(JavaScriptDateTimeConverter))]
        public DateTime? ModifiedDate { get; set; }


        [Required]
        [JsonProperty(PropertyName = "SchemaCode")]
        [StringLength(5)]
        public string SchemaCode { get; set; }

        [JsonProperty(PropertyName = "ParentId")]
        public Guid? ParentId { get; set; }

        [Required]
        [JsonProperty(PropertyName = "IsAccountDetail")]
        public bool IsAccountDetail { get; set; }

        [Required]
        [JsonProperty(PropertyName = "CategoryAccountId")]
        public Guid CategoryAccountId { get; set; }

        [JsonProperty(PropertyName = "AccountCategory")]
        public string AccountCategory { get; set; }

        [Required]
        [JsonProperty(PropertyName = "AccountTypeId")]
        public Guid AccountTypeId { get; set; }

        [JsonProperty(PropertyName = "AccountType")]
        public string AccountType { get; set; }

        [Required]
        [JsonProperty(PropertyName = "AccountCode")]
        [StringLength(24)]
        public string AccountCode { get; set; }

        [Required]
        [JsonProperty(PropertyName = "AccountName")]
        [StringLength(128)]
        public string AccountName { get; set; }

        [Required]
        [JsonProperty(PropertyName = "AccountLevel")]
        public short AccountLevel { get; set; }

        [JsonProperty(PropertyName = "DefaultValue")]
        public short? DefaultValue { get; set; }

        [JsonProperty(PropertyName = "Notes")]
        [StringLength(512)]
        public string Notes { get; set; }
    }
}
