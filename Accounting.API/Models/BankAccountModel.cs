﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Accounting.API.Models
{
    public class BankAccountModel
    {
        [Key]
        [JsonProperty(PropertyName = "Id")]
        public Guid Id { get; set; }

        [Required]
        [JsonProperty(PropertyName = "RowStatus")]
        public byte RowStatus { get; set; }

        [Required]
        [JsonProperty(PropertyName = "RowVersion")]
        public byte[] RowVersion { get; set; }

        [Required]
        [StringLength(24)]
        [JsonProperty(PropertyName = "CreatedBy")]
        public string CreatedBy { get; set; }

        [Required]
        [JsonProperty(PropertyName = "CreatedDate")]
        [JsonConverter(typeof(JavaScriptDateTimeConverter))]
        public DateTime CreatedDate { get; set; }

        [StringLength(24)]
        [JsonProperty(PropertyName = "ModifiedBy")]
        public string ModifiedBy { get; set; }

        [JsonProperty(PropertyName = "ModifiedDate")]
        [JsonConverter(typeof(JavaScriptDateTimeConverter))]
        public DateTime? ModifiedDate { get; set; }

        [JsonProperty(PropertyName = "BankName")]
        public string BankName { get; set; }

        [Required]
        [JsonProperty(PropertyName = "BankId")]
        public Guid BankId { get; set; }

        [Required]
        [StringLength(64)]
        [JsonProperty(PropertyName = "AccountNumber")]
        public string AccountNumber { get; set; }

        [Required]
        [StringLength(128)]
        [JsonProperty(PropertyName = "AccountName")]
        public string AccountName { get; set; }

        [Required]
        [JsonProperty(PropertyName = "IsActive")]
        public bool IsActive { get; set; }

        [StringLength(512)]
        [JsonProperty(PropertyName = "Notes")]
        public string Notes { get; set; }
    }
}
