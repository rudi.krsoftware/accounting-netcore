﻿using Accounting.API.Common.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;

namespace Accounting.API.Models
{
    public class ChartOfAccountTreeModel : ExtTreeMenuNode
    {
        [JsonProperty(PropertyName = "Id")]
        public Guid Id { get; set; }

        [JsonProperty(PropertyName = "Level")]
        public int Level { get; set; }

        [JsonProperty(PropertyName = "CategoryAccountId")]
        public Guid CategoryAccountId { get; set; }

        [JsonProperty(PropertyName = "AccountCategory")]
        public string AccountCategory { get; set; }

        [JsonProperty(PropertyName = "AccountTypeId")]
        public Guid AccountTypeId { get; set; }

        [JsonProperty(PropertyName = "AccountType")]
        public string AccountType { get; set; }

        [JsonProperty(PropertyName = "AccountLevel")]
        public short AccountLevel { get; set; }

        [JsonProperty(PropertyName = "DefaultValue")]
        public short? DefaultValue { get; set; }

        [JsonProperty(PropertyName = "CreatedBy")]
        public string CreatedBy { get; set; }

        [JsonProperty(PropertyName = "CreatedDate")]
        [JsonConverter(typeof(JavaScriptDateTimeConverter))]
        public DateTime CreatedDate { get; set; }

        [JsonProperty(PropertyName = "ModifiedBy")]
        public string ModifiedBy { get; set; }

        [JsonProperty(PropertyName = "ModifiedDate")]
        [JsonConverter(typeof(JavaScriptDateTimeConverter))]
        public DateTime? ModifiedDate { get; set; }

        public static ChartOfAccountTreeModel New(ChartOfAccountModel chartOfAccountModel)
        {
            return new ChartOfAccountTreeModel
            {
                Id = chartOfAccountModel.Id,
                Level = chartOfAccountModel.AccountLevel,
                Text = chartOfAccountModel.AccountCode + " - " + chartOfAccountModel.AccountName,
                CategoryAccountId = chartOfAccountModel.CategoryAccountId,
                AccountCategory = chartOfAccountModel.AccountCategory,
                AccountTypeId = chartOfAccountModel.AccountTypeId,
                AccountType = chartOfAccountModel.AccountType,
                IsLeaf = chartOfAccountModel.IsAccountDetail,
                AccountLevel = chartOfAccountModel.AccountLevel,
                DefaultValue = chartOfAccountModel.DefaultValue,
                CreatedBy = chartOfAccountModel.CreatedBy,
                CreatedDate = chartOfAccountModel.CreatedDate,
                ModifiedBy = chartOfAccountModel.ModifiedBy,
                ModifiedDate = chartOfAccountModel.ModifiedDate
            };
        }
    }
}
