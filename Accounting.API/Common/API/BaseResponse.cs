﻿namespace Accounting.API.Common.API
{
    public class BaseResponse
    {
        public BaseResponse()
        {
            msg = string.Empty;
            result = true;
            totalCount = 0;
            success = false;
            severity = 0;
        }
        public string msg { get; set; }
        public bool result { get; set; }
        public bool success { get; set; }
        public int totalCount { get; set; }
        public int severity { get; set; }
    }
}
