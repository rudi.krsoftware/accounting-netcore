﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Accounting.API.Common.Context
{
    public class BaseApproveContext : BaseContext
    {
        [StringLength(24)]
        [JsonProperty(PropertyName = "StatusApproval")]
        public string StatusApproval { get; set; }

        [Required]
        [StringLength(24)]
        [JsonProperty(PropertyName = "EmployeeCode")]
        public string EmployeeCode { get; set; }

        [Required]
        [StringLength(64)]
        [JsonProperty(PropertyName = "EmployeeName")]
        public string EmployeeName { get; set; }

        [Required]
        [StringLength(64)]
        [JsonProperty(PropertyName = "DivisionName")]
        public string DivisionName { get; set; }

        [Required]
        [StringLength(64)]
        [JsonProperty(PropertyName = "Position")]
        public string Position { get; set; }

        [Required]
        [StringLength(64)]
        [JsonProperty(PropertyName = "Location")]
        public string Location { get; set; }

        [Required]
        [StringLength(256)]
        [DataType(DataType.EmailAddress)]
        [JsonProperty(PropertyName = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(256)]
        [DataType(DataType.EmailAddress)]
        [JsonProperty(PropertyName = "CcEmail")]
        public string CcEmail { get; set; }
    }
}
