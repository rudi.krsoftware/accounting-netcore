﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Accounting.API.Common.Context
{
    public class BaseContext
    {
        [Key]
        [JsonProperty(PropertyName = "Id")]
        public Guid Id { get; set; }

        [Required]
        [JsonProperty(PropertyName = "RowStatus")]
        public byte RowStatus { get; set; }

        [Required]
        [JsonProperty(PropertyName = "RowVersion")]
        public byte[] RowVersion { get; set; }

        [Required]
        [StringLength(24)]
        [JsonProperty(PropertyName = "CreatedBy")]
        public string CreatedBy { get; set; }

        [Required]
        [JsonProperty(PropertyName = "CreatedDate")]
        [JsonConverter(typeof(JavaScriptDateTimeConverter))]
        public DateTime CreatedDate { get; set; }

        [StringLength(24)]
        [JsonProperty(PropertyName = "ModifiedBy")]
        public string ModifiedBy { get; set; }

        [JsonProperty(PropertyName = "ModifiedDate")]
        [JsonConverter(typeof(JavaScriptDateTimeConverter))]
        public DateTime? ModifiedDate { get; set; }


    }
}
