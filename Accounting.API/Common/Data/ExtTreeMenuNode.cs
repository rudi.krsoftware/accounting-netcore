﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Accounting.API.Common.Data
{
    [Serializable]
    public class ExtTreeMenuNode
    {
        public ExtTreeMenuNode()
        {
            Children = new List<ExtTreeMenuNode>();
        }

        [JsonProperty("text")]
        public string Text { get; set; }
        [JsonProperty("nodeType ")]
        public string NodeType { get; set; }
        [JsonProperty("iconCls")]
        public string IconCssClass { get; set; }
        [JsonProperty("leaf")]
        public bool IsLeaf { get; set; }
        [JsonProperty("href")]
        public string HtmlHref { get; set; }
        [JsonProperty("children")]
        public List<ExtTreeMenuNode> Children { get; set; } 

    }
}
