﻿namespace Accounting.API.Common.Parameter
{
    public enum LogicalOperator
    {
        AND,
        OR
    }
}
