﻿using System;

namespace Accounting.API.Common.Parameter
{
    [Serializable]
    public class JavascriptPaging
    {
        public string Kolom { get; set; }
        public string KolomDisplay { get; set; }
        public string Sorting { get; set; }
        public string SortingDisplay { get; set; }

        public string GetSortForLinq()
        {
            return Sorting == "0" ? "ASC" : "DESC";
        }
    }
}
