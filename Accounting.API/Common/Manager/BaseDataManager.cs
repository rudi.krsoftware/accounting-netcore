﻿using Accounting.API.Common.Parameter;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Accounting.API.Common.Manager
{
    public abstract class BaseDataManager
    {
        public static Collection<object> ListValue { get; set; }

        public static string GetQueryParameterLinqV02(params IListParameter[] parameters)
        {
            WhereTerm.AddDefaultParameter(ref parameters);
            return GetQueryParameterLinqWithoutRowStatus(parameters);
        }

        public static string GetQueryParameterLinqWithoutRowStatus(params IListParameter[] parameters)
        {
            var query = new StringBuilder();
            ListValue = new Collection<object>();
            var indexpass = 0;

            var total = parameters.Length - 1;
            var indexlogical = 0;
            foreach (var param in parameters)
            {
                var index = parameters.ToList().IndexOf(param) + indexpass;
                switch (param.ParamDataType)
                {
                    case EnumParamterDataTypes.DateTime:
                        {
                            var date1 = Convert.ToDateTime(param.GetValue());
                            var fdate = new DateTime(date1.Year, date1.Month, date1.Day, 0, 0, 0);
                            var ldate = new DateTime(date1.Year, date1.Month, date1.Day, 23, 59, 59);
                            if (param.Operator == SqlOperator.Equals)
                            {
                                var fparma = WhereTerm.Default(fdate, param.ColumnName, SqlOperator.GreatThanEqual);
                                var lparma = WhereTerm.Default(ldate, param.ColumnName, SqlOperator.LesThanEqual);
                                query.Append(
                                    GetValueParameterLinq(fparma, index) + DefaultValue.LOGICAL_SQL);
                                ListValue.Add(fparma.GetValue());
                                index++;
                                indexpass++;
                                query.Append(GetValueParameterLinq(lparma, index) +
                                             AddLogicalOperator(total, indexlogical, param));
                                ListValue.Add(lparma.GetValue());
                            }
                            else
                            {
                                query.Append(
                                    GetValueParameterLinq(param, index) +
                                    AddLogicalOperator(total, indexlogical, param));
                                ListValue.Add(date1);
                            }
                            indexlogical++;
                        }
                        break;
                    case EnumParamterDataTypes.DateTimeRange:
                        {
                            query.Append(
                                GetValueParameterLinq(param, index) +
                                AddLogicalOperator(total, indexlogical, param));
                            ListValue.Add(param.GetValue());
                            var date1 = Convert.ToDateTime(((IListRangeParameter)param).GetValue2());
                            //var ldate = new DateTime(date1.Year, date1.Month, date1.Day, 23, 59, 59);
                            ListValue.Add(date1);
                            indexpass++;
                            indexlogical++;
                        }
                        break;
                    case EnumParamterDataTypes.Guid:
                        query.Append(
                            GetValueParameterLinq(param, index) +
                            AddLogicalOperator(total, indexlogical, param)
                            );
                        ListValue.Add(new Guid(param.GetValue().ToString()));
                        indexlogical++;
                        break;
                    case EnumParamterDataTypes.NullableDateTime:
                        query.Append(param.ColumnName + " = null " + AddLogicalOperator(total, indexlogical, param));
                        indexpass--;
                        break;
                    case EnumParamterDataTypes.Column:
                        query.Append(param.ColumnName + GetStandardOperator(param.Operator, param) +
                                     AddLogicalOperator(total, indexlogical, param));
                        indexpass--;
                        break;
                    default:
                        query.Append(
                            GetValueParameterLinq(param, index) +
                            AddLogicalOperator(total, indexlogical, param));
                        ListValue.Add(param.GetValue());
                        indexlogical++;
                        break;
                }

            }
            if (query.Length != 0)
            {
                return ComposeLogicalBracket(query + " AND ");
            }

            return query.ToString();
        }

        private static string AddLogicalOperator(int total, int indexlogical, IListParameter param)
        {
            return (total - indexlogical == 0 ? "" : param.Logical.ToString());
        }

        private static string GetNumericLinq(IListParameter param, int index)
        {
            return string.Format(" ({0}{1} {2}) ",
                                 string.Empty,
                                 param.ColumnName, GetOperatorNumberLinq(param.Operator, index));
        }

        private static StringBuilder GetOperatorNumberLinq(SqlOperator sqloperator, int index)
        {
            switch (sqloperator)
            {
                case SqlOperator.NotEqual:
                    return new StringBuilder().AppendFormat("<> @{0}", index);
                case SqlOperator.GreatThan:
                    return new StringBuilder().AppendFormat("> @{0}", index);
                case SqlOperator.GreatThanEqual:
                    return new StringBuilder().AppendFormat(">= @{0}", index);
                case SqlOperator.LessThan:
                    return new StringBuilder().AppendFormat("< @{0}", index);
                case SqlOperator.LesThanEqual:
                    return new StringBuilder().AppendFormat("<= @{0}", index);
                case SqlOperator.IsNull:
                    return new StringBuilder().AppendFormat("== NULL");
                default:
                    return new StringBuilder().AppendFormat("= @{0}", index);
            }
        }

        private static string GetDateTimeLinq(IListParameter param, int index)
        {
            return string.Format(" ({0}{1}{2}) ", string.Empty,
                param.ColumnName, GetOperatorDateTimeLinq(param.Operator, index));
        }

        private static object GetOperatorDateTimeLinq(SqlOperator sqlOperator, int index)
        {
            switch (sqlOperator)
            {
                case SqlOperator.NotEqual:
                    return new StringBuilder().AppendFormat("<> @{0}", index);
                case SqlOperator.GreatThan:
                    return new StringBuilder().AppendFormat("> @{0}", index);
                case SqlOperator.GreatThanEqual:
                    return new StringBuilder().AppendFormat(">= @{0}", index);
                case SqlOperator.LessThan:
                    return new StringBuilder().AppendFormat("< @{0}", index);
                case SqlOperator.LesThanEqual:
                    return new StringBuilder().AppendFormat("<= @{0}", index);
                case SqlOperator.IsNull:
                    return new StringBuilder().AppendFormat("== NULL");
                default:
                    return new StringBuilder().AppendFormat("= @{0}", index);
            }
        }

        private static string GetDateTimeRangeLinq(IListRangeParameter rangecontrol, int index)
        {
            if (rangecontrol == null) return null;
            return string.Format(" ({0} {1} @{2} AND {3} {4} @{5})",
                                 rangecontrol.ColumnName, ">=",
                                 index,
                                  rangecontrol.ColumnName2, "<=",
                                 index + 1);
        }

        private static string GetBooleanLinq(IListParameter control, int index)
        {
            return string.Format(" ({0}{1} {2}) ",
                                 string.Empty,
                                 control.ColumnName, GetOperatorBooleanLinq(control.Operator, index));
        }

        private static string GetOperatorBooleanLinq(SqlOperator sqlOperator, int index)
        {
            switch (sqlOperator)
            {
                case SqlOperator.NotEqual:
                    return string.Format("!= @{0}", index);
                case SqlOperator.IsNull:
                    return "== NULL";
                default:
                    return string.Format("= @{0}", index);
            }
        }

        private static string GetGuidLinq(IListParameter param, int index)
        {
            return string.Format(" ({0}{1}{2}) ",
                                 string.Empty,
                                 param.ColumnName, GetOperatorGuidLinq(param.Operator, index));
        }

        private static string GetOperatorGuidLinq(SqlOperator @operator, int param)
        {
            switch (@operator)
            {
                case SqlOperator.NotEqual:
                    return string.Format("<> @{0}", param);
                default:
                    return string.Format(" == @{0}", param);
            }
        }

        private static string GetCharacterLinq(IListParameter param, int index)
        {
            return string.Format(" ({0}{1}{2}) ", string.IsNullOrEmpty(param.TableName) ? string.Empty : param.TableName + ".", param.ColumnName, GetOperatorCharacterLinq(param.Operator, index));
        }

        private static string GetOperatorCharacterLinq(SqlOperator @operator, int param)
        {
            switch (@operator)
            {
                case SqlOperator.NotEqual:
                    return string.Format(".Equals(@{0}) == false", param);
                case SqlOperator.GreatThan:
                    return string.Format("> @{0}", param);
                case SqlOperator.GreatThanEqual:
                    return string.Format(">= @{0}", param);
                case SqlOperator.LessThan:
                    return string.Format("< @{0}", param);
                case SqlOperator.LesThanEqual:
                    return string.Format("<= @{0}", param);
                case SqlOperator.BeginWith:
                    return string.Format(
                        ".StartsWith(@{0})", param);
                case SqlOperator.EndWith:
                    return string.Format(".EndsWith(@{0})", param);
                case SqlOperator.Like:
                    return string.Format(".Contains(@{0})", param);
                default:
                    return string.Format(".Equals(@{0})", param);
            }
        }

        private static string GetValueParameterLinq(IListParameter param, int index)
        {
            switch (param.ParamDataType)
            {
                case EnumParamterDataTypes.Number:
                    return GetNumericLinq(param, index);
                case EnumParamterDataTypes.NumberRange:
                    return GetNumericLinq(param, index);
                case EnumParamterDataTypes.DateTime:
                    return GetDateTimeLinq(param, index);
                case EnumParamterDataTypes.DateTimeRange:
                    return GetDateTimeRangeLinq(param as IListRangeParameter, index);
                case EnumParamterDataTypes.Bool:
                    return GetBooleanLinq(param, index);
                case EnumParamterDataTypes.Guid:
                    return GetGuidLinq(param, index);
                default:
                    return GetCharacterLinq(param, index);
            }
        }

        private static string GetStandardOperator(SqlOperator @operator, IListParameter param)
        {
            switch (@operator)
            {
                case SqlOperator.NotEqual:
                    return string.Format("!= {0}", param.Value);
                case SqlOperator.GreatThan:
                    return string.Format("> {0}", param.Value);
                case SqlOperator.GreatThanEqual:
                    return string.Format(">= {0}", param.Value);
                case SqlOperator.LessThan:
                    return string.Format("< {0}", param.Value);
                case SqlOperator.LesThanEqual:
                    return string.Format("<= {0}", param.Value);
                case SqlOperator.BeginWith:
                    return string.Format(".StartsWith({0})", param.Value);
                case SqlOperator.EndWith:
                    return string.Format(".EndsWith({0})", param.Value);
                case SqlOperator.Like:
                    return string.Format(".Contains({0})", param.Value);
                case SqlOperator.IsNull:
                    return "== null";
                default:
                    return string.Format("== {0}", param.Value);
            }
        }

        private static string ComposeLogicalBracket(string val)
        {
            /*var val = "1 OR 2 OR 3 AND 4 OR 5 AND 6 OR 7 OR 8 OR 9 OR 10 AND 11 OR ";
            val = "1 OR 2 OR 3 OR 4 OR 5 OR 6 OR 7 OR 8 OR 9 OR 10 OR";
            val = "1 OR 2 AND 3 OR 4 AND 5 OR 6 AND 7 OR 8 AND 9 OR 10 AND";
            val = "1 AND 2 OR 3 AND 4 OR 5 AND 6 OR 7 AND 8 OR 9 AND 10 AND";*/
            var a = val.Split(new[] { "AND", "OR" }, StringSplitOptions.None);
            var list = a.ToList();
            if (list.Count == 1) return val;

            var result = "";

            var br = false;
            foreach (var item in list)
            {
                var index = list.IndexOf(item) + 1;
                if (index >= list.Count || string.IsNullOrEmpty(item.Trim()))
                    continue;
                var rr = val.Between(item.Trim(), list[index].Trim());
                if (rr.Trim().ToLower().Equals("and"))
                {
                    if (br)
                    {
                        br = false;
                        result += item + ") AND ";
                    }
                    else
                        result += item + " AND ";

                }
                else
                {
                    if (!br)
                    {
                        if (index + 1 >= list.Count)
                            result += item;
                        else
                            result += "(" + item + " OR ";
                        br = true;

                    }
                    else
                    {
                        if (index + 1 >= list.Count)
                        {
                            result += item + ")";
                            br = false;

                        }
                        else
                        {
                            //var lg = val.Between(list[index], list[index + 1]);
                            result += item + " OR ";
                        }
                    }

                }
            }
            return result;
        }
    }
}
