﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Accounting.API.Migrations
{
    public partial class bugfixtableaccountType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "AccountCategoryName",
                table: "AccountTypes",
                newName: "AccountTypeName");

            migrationBuilder.RenameColumn(
                name: "AccountCategoryCode",
                table: "AccountTypes",
                newName: "AccountTypeCode");

            migrationBuilder.RenameIndex(
                name: "IX_AccountTypes_AccountCategoryCode",
                table: "AccountTypes",
                newName: "IX_AccountTypes_AccountTypeCode");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "AccountTypeName",
                table: "AccountTypes",
                newName: "AccountCategoryName");

            migrationBuilder.RenameColumn(
                name: "AccountTypeCode",
                table: "AccountTypes",
                newName: "AccountCategoryCode");

            migrationBuilder.RenameIndex(
                name: "IX_AccountTypes_AccountTypeCode",
                table: "AccountTypes",
                newName: "IX_AccountTypes_AccountCategoryCode");
        }
    }
}
