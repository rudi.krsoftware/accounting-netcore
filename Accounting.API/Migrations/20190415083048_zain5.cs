﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Accounting.API.Migrations
{
    public partial class zain5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ChartOfAccounts_CategoryAccounts_CategoryAccountId",
                table: "ChartOfAccounts");

            migrationBuilder.DropColumn(
                name: "AccountCategoryId",
                table: "ChartOfAccounts");

            migrationBuilder.AlterColumn<Guid>(
                name: "CategoryAccountId",
                table: "ChartOfAccounts",
                nullable: false,
                oldClrType: typeof(Guid),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_ChartOfAccounts_CategoryAccounts_CategoryAccountId",
                table: "ChartOfAccounts",
                column: "CategoryAccountId",
                principalTable: "CategoryAccounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ChartOfAccounts_CategoryAccounts_CategoryAccountId",
                table: "ChartOfAccounts");

            migrationBuilder.AlterColumn<Guid>(
                name: "CategoryAccountId",
                table: "ChartOfAccounts",
                nullable: true,
                oldClrType: typeof(Guid));

            migrationBuilder.AddColumn<Guid>(
                name: "AccountCategoryId",
                table: "ChartOfAccounts",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddForeignKey(
                name: "FK_ChartOfAccounts_CategoryAccounts_CategoryAccountId",
                table: "ChartOfAccounts",
                column: "CategoryAccountId",
                principalTable: "CategoryAccounts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
