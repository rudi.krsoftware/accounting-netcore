﻿namespace Authorization.API.Common.API
{
    public class BaseSubmitRequest
    {
        public string Id { get; set; }
        public byte? RowStatus { get; set; }
        public string User { get; set; }
    }
}
