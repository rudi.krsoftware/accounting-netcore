﻿namespace Authorization.API.Common.API
{
    public class BaseDeleteRequest
    {
        public string Id { get; set; }
        public string User { get; set; }
    }
}
