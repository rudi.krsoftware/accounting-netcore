﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Authorization.API.Common.Parameter
{
    public class WhereTermNumberRange : IListRangeParameter
    {
        public EnumParamterDataTypes ParamDataType { get; set; }
        public WhereTermNumberRange(double? number1, double? number2)
        {
            Value1 = number1;
            Value2 = number2;
            Logical = LogicalOperator.AND;
        }

        public WhereTermNumberRange()
        {
            Value1 = null;
            Value2 = null;
            Logical = LogicalOperator.AND;
        }

        public object GetValue()
        {
            return Value1;
        }

        public static WhereTermNumberRange Parameter(double v1, double v2, string column)
        {
            return new WhereTermNumberRange(v1, v2)
            {
                TableName = String.Empty,
                ParamDataType = EnumParamterDataTypes.NumberRange,
                Operator = SqlOperator.Equals,
                ColumnName = column,
                ColumnName2 = column
            };
        }

        public static WhereTermNumberRange Parameter(double v1, double v2, string column, LogicalOperator logical)
        {
            return new WhereTermNumberRange(v1, v2)
            {
                TableName = String.Empty,
                ParamDataType = EnumParamterDataTypes.NumberRange,
                Operator = SqlOperator.Equals,
                ColumnName = column,
                ColumnName2 = column,
                Logical = logical
            };
        }

        public static WhereTermNumberRange Parameter(double? v1, double? v2, string column, LogicalOperator logical)
        {
            return new WhereTermNumberRange(v1, v2)
            {
                TableName = String.Empty,
                ParamDataType = EnumParamterDataTypes.NumberRange,
                Operator = SqlOperator.Equals,
                ColumnName = column,
                ColumnName2 = column,
                Logical = logical
            };
        }

        public string TableName { get; set; }
        public string ColumnName { get; set; }
        public SqlOperator Operator { get; set; }
        public bool HasValue
        {
            get { return Value1 != null; }
        }

        public object Value { get; set; }
        public LogicalOperator Logical { get; set; }

        public object GetValue2()
        {
            return Value2;
        }
        public double? Value1 { get; set; }
        public double? Value2 { get; set; }
        public string TableName2 { get; set; }
        public string ColumnName2 { get; set; }
        public bool HasValue2 { get { return Value2 != null; } }


    }
}
