﻿namespace Authorization.API.Common.Parameter
{
    public enum LogicalOperator
    {
        AND,
        OR
    }
}
