﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Authorization.API.Common.Parameter
{
    public class PagingCollection : ICollection<Paging>
    {
        private readonly Collection<Paging> _list;

        public PagingCollection()
        {
            _list = new Collection<Paging>();
        }
        public IEnumerator<Paging> GetEnumerator()
        {
            return _list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(Paging item)
        {
            _list.Add(item);
        }

        public void Clear()
        {
            _list.Clear();
        }

        public bool Contains(Paging item)
        {
            return _list.Contains(item);
        }

        public void CopyTo(Paging[] array, int arrayIndex)
        {
            _list.CopyTo(array, arrayIndex);
        }

        public bool Remove(Paging item)
        {
            return _list.Remove(item);
        }

        public Paging Default
        {
            get
            {
                return _list.AsQueryable().FirstOrDefault();
            }
        }

        public int Count { get; private set; }
        public bool IsReadOnly { get; private set; }

        public static PagingCollection GetPagingCollection(Paging defaultPaging, IList<JavascriptPaging> list)
        {
            var pagingCollection = new PagingCollection();
            if (list == null || list.Count == 0)
            {
                pagingCollection.Add(new Paging
                {
                    Direction = Paging.GetExtJsSortColumn(defaultPaging.SortColumn, defaultPaging.Direction),
                    Limit = defaultPaging.Limit,
                    SortColumn = Paging.GetExtJsSortColumn(defaultPaging.SortColumn),
                    Start = defaultPaging.Start
                });
                return pagingCollection;
            }
            foreach (var javascriptPaging in list)
            {
                if (string.IsNullOrEmpty(javascriptPaging.Kolom) || string.IsNullOrEmpty(javascriptPaging.Sorting))
                {
                    pagingCollection.Add(new Paging
                    {
                        Direction = Paging.GetExtJsSortColumn(defaultPaging.SortColumn, defaultPaging.Direction),
                        Limit = defaultPaging.Limit,
                        SortColumn = Paging.GetExtJsSortColumn(defaultPaging.SortColumn),
                        Start = defaultPaging.Start
                    });
                }
                else
                {
                    pagingCollection.Add(new Paging
                    {
                        Direction = javascriptPaging.GetSortForLinq(),
                        Limit = defaultPaging.Limit,
                        SortColumn = javascriptPaging.Kolom,
                        Start = defaultPaging.Start
                    });
                }

            }
            return pagingCollection;
        }

        public static string LinqPaging(PagingCollection list)
        {
            var stringBuilder = new StringBuilder();

            foreach (var javascriptPaging in list)
            {
                stringBuilder.Append(javascriptPaging.SortColumn + " " + javascriptPaging.Direction + " , ");
            }
            var sort = stringBuilder.ToString().Trim();
            return sort.Substring(0, sort.Length - 1);

        }
    }
}
