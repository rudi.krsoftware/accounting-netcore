﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Authorization.API.Common.Parameter
{
    public static class StringExtensionMethod
    {
        public static string Between(this string src, string findfrom, string findto)
        {
            var start = src.IndexOf(findfrom, StringComparison.Ordinal);
            var to = src.IndexOf(findto, start + findfrom.Length, StringComparison.Ordinal);
            if (start < 0 || to < 0) return "";
            var s = src.Substring(
                           start + findfrom.Length,
                           to - start - findfrom.Length);
            return s;
        }


        public static string CreateMd5HashPassword(string plainPassword)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            //compute hash from the bytes of text            
            md5.ComputeHash(Encoding.ASCII.GetBytes(plainPassword));
            //get hash result after compute it            
            var bytes = md5.Hash;
            var strBuilder = new StringBuilder();
            for (var i = 0; i < bytes.Length; i++)
            {
                strBuilder.Append(bytes[i].ToString("X2"));
            }
            return strBuilder.ToString();
        }
    }
}
