﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.ComponentModel.DataAnnotations;

namespace Authorization.API.Common.Model
{
    public class BaseModel
    {
        [Key]
        [Required]
        [JsonProperty(PropertyName = "Id")]
        public Guid Id { get; set; }

        [Required]
        [JsonProperty(PropertyName = "RowStatus")]
        public byte RowStatus { get; set; }

        [Required]
        [JsonProperty(PropertyName = "CreatedBy")]
        public string CreatedBy { get; set; }

        [Required]
        [JsonConverter(typeof(JavaScriptDateTimeConverter))]
        [JsonProperty(PropertyName = "CreatedDate")]
        public DateTime CreatedDate { get; set; }

        [JsonProperty(PropertyName = "ModifiedBy")]
        public string ModifiedBy { get; set; }

        [JsonConverter(typeof(JavaScriptDateTimeConverter))]
        [JsonProperty(PropertyName = "ModifiedDate")]
        public DateTime? ModifiedDate { get; set; }
    }
}
