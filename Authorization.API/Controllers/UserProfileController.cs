﻿using System.Threading.Tasks;
using Authorization.API.Common.API;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace Authorization.API.Controllers
{
    [Produces("application/json")]
    [ApiController]
    public class UserProfileController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly IConfiguration _configuration;
        public UserProfileController(IMediator mediator, IConfiguration configuration)
        {
            _mediator = mediator;
            _configuration = configuration;
        }

        [Route("api/authorization/userprofile/list")]
        [Authorize]
        [HttpGet]
        public async Task<IActionResult> ListPaging([FromQuery]Services.UserProfile.ListPaging.Request request)
        {
            return ApiResultToActionResultMapper.ToActionResult(await _mediator.Send(request));
        }

        [Route("api/authorization/userprofile/get")]
        [Authorize]
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery]Services.UserProfile.Get.Request request)
        {
            return ApiResultToActionResultMapper.ToActionResult(await _mediator.Send(request));
        }

        [Route("api/authorization/userprofile/submit")]
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Submit([FromBody]Services.UserProfile.Submit.Request request)
        {
            return ApiResultToActionResultMapper.ToActionResult(await _mediator.Send(request));
        }

        [Route("api/authorization/userprofile/delete")]
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> Delete([FromBody]Services.UserProfile.Delete.Request request)
        {
            return ApiResultToActionResultMapper.ToActionResult(await _mediator.Send(request));
        }

        [Route("api/authorization/userprofile/changepassword")]
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> ChangePasswordUserProfile([FromBody]Services.UserProfile.ChangePassword.Request request)
        {
            return ApiResultToActionResultMapper.ToActionResult(await _mediator.Send(request));
        }

        [Route("api/authorization/userprofile/login")]
        [HttpPost]
        public async Task<IActionResult> DoLogin([FromBody]Services.UserProfile.Login.Request request)
        {
            return ApiResultToActionResultMapper.ToActionResult(await _mediator.Send(request));
        }
    }
}