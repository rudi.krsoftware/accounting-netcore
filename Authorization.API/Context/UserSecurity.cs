﻿using Authorization.API.Common.Model;
using Newtonsoft.Json;

namespace Authorization.API.Context
{
    public class UserSecurity : BaseModel
    {
        [JsonProperty(PropertyName = "UserPassword")]
        public string UserPassword { get; set; }

        [JsonProperty(PropertyName = "MustChangePassword")]
        public bool MustChangePassword { get; set; }

        [JsonProperty(PropertyName = "SecurityQuestion")]
        public string SecurityQuestion { get; set; }

        [JsonProperty(PropertyName = "SecurityAnswer")]
        public string SecurityAnswer { get; set; }
    }
}
