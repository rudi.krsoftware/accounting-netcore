﻿using Authorization.API.Common.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.ComponentModel.DataAnnotations;

namespace Authorization.API.Context
{
    public class UserProfile : BaseModel
    {
        [Required]
        [JsonProperty(PropertyName = "Username")]
        public string Username { get; set; }

        [Required]
        [JsonProperty(PropertyName = "Fullname")]
        public string Fullname { get; set; }

        [Required]
        [JsonProperty(PropertyName = "Telephone")]
        public string Telephone { get; set; }

        [JsonProperty(PropertyName = "Telephone2")]
        public string Telephone2 { get; set; }

        [Required]
        [JsonProperty(PropertyName = "Address")]
        public string Address { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [JsonProperty(PropertyName = "Email")]
        public string Email { get; set; }

        [Required]
        [JsonProperty(PropertyName = "Sex")]
        public int Sex { get; set; }

        [Required]
        [JsonConverter(typeof(JavaScriptDateTimeConverter))]
        [JsonProperty(PropertyName = "RegisteredDate")]
        public DateTime RegisteredDate { get; set; }

        [JsonProperty(PropertyName = "IsMobileUser")]
        public bool IsMobileUser { get; set; }

        [JsonProperty(PropertyName = "DeviceId")]
        public string DeviceId { get; set; }

        [JsonProperty(PropertyName = "IsActive")]
        public bool IsActive { get; set; }
    }
}
