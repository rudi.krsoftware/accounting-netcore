﻿using Authorization.API.Common.API;
using Authorization.API.Common.Parameter;
using Authorization.API.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Authorization.API.Services.UserProfile.Login
{
    public class Handler : IRequestHandler<Request, ApiResult<Response>>
    {
        private readonly DataContext _dbContext;
        private readonly IConfiguration _configuration;
        public Handler(DataContext dbContext, IConfiguration configuration)
        {
            _dbContext = dbContext;
            _configuration = configuration;
        }

        public async Task<ApiResult<Response>> Handle(Request request, CancellationToken cancellationToken)
        {
            var userProfile = await _dbContext.UserProfile.FirstOrDefaultAsync(c => c.Username == request.Username);
            if(userProfile == null)
            {
                return ApiResult<Response>.NotFound("User Account Not Found");
            }
            var password = StringExtensionMethod.CreateMd5HashPassword(request.Password);
            var userSecurity = await _dbContext.UserSecurity.FirstOrDefaultAsync(c => c.Id == userProfile.Id && c.UserPassword == password && c.MustChangePassword == false);
            if (userSecurity == null)
            {
                return ApiResult<Response>.NotFound("User Account Not Found");
            }

            var claim = new[] { new Claim(JwtRegisteredClaimNames.Sub, userProfile.Username) };
            var signinKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:SigningKey"]));
            var expiredDateTime = DateTime.Now.AddMinutes(Convert.ToInt32(_configuration["Jwt:ExpiryInMinutes"]));
            var token = new JwtSecurityToken(issuer: _configuration["Jwt:Site"], audience: _configuration["Jwt:Site"], expires: expiredDateTime, signingCredentials: new SigningCredentials(signinKey, SecurityAlgorithms.HmacSha256));

            return ApiResult<Response>.Ok(new Response
            {
                msg = "Login Success",
                Token = new JwtSecurityTokenHandler().WriteToken(token),
                ExpiredToken = token.ValidTo,
                Username = userProfile.Username
            });
        }
    }
}
