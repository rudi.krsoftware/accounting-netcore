﻿using Authorization.API.Common.API;
using System;

namespace Authorization.API.Services.UserProfile.Login
{
    public class Response : BaseResponse
    {
        public string Token { get; set; }
        public DateTime ExpiredToken { get; set; }
        public string Username { get; set; }
    }
}
