﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;
using Authorization.API.Common.API;
using Authorization.API.Context;

namespace Authorization.API.Services.UserProfile.Get
{
    public class Handler : IRequestHandler<Request, ApiResult<Response>>
    {
        private readonly DataContext _dbContext;
        public Handler(DataContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<ApiResult<Response>> Handle(Request request, CancellationToken cancellationToken)
        {
            var userProfile = await _dbContext.UserProfile.FirstOrDefaultAsync(c => c.Id == new Guid(request.Id));
            if (userProfile == null)
            {
                return ApiResult<Response>.NotFound("Data User Profile not found!");
            }

            return ApiResult<Response>.Ok(new Response
            {
                UserProfileModel = userProfile,
                success = true,
                result = true,
            });
        }
    }
}
