﻿using MediatR;
using Authorization.API.Common.API;

namespace Authorization.API.Services.UserProfile.Delete
{
    public class Request : BaseDeleteRequest, IRequest<ApiResult<Response>>
    {
    }
}
