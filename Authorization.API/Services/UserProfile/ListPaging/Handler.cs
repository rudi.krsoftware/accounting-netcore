﻿using Authorization.API.Common.API;
using Authorization.API.Common.Manager;
using Authorization.API.Common.Parameter;
using Authorization.API.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading;
using System.Threading.Tasks;

namespace Authorization.API.Services.UserProfile.ListPaging
{
    public class Handler : BaseDataManager, IRequestHandler<Request, ApiResult<Response>>
    {
        private readonly DataContext _dbContext;
        public Handler(DataContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<ApiResult<Response>> Handle(Request request, CancellationToken cancellationToken)
        {
            var javascriptPagings = JsonConvert.DeserializeObject<List<JavascriptPaging>>(request.Sorters ?? "");
            var whereTermList = ListParameter.GetListParameter(request.Params);
            var whereterm = GetQueryParameterLinqV02(whereTermList.ToArray());
            var pagingCollection = PagingCollection.GetPagingCollection(Paging.Instance(request.Start, request.Limit, Paging.DEFAULT_SORT_DIRECTION, request.Sort), javascriptPagings);

            var userProfiles = await _dbContext.UserProfile.Where(whereterm, ListValue.ToArray()).OrderBy(PagingCollection.LinqPaging(pagingCollection)).Skip(pagingCollection.Default.Start).Take(pagingCollection.Default.Limit).ToListAsync();

            var totalCount = await _dbContext.UserProfile.Where(whereterm, ListValue.ToArray()).CountAsync();

            return ApiResult<Response>.Ok(new Response
            {
                ListUserProfile = userProfiles,
                msg = "Success",
                totalCount = totalCount,
                success = true,
                result = true,
            });
        }
    }
}
