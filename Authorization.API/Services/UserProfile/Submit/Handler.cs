﻿using Authorization.API.Common.API;
using Authorization.API.Common.Parameter;
using Authorization.API.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Authorization.API.Services.UserProfile.Submit
{
    public class Handler : IRequestHandler<Request, ApiResult<Response>>
    {
        private readonly DataContext _dbContext;
        public Handler(DataContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<ApiResult<Response>> Handle(Request request, CancellationToken cancellationToken)
        {
            try
            {
                using (var scope = _dbContext.Database.BeginTransaction())
                {
                    var id = Guid.NewGuid();
                    if (!string.IsNullOrEmpty(request.Id))
                    {
                        id = new Guid(request.Id);
                    }
                    var userProfile = await _dbContext.UserProfile.FirstOrDefaultAsync(c => c.Id == id);
                    if (userProfile != null)
                    {
                        var haveUser = await _dbContext.UserProfile.AnyAsync(c => c.Username == request.Username && c.Id != id);
                        if (haveUser)
                        {
                            return ApiResult<Response>.ValidationError("Duplicate Username");
                        }
                        userProfile.Username = request.Username;
                        userProfile.Fullname = request.Fullname;
                        userProfile.Telephone = request.Telephone;
                        userProfile.Telephone2 = request.Telephone2;
                        userProfile.Address = request.Address;
                        userProfile.Email = request.Email;
                        userProfile.Sex = request.Sex;
                        //userProfile.RegisteredDate = request.RegisteredDate;
                        //userProfile.IsMobileUser = request.IsMobileUser;
                        //userProfile.DeviceId = request.DeviceId;
                        userProfile.ModifiedBy = request.User;
                        userProfile.ModifiedDate = DateTime.Now;
                        _dbContext.UserProfile.Update(userProfile);
                        await _dbContext.SaveChangesAsync();
                    }
                    else
                    {
                        var userProfileModel = new Context.UserProfile
                        {
                            Id = id,
                            RowStatus = 0,
                            Username = request.Username,
                            Fullname = request.Fullname,
                            Telephone = request.Telephone,
                            Telephone2 = request.Telephone2,
                            Address = request.Address,
                            Email = request.Email,
                            Sex = request.Sex,
                            RegisteredDate = request.RegisteredDate,
                            IsMobileUser = false,
                            DeviceId = "",
                            IsActive = true,
                            CreatedBy = request.User,
                            CreatedDate = DateTime.Now,
                            ModifiedBy = request.User,
                            ModifiedDate = DateTime.Now,
                        };
                        _dbContext.UserProfile.Add(userProfileModel);


                        var userPassword = StringExtensionMethod.CreateMd5HashPassword(request.UserPassword);
                        var userSecurityModel = new Context.UserSecurity
                        {
                            Id = id,
                            RowStatus = 0,
                            UserPassword = userPassword,
                            MustChangePassword = request.MustChangePassword,
                            SecurityQuestion = request.SecurityQuestion,
                            SecurityAnswer = request.SecurityAnswer,
                            CreatedBy = request.User,
                            CreatedDate = DateTime.Now,
                            ModifiedBy = request.User,
                            ModifiedDate = DateTime.Now,
                        };
                        _dbContext.UserSecurity.Add(userSecurityModel);

                        await _dbContext.SaveChangesAsync();
                    }

                    scope.Commit();

                    return ApiResult<Response>.Ok(new Response
                    {
                        msg = "Submit User Profile Success",
                        success = true,
                        result = true,
                    });
                }
            }
            catch (Exception ex)
            {
                return ApiResult<Response>.ErrorDuringProcessing(ex.Message);
            }
        }
    }
}
