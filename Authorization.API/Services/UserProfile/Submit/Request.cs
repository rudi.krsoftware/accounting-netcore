﻿using MediatR;
using System;
using Authorization.API.Common.API;

namespace Authorization.API.Services.UserProfile.Submit
{
    public class Request : BaseSubmitRequest, IRequest<ApiResult<Response>>
    {
        public string Username { get; set; }
        public string Fullname { get; set; }
        public string Telephone { get; set; }
        public string Telephone2 { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public int Sex { get; set; }
        public DateTime RegisteredDate { get; set; }
        public bool IsMobileUser { get; set; }
        public string DeviceId { get; set; }
        public bool IsActive { get; set; }

        public string UserPassword { get; set; }
        public bool MustChangePassword { get; set; }
        public string SecurityQuestion { get; set; }
        public string SecurityAnswer { get; set; }
    }
}
