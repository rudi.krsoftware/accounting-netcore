﻿using MediatR;
using Authorization.API.Common.API;

namespace Authorization.API.Services.UserProfile.ChangePassword
{
    public class Request : BaseSubmitRequest, IRequest<ApiResult<Response>>
    {
        public string UserPassword { get; set; }
        public string CheckPassword { get; set; }
    }
}
