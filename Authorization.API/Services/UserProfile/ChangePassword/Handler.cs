﻿using Authorization.API.Common.API;
using Authorization.API.Common.Parameter;
using Authorization.API.Context;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Authorization.API.Services.UserProfile.ChangePassword
{
    public class Handler : IRequestHandler<Request, ApiResult<Response>>
    {
        private readonly DataContext _dbContext;
        public Handler(DataContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<ApiResult<Response>> Handle(Request request, CancellationToken cancellationToken)
        {
            try
            {
                using (var scope = _dbContext.Database.BeginTransaction())
                {
                    var id = new Guid(request.Id);
                    if(request.UserPassword != request.CheckPassword)
                    {
                        return ApiResult<Response>.NotFound("Password not match");
                    }
                    var userSecurity = await _dbContext.UserSecurity.FirstOrDefaultAsync(c => c.Id == id);
                    if (userSecurity == null)
                    {
                        return ApiResult<Response>.NotFound("User Profile not found");
                    }
                    var userPassword = StringExtensionMethod.CreateMd5HashPassword(request.UserPassword);
                    userSecurity.UserPassword = userPassword;
                    userSecurity.ModifiedBy = request.User;
                    userSecurity.ModifiedDate = DateTime.Now;
                    _dbContext.UserSecurity.Update(userSecurity);
                    await _dbContext.SaveChangesAsync();
                    scope.Commit();

                    return ApiResult<Response>.Ok(new Response
                    {
                        msg = "Change User Password Success",
                        success = true,
                        result = true,
                    });
                }
            }
            catch (Exception ex)
            {
                return ApiResult<Response>.ErrorDuringProcessing(ex.Message);
            }
        }
    }
}
