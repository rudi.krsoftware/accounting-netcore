﻿Ext.define('base.data.DataFilterStore', {
    extend: 'Ext.data.Store',
    fields: [
        'Id',
        'RowStatus',
        'RowVersion',
        'FilterName',
        'ObjectName',
        'FilterJson',
        'SortJson',
        { name: 'Shared', type: 'bool' },
        { name: 'SetAsDefaultParam', type: 'bool' },
        'CreatedBy',
        { name: 'CreatedDate', type: 'date' },
        'ModifiedBy',
        { name: 'ModifiedDate', type: 'date' },
        { name: 'Crud', type: 'int' }
    ],
    proxy: {
        type: 'ajax',
        url: '',
        reader: {
            type: 'json',
            rootProperty: 'ListUserDataFilter'
        }
    },
    remoteSort: true
});