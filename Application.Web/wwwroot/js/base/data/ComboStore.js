﻿Ext.define('base.data.ComboStore', {
    extend: 'Ext.data.Store',
    fields: [
        'Id',
        'Name',
        'Code'
    ],
    proxy: {
        type: 'ajax',
        url: url == null ? '' : url,
        reader: {
            type: 'json',
            root: root,
            idProperty: 'Id',
            totalProperty: 'totalCount'
        }
    },
    remoteSort: true
});