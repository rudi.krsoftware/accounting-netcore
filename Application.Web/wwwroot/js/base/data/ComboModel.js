﻿Ext.define('base.data.ComboModel', {
    extend: 'Ext.data.Model',
    fields: [
		 'Id',
		 'Name',
		 'Code'
    ],
    proxy: {
        type: 'ajax',
        url: '',
        reader: {
            type: 'json',
            root: 'ListCombo',
            idProperty: 'Id',
            totalProperty: 'totalCount'
        }
    }
});