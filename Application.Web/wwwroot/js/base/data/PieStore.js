﻿Ext.define('base.data.PieStore', {
    extend: 'Ext.data.Store',
    fields: [
        'Label',
        { name: 'Value', type: 'float' }
    ],
    proxy: {
        type: 'ajax',
        url: '',
        reader: {
            type: 'json',
            root: 'list'
        }
    },
    remoteSort: true
});