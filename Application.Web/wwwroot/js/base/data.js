﻿Ext.define('base.data', {
    statics: {
        getComboStore: function(root, url) {
            return Ext.create('Ext.data.ArrayStore', {
                remoteSort: true,
                fields: [
                        'Id',
                        'Name',
                        'Code'
                ],
                proxy: {
                    type: 'ajax',
                    url: url == null ? '' : url,
                    reader: {
                        type: 'json',
                        root: root,
                        idProperty: 'Id',
                        totalProperty: 'totalCount'
                    }
                }
            });
        }
    }
});