﻿Ext.define('base.view.MaintainForm', {
    extend: 'Ext.panel.Panel',
    iconCls: 'icon-default',
    layout: 'border',
    closable: true,
    islookup:false,
    initComponent: function () {
        var me = this;
        
        me.callParent(arguments);
    },
    defaultParam: function () {
        return { Kolom: "RowStatus", KolomDisplay: "RowStatus", DataType: "Double", Operator: "0", OperatorDisplay: "", Keyword1: "0", Keyword2: "", StringKeyword: "", NumberKeyword1: 0, NumberKeyword2: 0, DateKeyword1: null, DateKeyword2: null, LogicalOperator: "0", LogicalOperatorDisplay: "AND" };
    },
    loadState: function () {
        if (this.state != null && this.cgrid != null) this.cgrid.applyState(this.state);
    },
    resetPageIndexAfterSearch: function () {
        try {
            this.cgrid.dockedItems.items[1].moveFirst();
        } catch (err) {
            console.log(err);
        }
    }
});