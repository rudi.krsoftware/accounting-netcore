﻿Ext.define('base.view.FormParameter', {
    extend: 'Ext.window.Window',
    alias: 'widget.formparameter',
    height: 520,
    width: 640,
    iconCls: 'icon-filter',
    filterKey: 'DEFAULT',
    filterName: 'DEFAULT',
    initComponent: function (config) {
        var a = this;
        a.sortStore = Ext.create('Ext.data.ArrayStore', {
            fields: [
                { name: 'Kolom' },
                { name: 'KolomDisplay' },
                { name: 'Sorting' },
                { name: 'SortingDisplay' }
            ]
        });
        a.paramStore = Ext.create('Ext.data.ArrayStore', {
            fields: [
                { name: 'Kolom' },
                { name: 'KolomDisplay' },
                { name: 'DataType' },
                { name: 'Operator' },
                { name: 'OperatorDisplay' },
                { name: 'Keyword1' },
                { name: 'Keyword2' },
                { name: 'StringKeyword' },
                { name: 'NumberKeyword1', type: 'float' },
                { name: 'NumberKeyword2', type: 'float' },
                { name: 'DateKeyword1', type: 'date', dateFormat: 'd-M-y' },
                { name: 'DateKeyword2', type: 'date', dateFormat: 'd-M-y' },
                { name: 'LogicalOperator' },
                { name: 'LogicalOperatorDisplay' }
            ]
        });
        if (a.jparent != null && a.sortData != null) {
            a.sortStore.loadData(a.sortData);
        }
        if (a.jparent != null && a.paramData != null) {
            a.paramStore.loadData(a.paramData);
        }
        a.sortgrid = Ext.create('Ext.grid.Panel', {
            store: a.sortStore,
            anchor: '100% 100%',
            loadMask: true,
            height: 150,
            region: 'south',
            stripeRows: true,
            enableColumnHide: false,
            enableHdMenu: false,
            enableDragDrop: false,
            columns: [
                {
                    xtype: 'actioncolumn',
                    width: 29,
                    items: [{
                        icon: 'Content/images/delete.png',
                        tooltip: 'Remove',
                        handler: function (grid, rowIndex) {
                            grid.store.removeAt(rowIndex);
                        }
                    }]
                }, { header: 'Kolom', width: 170, sortable: false, dataIndex: 'KolomDisplay' },
                { header: 'Sorting', width: 130, sortable: false, dataIndex: 'SortingDisplay' }
            ]
        });
        a.paramgrid = Ext.create('Ext.grid.Panel', {
            store: a.paramStore,
            anchor: '100% 100%',
            loadMask: true,
            height: 150,
            layout: 'fit',
            region: 'center',
            stripeRows: true,
            enableColumnHide: false,
            enableHdMenu: false,
            enableDragDrop: false,
            columns: [
                {
                    xtype: 'actioncolumn',
                    width: 29,
                    items: [{
                        icon: 'Content/images/delete.png',
                        tooltip: 'Remove',
                        handler: function (grid, rowIndex) {
                            grid.store.removeAt(rowIndex);
                        }
                    }]
                },
                { header: 'Kolom', width: 170, dataIndex: 'KolomDisplay', sortable: false },
                { header: 'Operator', width: 125, dataIndex: 'OperatorDisplay', sortable: false },
                { header: 'Keyword 1', width: 135, dataIndex: 'Keyword1', sortable: false },
                { header: 'Keyword 2', width: 108, dataIndex: 'Keyword2', sortable: false },
                { header: 'Logical', width: 60, dataIndex: 'LogicalOperatorDisplay', sortable: false }
            ]
        });
        a.baddparam = Ext.create('Ext.button.Button', { text: 'Add Parameter', iconCls: 'icon-add-filter', scale: 'medium' });
        a.baddsort = Ext.create('Ext.button.Button', { text: 'Add Sort', iconCls: 'icon-add-sort', scale: 'medium' });
        a.bclear = k.btn.clear({ text: 'Clear',iconCls: 'icon-remove-all' });
        a.bok = Ext.create('Ext.button.Button', { text: 'OK', iconCls: 'icon-ok', scale: 'medium' });
        a.bclose = Ext.create('Ext.button.Button', { text: 'Close', iconCls: 'icon-cancel', scale: 'medium' });
        a.bclearparam = k.btn.clear({ text: 'Clear Parameter', iconCls: 'icon-clear-filter' });
        a.bclearsort = k.btn.clear({ text: 'Clear Sorting', iconCls: 'icon-clear-sort' });
        a.bsaveparam = k.btn.save({ text: 'Simpan Parameter' });
        a.bloadparam = k.btn.exportData({ text: 'Load Parameter', iconCls: 'icon-load-filter' });
        a.firstKeyword = Ext.create('Ext.form.field.Text', { hideMode: 'visibility', margin :'0 3 0 0', hidden: true, emptyText: 'Isi Keyword Karakter', name: 'FirstKeyword', value: '', anchor: '100%' });
        a.secondKeyword = Ext.create('Ext.form.field.Text', { hideMode: 'visibility', hidden: true, emptyText: 'Isi Keyword Karakter', name: 'SecondKeyword', value: '', anchor: '100%' });
        a.firstNumber = Ext.create('Ext.form.field.Number', { hideMode: 'visibility', margin :'0 3 0 0', hidden: true, emptyText: 'Isi Keyword Angka', name: 'FirstNumber', value: '', anchor: '100%' , hideTrigger: true});
        a.secondNumber = Ext.create('Ext.form.field.Number', { hideMode: 'visibility', margin: '0 3 0 0', hidden: true, emptyText: 'Isi Keyword Angka', name: 'SecondNumber', value: '', anchor: '100%', hideTrigger: true });
        a.firstDate = Ext.create('Ext.form.field.Date', { hideMode: 'visibility', margin :'0 3 0 0', hidden: true, emptyText: 'Isi Keyword Tanggal', name: 'FirstDate', value: '', anchor: '100%' });
        a.secondDate = Ext.create('Ext.form.field.Date', { hideMode: 'visibility', margin :'0 3 0 0', hidden: true, emptyText: 'Isi Keyword Tanggal', name: 'SecondDate', value: '', anchor: '100%' });
        a.andLabel = Ext.create('Ext.form.field.Display', { hideMode: 'visibility', margin :'0 3 0 0', flex: .2, border: false, hidden: true, value: '&nbsp; Dan &nbsp;' });
        a.comboKeyword = Ext.create('Ext.form.field.ComboBox', {
            hideMode: 'visibility',
            hidden: true,
            margin :'0 3 0 0',
            emptyText: 'Pilih ....',
            name: 'ComboKeyword',
            store: Ext.create('Ext.data.ArrayStore', {
                fields: ['Id', 'Name', 'IdType']
            }),
            valueField: 'Id',
            displayField: 'Name',
            typeAhead: false,
            editable: false,
            mode: 'local',
            forceSelection: false,
            triggerAction: 'all',
            anchor: '100%'
        });
        a.boolKeyword = Ext.create('Ext.form.field.Checkbox', { hideMode: 'visibility', margin: '0 3 0 0', hidden: true, fieldLabel: 'Is True?', name: 'boolKeyword', anchor: '100%', inputValue: true, checked: false });

        a.logicalOperator = Ext.create('Ext.form.field.ComboBox', {
            name: 'LogicalOperator',
            fieldLabel: 'Logical Operator',
            store: Ext.create('Ext.data.ArrayStore', {
                fields: ['field', 'text'],
                data: [
                    ['0', 'AND'],
                    ['1', 'OR']
                ]
            }),
            valueField: 'field',
            displayField: 'text',
            typeAhead: false,
            editable: false,
            mode: 'local',
            forceSelection: false,
            triggerAction: 'all',
            emptyText: 'Pilih Logical Operator ...',
            anchor: '100%'
        });
        a.sortDirection = Ext.create('Ext.form.field.ComboBox', {
            name: 'SortDirection',
            fieldLabel: 'Sorting Operator',
            store: Ext.create('Ext.data.ArrayStore', {
                fields: ['field', 'text'],
                data: [
                    ['0', 'ASCENDING (A-Z)'],
                    ['1', 'DESCENDING (Z-A)']
                ]
            }),
            valueField: 'field',
            displayField: 'text',
            typeAhead: false,
            editable: false,
            mode: 'local',
            forceSelection: false,
            triggerAction: 'all',
            emptyText: 'Pilih Sorting Operator ...',
            anchor: '100%'
        });
        a.columnName = Ext.create('Ext.form.field.ComboBox', {
            name: 'ColumnNama',
            fieldLabel: 'Kolom',
            store: Ext.create('Ext.data.ArrayStore', {
                fields: ['text', 'field', 'type', 'json'],
                data: a.dataColumn
            }),
            valueField: 'field',
            displayField: 'text',
            typeAhead: false,
            editable: false,
            mode: 'local',
            forceSelection: false,
            triggerAction: 'all',
            emptyText: 'Pilih Kolom ...',
            anchor: '100%',
            listeners: {
                'select': function (combo, record) {
                    a.currentDataType = record.data.type;
                    switch (record.data.type.toLowerCase()) {
                        case "double":
                            a.isDoubleType = true;
                            a.stringOperator.hide();
                            a.firstKeyword.hide();
                            a.numberOperator.show();
                            a.firstNumber.show();
                            if (a.numberOperator.getValue() == '9') {
                                a.secondNumber.show();
                                a.andLabel.show();
                            } else {
                                a.andLabel.hide();
                                a.secondNumber.hide();
                            }
                            a.firstDate.hide();
                            a.secondDate.hide();
                            a.comboKeyword.hide();
                            a.comboOperator.hide();
                            a.boolKeyword.hide();
                            break;
                        case "decimal":
                            a.isDoubleType = true;
                            a.stringOperator.hide();
                            a.firstKeyword.hide();
                            a.numberOperator.show();
                            a.firstNumber.show();
                            if (a.numberOperator.getValue() == '9') {
                                a.secondNumber.show();
                                a.andLabel.show();
                            } else {
                                a.andLabel.hide();
                                a.secondNumber.hide();
                            }
                            a.firstDate.hide();
                            a.secondDate.hide();
                            a.comboKeyword.hide();
                            a.comboOperator.hide();
                            a.boolKeyword.hide();
                            break;
                        case "datetime":
                            a.isDoubleType = false;
                            a.stringOperator.hide();
                            a.firstKeyword.hide();
                            a.numberOperator.show();
                            a.firstDate.show();
                            if (a.numberOperator.getValue() == '9') {
                                a.secondDate.show();
                                a.andLabel.show();
                            } else {
                                a.andLabel.hide();
                                a.secondDate.hide();
                            }
                            a.firstNumber.hide();
                            a.secondNumber.hide();
                            a.comboKeyword.hide();
                            a.comboOperator.hide();
                            a.boolKeyword.hide();
                            break;
                        case "combo":
                            a.isDoubleType = false;
                            a.stringOperator.hide();
                            a.firstKeyword.hide();
                            a.numberOperator.hide();
                            a.firstDate.hide();
                            a.andLabel.hide();
                            a.secondDate.hide();
                            a.firstNumber.hide();
                            a.secondNumber.hide();
                            a.comboKeyword.show();
                            a.comboOperator.show();
                            a.comboKeyword.store.removeAll();
                            a.comboKeyword.store.add(record.data.json);
                            a.boolKeyword.hide();
                            break;
                        case "bool":
                            a.stringOperator.hide();
                            a.firstKeyword.hide();
                            a.numberOperator.hide();
                            a.firstNumber.hide();
                            a.secondNumber.hide();
                            a.andLabel.hide();
                            a.firstDate.hide();
                            a.secondDate.hide();
                            a.comboKeyword.hide();
                            a.comboOperator.hide();
                            a.boolKeyword.show();
                            break;
                        default:
                            a.stringOperator.show();
                            a.firstKeyword.show();
                            a.numberOperator.hide();
                            a.firstNumber.hide();
                            a.secondNumber.hide();
                            a.andLabel.hide();
                            a.firstDate.hide();
                            a.secondDate.hide();
                            a.comboKeyword.hide();
                            a.comboOperator.hide();
                            a.boolKeyword.hide();
                            break;
                    }
                    //a.stringField.doLayout();
                }
            }
        });
        a.stringOperator = Ext.create('Ext.form.field.ComboBox', {
            name: 'StringOperator',
            hideMode: 'visibility',
            margin :'0 3 0 0',
            store: Ext.create('Ext.data.ArrayStore', {
                fields: ['field', 'text'],
                data: [
                    ['0', 'Equal (=)'],
                    ['1', 'Not Equal (!=)'],
                    ['2', 'Begin With (|*)'],
                    ['3', 'End With (*|)'],
                    ['4', 'Like (*|*)']
                ]
            }),
            valueField: 'field',
            displayField: 'text',
            typeAhead: false,
            editable: false,
            mode: 'local',
            forceSelection: false,
            triggerAction: 'all',
            emptyText: 'Pilih Operator ...',
            anchor: '100%'
        });
        a.comboOperator = Ext.create('Ext.form.field.ComboBox', {
            name: 'ComboOperator',
            hideMode: 'visibility',
            margin :'0 3 0 0',
            hidden: true,
            store: Ext.create('Ext.data.ArrayStore', {
                fields: ['field', 'text'],
                data: [
                    ['0', 'Equal (=)']
                ]
            }),
            valueField: 'field',
            displayField: 'text',
            typeAhead: false,
            editable: false,
            mode: 'local',
            forceSelection: false,
            triggerAction: 'all',
            emptyText: 'Pilih Operator ...',
            value: 0,
            anchor: '100%'
        });
        a.numberOperator = Ext.create('Ext.form.field.ComboBox', {
            name: 'NumberOperator',
            hidden: true,
            margin :'0 3 0 0',
            hideMode: 'visibility',
            store: Ext.create('Ext.data.ArrayStore', {
                fields: ['field', 'text'],
                data: [
                    ['0', 'Equal (=)'],
                    ['1', 'Not Equal (!=)'],
                    ['5', 'Great Than (>)'],
                    ['6', 'Great Than Equal (>=)'],
                    ['7', 'Less Than (<)'],
                    ['8', 'Less Than Equal(<=)'],
                    ['9', 'Between'],
                    ['10', 'Is NULL']
                ]
            }),
            valueField: 'field',
            displayField: 'text',
            typeAhead: false,
            editable: false,
            mode: 'local',
            forceSelection: false,
            triggerAction: 'all',
            emptyText: 'Pilih Operator ...',
            anchor: '100%',
            listeners: {
                'select': function (combo, record) {
                    if (a.isDoubleType == true) {
                        if (record.data.field == '9') {
                            a.secondNumber.show();
                            a.andLabel.show();
                        } else {
                            a.andLabel.hide();
                            a.secondNumber.hide();
                        }
                    } else if (a.isDoubleType == false) {
                        if (record.data.field == '9') {
                            a.secondDate.show();
                            a.andLabel.show();
                        } else {
                            a.andLabel.hide();
                            a.secondDate.hide();
                        }
                    }

                    //a.stringField.doLayout();
                }
            }
        });

        a.stringField = Ext.create('Ext.form.FieldContainer', {
            fieldLabel: 'Operator',
            frame: false, border: false,
            defaults: {
                flex: 1, hideLabel: true
            },
            layout: 'hbox',
            items: [
                a.stringOperator, a.numberOperator, a.comboOperator, a.firstKeyword, a.firstNumber, a.firstDate, a.andLabel, a.secondNumber, a.secondDate, a.comboKeyword, a.boolKeyword
            ]
        });

        a.npanel = Ext.create('Ext.form.Panel', {
            region: 'north',
            bodyStyle: 'padding:10px 5px 0',
            layout: 'form',
            frame: false,
            height: 180,
            fieldDefaults: {
                msgTarget: 'side',
                labelWidth: 120
            },
            items: [a.columnName, a.stringField, a.logicalOperator, a.sortDirection],
            buttons: [a.bclearsort, a.bclearparam, a.baddsort, a.baddparam]
        });
        var b = Ext.apply({
            items: [a.npanel, a.paramgrid, a.sortgrid],
            height: 520,
            width: 650,
            buttons: [a.bloadparam, a.bsaveparam, a.bok, a.bclose]
        });
        Ext.apply(this, b, config);
        a.baddparam.on('click', a.onAddParameter, a);
        a.baddsort.on('click', a.onAddSorter, a);
        a.bclearsort.on('click', a.clearSortStore, a);
        a.bclearparam.on('click', a.clearParamStore, a);
        a.bclose.on('click', a.onClose, a);
        a.bsaveparam.on('click', a.onSaveParameter, a);
        a.bloadparam.on('click', a.onLoadParameter, a);

        a.logicalOperator.setValue('0');
        a.sortDirection.setValue('0');
        a.callParent(arguments);
    },
    onSaveParameter: function() {
        var me = this;
        var win = Ext.create('base.view.SaveParameterProfile');
        
        win.create(Ext.JSON.encode(me.getJsonParam()), Ext.JSON.encode(me.getJsonSort()), me.filterKey, me.filterName);
    },
    onLoadParameter: function () {
        var me = this;
        Ext.state.Manager.provider.ntongDisimpenHeula = true;
        UserObjectLayout.GetStates('userdatafilterlookup;userdatafilteranotheruserlookup', function (a, response) {
            var svalue = response.result.result.list;
            var state, state2;
            if (svalue[0] !== null) {
                state = k.state.decodeValue(svalue[0].ObjectValue);
            }
            if (svalue[1] != null) {
                state2 = k.state.decodeValue(svalue[1].ObjectValue);
            }
            var list = Ext.create('base.view.LoadParameterProfile', { state: state, state2: state2, keytag: 'userdatafilterlookup', keytag2: 'userdatafilteranotheruserlookup' });
            list.openLoad(me.filterKey);
            list.on('onSelectedItemLookup', function (cdata) {
                try {
                    me.paramStore.loadData(Ext.JSON.decode(cdata.data.FilterJson));
                    me.sortStore.loadData(Ext.JSON.decode(cdata.data.SortJson));
                    if (me.jparent != null && me.jparent.onRefreskClicked != null) {
                        me.jparent.jparams = me.getJsonParam();
                        me.jparent.jsorts = me.getJsonSort();
                        me.jparent.onRefreskClicked();
                    }
                } catch (err) {
                    k.msg.error(err);
                }
            }, me);
            Ext.state.Manager.provider.ntongDisimpenHeula = false;
        });
        
    },
    sortRecord: function () {
        return Ext.data.Record.create([{ name: 'Kolom' }, { name: 'KolomDisplay' }, { name: 'Sorting' }, { name: 'SortingDisplay' }]);
    },
    paramRecord: function () {
        return Ext.data.Record.create([
            { name: 'Kolom' },
            { name: 'KolomDisplay' },
            { name: 'DataType' },
            { name: 'Operator' },
            { name: 'OperatorDisplay' },
            { name: 'Keyword1' },
            { name: 'Keyword2' },
            { name: 'StringKeyword' },
            { name: 'NumberKeyword1', type: 'float' },
            { name: 'NumberKeyword2', type: 'float' },
            { name: 'DateKeyword1', type: 'date' },
            { name: 'DateKeyword2', type: 'date' },
            { name: 'LogicalOperator' },
            { name: 'LogicalOperatorDisplay' }
        ]);
    },
    islookup: false,
    onClose: function () {
        var a = this;
        if (a.jparent != null) {

            if (a.sortStore.getCount() > 0) {
                a.jparent.sortData = a.getArraySort();
            }
            if (a.paramStore.getCount() > 0) {
                a.jparent.paramData = a.getArrayParam();
            }

        }
        a.close();
    },
    getArraySort: function () {
        var a = this;
        var fff = new Array();
        a.sortStore.each(function (q) {
            fff.push([q.data.Kolom, q.data.KolomDisplay, q.data.Sorting, q.data.SortingDisplay]);
        }, this);
        return fff;
    },
    getJsonSort: function () {
        var a = this;
        var fff = new Array();
        a.sortStore.each(function (q) {
            fff.push({ Kolom: q.data.Kolom, KolomDisplay: q.data.KolomDisplay, Sorting: q.data.Sorting, SortingDisplay: q.data.SortingDisplay });
        }, this);
        return fff;
    },
    getArrayParam: function () {
        var a = this;
        var aaa = new Array();
        a.paramStore.each(function (q) {
            aaa.push([q.data.Kolom, q.data.KolomDisplay, q.data.DataType, q.data.Operator, q.data.OperatorDisplay, q.data.Keyword1, q.data.Keyword2, q.data.StringKeyword, q.data.NumberKeyword1, q.data.NumberKeyword2, q.data.DateKeyword1, q.data.DateKeyword2, q.data.LogicalOperator, q.data.LogicalOperatorDisplay]);
        }, this);
        return aaa;
    },
    getJsonParam: function () {
        var a = this;
        var aaa = new Array();
        a.paramStore.each(function (q) {
            aaa.push({ Kolom: q.data.Kolom, KolomDisplay: q.data.KolomDisplay, DataType: q.data.DataType, Operator: q.data.Operator, OperatorDisplay: q.data.OperatorDisplay, Keyword1: q.data.Keyword1, Keyword2: q.data.Keyword2, StringKeyword: q.data.StringKeyword, NumberKeyword1: q.data.NumberKeyword1, NumberKeyword2: q.data.NumberKeyword2, DateKeyword1: q.data.DateKeyword1, DateKeyword2: q.data.DateKeyword2, LogicalOperator: q.data.LogicalOperator, LogicalOperatorDisplay: q.data.LogicalOperatorDisplay });
        }, this);
        return aaa;
    },
    clearSortStore: function () {
        this.sortStore.removeAll();
    },
    clearParamStore: function () {
        this.paramStore.removeAll();
    },
    addCharRecord: function () {
        var a = this;
        return {
            Kolom: a.columnName.getValue(),
            KolomDisplay: a.columnName.getRawValue(),
            DataType: a.currentDataType,
            Operator: a.stringOperator.getValue(),
            OperatorDisplay: a.stringOperator.getRawValue(),
            Keyword1: a.firstKeyword.getValue(),
            Keyword2: a.secondKeyword.getValue(),
            StringKeyword: a.firstKeyword.getValue(),
            LogicalOperator: a.logicalOperator.getValue(),
            LogicalOperatorDisplay: a.logicalOperator.getRawValue()
        };
    },
    addNumberRecord: function () {
        var a = this;
        return {
            Kolom: a.columnName.getValue(),
            KolomDisplay: a.columnName.getRawValue(),
            DataType: a.currentDataType,
            Operator: a.numberOperator.getValue(),
            OperatorDisplay: a.numberOperator.getRawValue(),
            Keyword1: Ext.util.Format.number(a.firstNumber.getValue(), '0,000'),
            Keyword2: Ext.util.Format.number(a.secondNumber.getValue(), '0,000'),
            NumberKeyword1: a.firstNumber.getValue(),
            NumberKeyword2: a.secondNumber.getValue(),
            LogicalOperator: a.logicalOperator.getValue(),
            LogicalOperatorDisplay: a.logicalOperator.getRawValue()
        };
    },
    addDateRecord: function () {
        var a = this;
        return {
            Kolom: a.columnName.getValue(),
            KolomDisplay: a.columnName.getRawValue(),
            DataType: a.currentDataType,
            Operator: a.numberOperator.getValue(),
            OperatorDisplay: a.numberOperator.getRawValue(),
            Keyword1: Ext.util.Format.date(a.firstDate.getValue(), 'd-M-y'),
            Keyword2: Ext.util.Format.date(a.secondDate.getValue(), 'd-M-y'),
            DateKeyword1: a.firstDate.getValue(),
            DateKeyword2: a.secondDate.getValue(),
            LogicalOperator: a.logicalOperator.getValue(),
            LogicalOperatorDisplay: a.logicalOperator.getRawValue()
        };
    },
    addComboRecord: function () {
        //if ()
    },
    addBoolRecord: function () {
        var a = this;
        return {
            Kolom: a.columnName.getValue(),
            KolomDisplay: a.columnName.getRawValue(),
            DataType: a.currentDataType,
            Operator: 0,
            OperatorDisplay: "Equal (=)",
            Keyword1: a.boolKeyword.getValue(),
            Keyword2: a.secondKeyword.getValue(),
            StringKeyword: a.firstKeyword.getValue(),
            LogicalOperator: a.logicalOperator.getValue(),
            LogicalOperatorDisplay: a.logicalOperator.getRawValue()
        };
    },

    onAddParameter: function () {
        var a = this;
        if (a.currentDataType == null) return;
        switch (a.currentDataType.toLowerCase()) {
            case 'string':
                if (a.stringOperator.getValue() == null || a.firstKeyword.getValue() == '') return;
                a.paramgrid.store.add(a.addCharRecord());
                break;
            case 'combo':
                if (a.comboOperator.getValue() == null || a.comboKeyword.getValue() == '') return;
                a.paramgrid.store.add(a.addCharRecord());
            case 'double':
                switch (a.numberOperator.getValue()) {
                    case '9':
                        if (a.numberOperator.getValue() == null || a.firstNumber.getValue() == null) return;
                        if (a.numberOperator.getValue() == '9' && a.secondNumber.getValue() == null) return;
                        a.paramgrid.store.add(a.addNumberRecord());
                        break;
                    case '10':
                        if (a.numberOperator.getValue() == null) return;
                        a.paramgrid.store.add(a.addNumberRecord());
                        break;
                    default:
                        if (a.numberOperator.getValue() == null || a.firstNumber.getValue() == null) return;
                        a.paramgrid.store.add(a.addNumberRecord());
                        break;
                }
                break;
            case 'decimal':
                switch (a.numberOperator.getValue()) {
                    case '9':
                        if (a.numberOperator.getValue() == null || a.firstNumber.getValue() == null) return;
                        if (a.numberOperator.getValue() == '9' && a.secondNumber.getValue() == null) return;
                        a.paramgrid.store.add(a.addNumberRecord());
                        break;
                    case '10':
                        if (a.numberOperator.getValue() == null) return;
                        a.paramgrid.store.add(a.addNumberRecord());
                        break;
                    default:
                        if (a.numberOperator.getValue() == null || a.firstNumber.getValue() == null) return;
                        a.paramgrid.store.add(a.addNumberRecord());
                        break;
                }
                break;
            case 'DateTime':
                switch (a.numberOperator.getValue()) {
                    case '9':
                        if (a.numberOperator.getValue() == null || a.firstDate.getValue() == null) return;
                        if (a.numberOperator.getValue() == '9' && a.secondDate.getValue() == null) return;
                        a.paramgrid.store.add(a.addDateRecord());
                        break;
                    case '10':
                        if (a.numberOperator.getValue() == null) return;
                        a.paramgrid.store.add(a.addDateRecord());
                        break;
                    default:
                        if (a.numberOperator.getValue() == null || a.firstDate.getValue() == null) return;
                        a.paramgrid.store.add(a.addDateRecord());
                        break;
                }
                break;
            case 'bool':
                a.paramgrid.store.add(a.addBoolRecord());
                break;
        }
    },
    onAddSorter: function () {
        var a = this;
        if (a.columnName.getValue() == '' || a.sortDirection.getValue() == '') return;
        a.sortgrid.store.add({ Kolom: a.columnName.getValue(), KolomDisplay: a.columnName.getRawValue(), Sorting: a.sortDirection.getValue(), SortingDisplay: a.sortDirection.getRawValue() });
    }
});