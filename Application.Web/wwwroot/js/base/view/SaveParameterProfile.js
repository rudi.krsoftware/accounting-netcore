﻿Ext.define('base.view.SaveParameterProfile', {
    extend: 'base.view.EditorForm',
    title: 'Profile Filter Data',
    width: 420,
    height: 250,
    modal: true,
    layout: 'fit',
    collapsible: true,
    initComponent: function () {
        var me = this;
        me.buttonOk = k.btn.ok();
        me.buttonClose = k.btn.close();
        me.objId = Ext.create('Ext.form.field.Hidden', {
            name: 'Id'
        });
        me.RowStatus = Ext.create('Ext.form.field.Hidden', {
            name: 'RowStatus'
        });
        me.RowVersion = Ext.create('Ext.form.field.Hidden', {
            name: 'RowVersion'
        });
        me.FilterName = Ext.create('Ext.form.field.Text', {
            fieldLabel: 'Nama Profile',
            allowBlank: false,
            maxLength: 64,
            enforceMaxLength: true,
            name: 'FilterName',
            anchor: '96%'
        });
        me.ObjectName = Ext.create('Ext.form.field.Hidden', {
            name: 'ObjectName'
        });
        
        me.FilterJson = Ext.create('Ext.form.field.Hidden', {
            name: 'FilterJson'
        });
        me.SortJson = Ext.create('Ext.form.field.Hidden', {
            name: 'SortJson'
        });
        
        me.Shared = Ext.create('Ext.form.field.Checkbox', {
            boxLabel: 'Shared Ke Pengguna Lain',
            name: 'Shared',
            checked: true
        });
        me.SetAsDefaultParam = Ext.create('Ext.form.field.Checkbox', {
            boxLabel: 'Set Sebagai Filter Utama',
            name: 'SetAsDefaultParam',
            checked: false
        });
        me.cpanel = Ext.create('Ext.form.Panel', {
            items: [
                me.objId, me.RowStatus, me.RowVersion, me.FilterName, me.ObjectName, me.FilterJson, me.SortJson, me.Shared, me.SetAsDefaultParam
            ],
            buttons: [me.buttonOk, me.buttonClose]
        });

        Ext.applyIf(me, {
            items: [
                me.cpanel
            ]
        });
        me.callParent(arguments);
        me.buttonOk.on('click', me.onButtonOkClicked, me);
        me.buttonClose.on('click', me.closeForm, me);
    },
    closeForm: function () {
        var me = this;
        if (me.cpanel != null && me.cpanel.getForm != null && me.cpanel.getForm().isDirty()) {
            k.msg.ask(me.cancelmsg, function (btn) {
                if (btn === 'ok') {
                    me.close();
                }
            });
        } else {
            me.close();
        }
    },
    create: function (json, sort, key,name) {
        var me = this;
        me.show();

        me.cancelmsg = 'Batalkan Input Profile Filter ?';
        me.confirm = 'Simpan data Profile Filter ?';
        me.setTitle('Input Profile Filter');

        me.insertUrl = 'UserDataFilter/Insert';

        me.ObjectName.setValue(key);
        me.FilterName.setValue(name);
        me.FilterJson.setValue(json);
        me.SortJson.setValue(sort);
    },
    onButtonOkClicked: function () {
        var me = this;
        if (!me.cpanel.getForm().isValid()) {
            k.msg.warning('Pengisian Tidak Valid Di Form ini');
            return;
        }
        k.msg.ask(me.confirm, function (btn) {
            if (btn === 'ok') {
                var params = me.cpanel.getValues();
                k.msg.wait('Silahkan Tunggu, Proses Penyimpanan Data.....');
                try {
                    Ext.Ajax.request({
                        method: 'POST',
                        timeout: k.sys.timeout,
                        params: params,
                        url: me.insertUrl,
                        success: function (result) {
                            var jresult = Ext.JSON.decode(result.responseText);
                            if (jresult.result === false) {
                                if (jresult.severity === 1)
                                    k.msg.warning(jresult.msg);
                                else
                                    k.msg.error(jresult.msg);
                            } else {
                                k.msg.info(jresult.msg);
                                // ReSharper disable Html.EventNotResolved
                                me.fireEvent("afterInsertSuccessEvent");
                                // ReSharper restore Html.EventNotResolved
                                me.close();
                            }
                        },
                        failure: k.sys.ajaxFailure
                    });
                } catch (err) {
                    k.msg.error(err);
                }

            }
        });
    }
});