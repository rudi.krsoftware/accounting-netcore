﻿Ext.define('base.view.EditorForm', {
    extend: 'Ext.window.Window',
    iconCls: 'icon-default',
    layout: 'fit',
    height: 431,
    width: 440,
    fromButtonClose: false,
    //maxHeight: 650,
    //maxWidth: 820,
    animCollapse: true,
    animateTarget: document.id,
    cancelmsg: '',
    modal: true,
    maximizable: true,
    initComponent: function () {
        var me = this;
        
        me.btnOk = k.btn.ok();
        me.btnAction = k.btn.ok({ hidden: true, text: 'Action' });
        me.btnPrint = k.btn.ok({ hidden: true, text: 'Cetak' });
        me.btnClose = k.btn.close();
        me.instructionField = Ext.widget({
            xtype: 'fieldset',
            title: 'Intruksi',
            collapsible: true,
            defaults: {
                anchor: '100%'
            },
            html: 'Intruksi Cara Pengisisan Dengan HTML Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum'
        });
        me.btnOk.on('click', me.onButtonOkClicked, me);
        me.btnClose.on('click', me.closeForm, me);
        //me.buttonApprove = k.btn.approve({ hidden: true });
        //me.buttonRevise = k.btn.revise({ hidden: true });
        //me.buttonReject = k.btn.reject({ hidden: true });
        me.dontCloseForm = Ext.create('Ext.form.field.Checkbox', {
            checked: true,
            boxLabel: 'Tutup Setelah Penyimpanan Data'
        });
        me.resetForm = Ext.create('Ext.form.field.Checkbox', {
            checked: true,
            boxLabel: 'Clear Form Setelah Penyimpanan Data'
        });
        me.topToolbar = Ext.create('Ext.toolbar.Toolbar', {
            dock: 'top',
            hidden: true,
            items: [me.dontCloseForm, { xtype: 'tbseparator' }, me.resetForm]
        });
        Ext.applyIf(me, {
            dockedItems: [me.topToolbar],
            buttons: [/*me.buttonApprove, me.buttonRevise, me.buttonReject,*/ me.btnPrint, me.btnAction, me.btnOk, me.btnClose]
        });
        /*Ext.applyIf(me, {
            dockedItems: [
                //me.topToolbar 
                /*{
                xtype: 'toolbar',
                dock: 'top',
                items: [me.dontCloseForm, { xtype: 'tbseparator' }, me.resetForm]
            }#1# {
                    xtype: 'toolbar',
                    flex: 1,
                    dock: 'bottom',
                    ui: 'footer',
                    layout: {
                                pack: 'end',
                                type: 'hbox'
                            },
                    items: [
                        me.buttonApprove, me.buttonRevise, me.buttonReject, me.btnPrint, me.btnAction, me.btnOk, me.btnClose
                    ]
                }
            ]
            //buttons: [me.buttonApprove, me.buttonRevise, me.buttonReject,me.btnPrint, me.btnAction, me.btnOk, me.btnClose]

        });*/
        //me.addEvents('afterInsertSuccessEvent');
        //me.addEvents('onresetformischecked');
        //me.on('beforeclose', me.onBeforeClosingForm, me);
        me.callParent(arguments);
    },
    closeForm: function () {
        var me = this;
        me.fromButtonClose = true;
        if (me.cancelmsg === '') {
            me.close();
            return;
        }
        if (me.cpanel != null && me.cpanel.getForm != null && me.cpanel.getForm().isDirty()) {
            k.msg.ask(me.cancelmsg, function (btn) {
                if (btn === 'ok') {
                    me.close();
                }
            });
        } else {
            me.close();
        }
    },
    onBeforeClosingForm: function () {
        if (this.fromButtonClose === true) return true;
        this.fireEvent("onresetformischecked", this.resetForm.getValue());
        return this.dontCloseForm.getValue();
    },
    onButtonOkClicked: function () {
        var me = this;
        if (me.cpanel != null && !me.cpanel.getForm().isValid()) {
            k.msg.warning('Pengisian Form Tidak Valid, Cek Tanda Merah di setiap Isian');
            return;
        }
    },
    showApprovalButton: function () {
        this.buttonApprove.show();
        this.buttonRevise.show();
        this.buttonReject.show();
    }
});