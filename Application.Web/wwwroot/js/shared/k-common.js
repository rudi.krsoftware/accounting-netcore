﻿var k = k || {};
Ext.define('k.app', {
    statics: {
        apiBoUrl: 'http://localhost:5300/',
        apiAccountingUrl: 'http://localhost:5200/'
    }
});

function kdownload(url, fileName) {
    var centerWidth = (window.screen.width - 810) / 2;
    var centerHeight = (window.screen.height - 610) / 2;
    var strFeature = 'height=560,';
    strFeature += 'width=810,';
    strFeature += 'left=' + centerWidth + ',';
    strFeature += 'top=' + centerHeight + ',';
    strFeature += 'center=yes,';
    strFeature += 'status=no,';
    strFeature += 'help=no,';
    strFeature += 'resizable=yes,';
    strFeature += 'scrollbars=yes,';
    strFeature += 'menubar=yes';
    var modalWin = window.open('Download?filename=' + fileName + '&surl=' + url, "", strFeature);
    window.onfocus = function () {
        if (modalWin && !modalWin.closed)
            modalWin.focus();
    };
}

Ext.define('k.format', {
    statics: {
        date: 'Y-m-d',
        datetime: 'Y-m-d H:i:s',
        decimal: '0,000.0000',
        number: '0,000.00',
        renderdate: Ext.util.Format.dateRenderer('Y-m-d'),
        renderdatetime: Ext.util.Format.dateRenderer('Y-m-d H:i:s'),
        rendernumber: Ext.util.Format.numberRenderer('0,000.00'),
        renderdecimal: Ext.util.Format.numberRenderer('0,000.0000')
    }
});
Ext.define('k.array', {
    statics: {
        contains: function (item, list) {
            for (var v = 0; v < list.length; v++) {
                if (list[v] === item) {
                    return true;
                }
            }
            return false;
        }
    }
});
Ext.define('k.sys', {
    statics: {
        version: '0.0.1',
        codeName: 'shangyang',
        timeout: 720000,
        ajaxFailure: function (result) {
            try {
                if (result.responseText !== '' ^ result.responseText != null) {
                    if (result.statusText === 'transaction aborted') {
                        k.msg.error('Network connection time out');
                    }
                    else if (result.responseText.indexOf('html')) {
                    } else {
                        k.msg.error('Error connection, server could not respond');
                    }
                } else {
                    k.msg.errorResponseText(result.responseText);
                }
            } catch (exception) {
                k.msg.error(result);
            }
            
        },
        console: function(msg) {
            if (!Ext.isIE)
                console.log(new Date() + ' - ' + msg);
        },
        loadCallback: function (records, operation, success) {
            if (!success) {
                if (operation.error.indexOf('Expired Page') > 0)
                    window.location = "Expired";
                else
                    k.msg.errorResponseText(operation.error);
            }
        },
        checkJson: function(result) {
            if (result != null) {
                if (result.responseText.indexOf('Expired Page') > 0) {
                    window.location = "Expired";
                    return;
                }
            }

        },
        checkJsonForm: function(result, e) {
            if (e != null) {
                if (e.response != null) {
                    if (e.response.responseText.indexOf('Expired Page') > 0) {
                        window.location = "Expired";
                        return;
                    }
                }
            }

        },
        storeLoadCallback: function (records, operation, success) {
            if (success === false) {
                var isjson = k.sys.isJson(operation.serverResponse.responseText);
                if (isjson) {
                    var jresult = Ext.JSON.decode(operation.serverResponse.responseText);
                    if (jresult.result === false) {
                        if (jresult.severity === 1)
                            k.msg.warning(jresult.msg);
                        else
                            k.msg.error(jresult.msg);
                    } else {
                        if (fn != null) {
                            fn();
                        }
                    }
                } else {
                    k.msg.html(operation.serverResponse.responseText, function(btn) {
                        if (btn !== 'ok') return;
                        window.location.href = window.location.origin + '/Expired';
                    });
                }
            }
        },
        isJson: function(value) {
            try {
                JSON.parse(value);
            } catch (e) {
                return false;
            }
            return true;
        }
    }
});
Ext.define('k.util', {
    statics: {
        setViews: function (ctl) {
            for (var i = 0; i < ctl.length; i++) {
                ctl[i].setReadOnly(true);
                ctl[i].inputEl.dom.style.background = '#DEDEDE';
            }
        }
        , setDefaultComboValue: function (combo) {
            if (!combo.hasListener('datachanged')) {
                combo.store.on('datachanged', function () {
                    if (combo.store.getCount() > 0)
                        if (combo.valueField === "Name")
                            combo.setValue(combo.store.data.items[0].data.Name);
                        else if (combo.valueField === "ID")
                            combo.setValue(combo.store.data.items[0].data.ID);
                });
            }
        }
        , setValueComboLoadData: function (combo, value) {
            if (!combo.hasListener('datachanged')) {
                combo.store.on('datachanged', function () {
                    if (combo.valueField === "ID" || combo.valueField === "Name")
                        combo.setValueCustom(value);
                    else
                        combo.setValue(value);
                });
            }
        }
    }
});
Ext.define('k.sys.data', {
    statics: {
        empty: function(root) {
            return "{\"totalCount\":\"1\", \"" + root + "\": []}";
        },
        getFilter: function(property, value) {
            return "{\"property\":\"" + property + "\",\"value\": \"" + value + "\"}";
        },
        dateRendered: function() {
            return Ext.util.Format.dateRenderer('d-m-Y');
        },
        defaultRowStatus: function() {
            return [
                { Id: '0', Name: 'Active', IdType: 'byte' }, { Id: '1', Name: 'Not Active', IdType: 'byte' },
                { Id: '2', Name: 'Processed', IdType: 'byte' },
                { Id: '99', Name: 'Unknown', IdType: 'byte' }
            ];
        },
        getJsonString: function(store) {
            var jarray = '';
            store.each(function(record) {
                jarray += Ext.JSON.encode(record.data) + ',';
            });
            jarray = jarray.substring(0, jarray.length - 1);
            return "[" + jarray + "]";
        },
        getJsonStringFromRecords: function (records) {
            var jarray = '';
            for (var i = 0; i < records.length; i++) {
                jarray += Ext.JSON.encode(records[i].data) + ',';
            }
            jarray = jarray.substring(0, jarray.length - 1);
            return "[" + jarray + "]";
        },
        getJsonStringFromRecord: function (record) {
            var jarray = '';
            jarray += Ext.JSON.encode(record);
            return "[" + jarray + "]";
        },
        newGuid: function() {
            var d = new Date().getTime();
            var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
                var r = (d + Math.random() * 16) % 16 | 0;
                d = Math.floor(d / 16);
                return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
            });
            return uuid;
        },
        renderDataComboGrid: function (val, metaData) {
            var result = '';
            if (val === null)
                return '';
            var combo = metaData.column.editor;
            if (combo) {
                combo.store.data.items.forEach(function (item) {
                    if (item.data[this.valueField] === val.toString())
                        result = item.data.value;
                }, combo);
            }
            if (result !== '')
                return result;
            return val;
        }
    }
});

Ext.define('k.msg', {
    statics: {
        wait: function(message, renderTo) {
            if (message == null || message === '')
                message = 'Please wait, Processing data...';
            k.msg.mask = Ext.Msg.show({
                msg: message,
                width: 300,
                wait: true,
                modal: true,
                interval: 200,
                closeable: true,
                //renderTo: renderTo == null ? document.body : renderTo,
                autoDestroy: true,
                alwaysOnTop: true
            });
            //new Ext.ZIndexManager().bringToFront(k.msg.mask);
            k.msg.mask.zIndexManager.bringToFront(k.msg.mask);
        },
        masking: function(msg) {
            k.msg.mask = new Ext.LoadMask(Ext.getBody(), { msg: (msg == null ? 'Please wait...' : msg) });
            k.msg.mask.show();
        },
        hide: function() {
            if (k.msg.mask == null) return;
            k.msg.mask.hide();
        },
        mask: null,
        msgctr: null,
        createBox: function(t, s) {
            return '<div class="msg"><h3>' + t + '</h3><p>' + s + '</p></div>';
            //return '<div class="x-mask" style="min-height:100%; min-width: 100%;  z-index: 9999999; right: auto; left: 0px; top: 0.047px;" class="msg"><h3>' + t + '</h3><p>' + s + '</p></div>'
        },
        notify: function(title, msg, delaytime) {
            if (!k.msg.msgctr) {
                k.msg.msgctr = Ext.DomHelper.insertFirst(document.body, { id: 'msg-div', style: 'z-index:9999999' }, true);
            }
            //var s = Ext.String.format.apply(String, Array.prototype.slice.call(arguments, 1));
            var m = Ext.DomHelper.append(k.msg.msgctr, k.msg.createBox(title, msg), true);
            m.hide();
            m.slideIn('t').ghost('t', { delay: (delaytime == null ? 2000 : delaytime), remove: true });
            k.msg.mask.zIndexManager.bringToFront(k.msg.mask);
        },
        createInboxMessage: function(inboxmodel) {
            k.msg.inboxmodel = inboxmodel;
            return "<div class='msg'><h3>INBOX NOTIFIKASI</h3><a href='javascript:k.msg.inboxfn(\"" + inboxmodel.UrlAction + "\", \"" + inboxmodel.JavascriptAction + "\", \"" + inboxmodel.ObjectId + "\", \"" + inboxmodel.Id + "\", \"" + inboxmodel.RequestNumber + "\")';><p>" + inboxmodel.Subject + "</p><a></div>";
            //return '<div class="x-mask" style="min-height:100%; min-width: 100%;  z-index: 9999999; right: auto; left: 0px; top: 0.047px;" class="msg"><h3>' + t + '</h3><p>' + s + '</p></div>'
        },
        inboxfn: function(urlAction, jsAction, objid, id, requestNumber){
            var inbox = Ext.create(urlAction);
            eval("inbox." + jsAction + "('" + objid + "', '" + id + "' , '" + requestNumber + "');");
            inbox.on('show', function () {
                InboxJob.SetHasView(id, function () {
                });
            });
            k.msg.mask.zIndexManager.bringToFront(k.msg.mask);
        },
        inboxnotify: function(delaytime, inboxmodel) {
            if (!k.msg.msgctr) {
                k.msg.msgctr = Ext.DomHelper.insertFirst(document.body, { id: 'msg-div', style: 'z-index:9999999' }, true);
            }
            //var s = Ext.String.format.apply(String, Array.prototype.slice.call(arguments, 1));
            var m = Ext.DomHelper.append(k.msg.msgctr, k.msg.createInboxMessage(inboxmodel), true);
            m.hide();
            m.slideIn('t').ghost('t', { delay: (delaytime == null ? 2500 : delaytime), remove: true });
            k.msg.mask.zIndexManager.bringToFront(k.msg.mask);
        },
        warning: function (message, fn) {
            k.msg.mask =  Ext.Msg.show({
                title: 'Warning',
                msg: message,
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.WARNING,
                fn: fn,
                width: 400,
                modal: true,
                alwaysOnTop: true
            });
            k.msg.mask.zIndexManager.bringToFront(k.msg.mask);
        },
        error: function(message, renderTo) {
            Ext.Msg.show({
                title: 'Error',
                msg: message,
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.ERROR,
                modal: true,
                renderTo: renderTo == null ? document.body : renderTo,
                width: 390
            });
            if(k.msg.mask) k.msg.mask.zIndexManager.bringToFront(k.msg.mask);
        },
        errorResponseText: function(message) {
            Ext.Msg.show({
                title: 'Error',
                msg: message,
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.ERROR,
                width: 700,
                modal: true,
                height: 400
            });
            k.msg.mask.zIndexManager.bringToFront(k.msg.mask);
        },
        info: function(message, fn) {
            k.msg.hide();
            Ext.Msg.show({
                title: 'Information',
                msg: message,
                fn: fn,
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.INFO,
                width: 300
            });
            k.msg.mask.zIndexManager.bringToFront(k.msg.mask);
        },
        ask: function(message, fn) {
            Ext.Msg.show({
                title: 'Confirmation',
                msg: message,
                buttons: Ext.MessageBox.OKCANCEL,
                icon: Ext.MessageBox.QUESTION,
                fn: fn,
                width: 400
            });
            //k.msg.mask.zIndexManager.bringToFront(k.msg.mask);
        },
        show: function(jresult, showMessageInfo, fn) {
            if (jresult.result === false) {
                if (jresult.severity === 1)
                    k.msg.warning(jresult.msg);
                else
                    k.msg.error(jresult.msg);
            } else {
                if (showMessageInfo === true) {
                    k.msg.info(jresult.msg);
                }
                if (fn != null) {
                    fn();
                }
            }
            k.msg.mask.zIndexManager.bringToFront(k.msg.mask);
        },
        storeCallbackInfo: function (records, operation, success) {
            if (success === true) return;
            var isjson;
            try {
                isjson = k.sys.isJson(operation.serverResponse.responseText);
            } catch(err) {
                isjson = false;
            }
            
            if (isjson === true) {
                var jresult = Ext.JSON.decode(operation.serverResponse.responseText);
                if (jresult.result === false) {
                    if (jresult.severity === 1)
                        k.msg.warning(jresult.msg);
                    else
                        k.msg.error(jresult.msg);
                } else {
                    if (fn != null) {
                        fn();
                    }
                }
            } else {
                Ext.Msg.show({
                    title: 'SESSION EXPIRED',
                    msg: 'Session Login Anda Sudah Habis, pilih OK untuk refresh browser secara otomatis, pilih CANCEL untuk refresh browser secara manual (Tekan F5)',
                    buttons: Ext.MessageBox.OKCANCEL,
                    icon: Ext.MessageBox.WARNING,
                    width: 500,
                    height: 90,
                    modal: true,
                    fn: function(btn) {
                        if (btn !== 'ok') return;
                        window.location.href = window.location.origin;
                    }
                });
            }
            k.msg.mask.zIndexManager.bringToFront(k.msg.mask);
        },
        html: function(message, fn) {
            Ext.Msg.show({
                title: 'Error',
                msg: message,
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.WARNING,
                width: 1024,
                height: 768,
                modal: true, 
                fn: fn
            });
            k.msg.mask.zIndexManager.bringToFront(k.msg.mask);
        }
    }
});

Ext.define('k.btn', {
    statics: {
        save: function (config) {
            var b = { text: 'Simpan', iconCls: 'icon-save', scale: 'medium' };
            var a = Ext.create('Ext.button.Button', { text: 'Simpan' });
            var c = Ext.apply(a, config, b);
            return c;
        },
        ok: function (config) {
            var b = { iconCls: 'icon-ok', scale: 'medium' };
            var a = Ext.create('Ext.button.Button', { text: 'OK' });
            var c = Ext.apply(a, config, b);
            return c;
        },
        draft: function (config) {
            var b = { iconCls: 'icon-ok', scale: 'medium' };
            var a = Ext.create('Ext.button.Button', { text: 'Save As Draft' });
            var c = Ext.apply(a, config, b);
            return c;
        },
        select: function (config) {
            var b = { text: 'Pilih', iconCls: 'glyph-select', scale: 'medium' };
            var a = Ext.create('Ext.button.Button', { text: 'Pilih' });
            var c = Ext.apply(a, config, b);
            return c;
        },
        search: function () {
            return Ext.create('Ext.button.Button', { text: 'Cari', scale: 'medium', iconCls: 'icon-search' });
        },
        cancel: function (config) {
            var b = { iconCls: 'icon-cancel', scale: 'medium' };
            var a = Ext.create('Ext.button.Button', { text: 'Batal' });
            var c = Ext.apply(a, config, b);
            return c;
        },
        close: function (config) {
            var b = { iconCls: 'icon-cancel', scale: 'medium' };
            var a = Ext.create('Ext.button.Button', { text: 'Tutup' });
            var c = Ext.apply(a, config, b);
            return c;
        },
        edit: function () {
            return Ext.create('Ext.button.Button', { text: 'Edit', iconCls: 'icon-edit', scale: 'medium' });
        },
        add: function (config) {
            var b = { iconCls: 'icon-add', scale: 'medium' };
            var a = Ext.create('Ext.button.Button', { text: 'Input' });
            var c = Ext.apply(a, config, b);
            return c;
        },
        erase: function (config) {
            var b = { iconCls: 'icon-delete', scale: 'medium' };
            var a = Ext.create('Ext.button.Button', { text: 'Hapus' });
            var c = Ext.apply(a, config, b);
            return c;
        },
        create: function () {
            return Ext.create('Ext.button.Button', { text: 'Input'/*, iconCls: 'icon-new'*/ });
        },
        refresh: function () {
            return Ext.create('Ext.button.Button', { text: 'Refresh', iconCls: 'icon-refresh', scale: 'medium' });
        },
        clear: function (config) {
            var b = { iconCls: 'icon-clear', scale: 'medium' };
            var a = Ext.create('Ext.button.Button', { text: 'Clear' });
            var c = Ext.apply(a, config, b);
            return c;
        },
        open: function () {
            return Ext.create('Ext.button.Button', { text: 'Buka', iconCls: 'icon-open', scale: 'medium' });
        },
        upload: function (config) {
            var b = { iconCls: 'icon-default', scale: 'medium' };
            var a = Ext.create('Ext.button.Button', { text: 'Unggah' });
            var c = Ext.apply(a, config, b);
            return c;
        },
        parameter: function () {
            return Ext.create('Ext.button.Button', { text: 'Parameter', scale: 'medium', iconCls: 'icon-parameter' });
        },
        exportData: function (config) {
            var b = { text: 'Export', iconCls: 'icon-export', scale: 'medium' };
            var a = Ext.create('Ext.button.Button');
            var c = Ext.apply(a, config, b);
            return c;
        },
        process: function (config) {
            var b = { /*iconCls: 'icon-process',*/ scale: 'medium' };
            var a = Ext.create('Ext.button.Button', { text: 'Process' });
            var c = Ext.apply(a, config, b);
            return c;
        },
        addParam: function () {
            return Ext.create('Ext.button.Button', { scale: 'medium' ,text: 'Add Parameter', iconCls: 'icon-add-param' });
        },
        addSort: function () {
            return Ext.create('Ext.button.Button', { scale: 'medium' , text: 'Add Sort', iconCls: 'icon-add-sort' });
        },
        browse: function (config) {
            var b = { text: '...', iconCls: 'icon-process' };
            var a = Ext.create('Ext.button.Button');
            var c = Ext.apply(a, config, b);
            return c;
        },
        approve: function (config) {
            var b = { iconCls: 'icon-approve', scale: 'medium' };
            var a = Ext.create('Ext.button.Button', {
                text: 'Setuju', /*style: {
                    background: '#E9FFEA'
                },*/ enableKeyEvents: true
            });
            var c = Ext.apply(a, config, b);
            return c;
        },
        revise: function (config) {
            var b = { iconCls: 'icon-revise', scale: 'medium' };
            var a = Ext.create('Ext.button.Button', {
                text: 'Revisi', /*style: {
                    background: '#CEFFF6'
                },*/ enableKeyEvents: true
            });
            var c = Ext.apply(a, config, b);
            return c;
        },
        reject: function (config) {
            var b = { iconCls: 'icon-reject', scale: 'medium' };
            var a = Ext.create('Ext.button.Button', {
                text: 'Tolak', /*style: {
                    background: '#FFE4E1'
                },*/ enableKeyEvents: true
            });
            var c = Ext.apply(a, config, b);
            return c;
        },
        print: function (config) {
            var b = { iconCls: 'icon-print', scale: 'medium' };
            var a = Ext.create('Ext.button.Button', {
                text: 'Print', /*style: {
                    background: '#FFE4E1'
                },*/ enableKeyEvents: true
            });
            var c = Ext.apply(a, config, b);
            return c;
        }
    }
});

Ext.define('k.defl', {
    statics: {
        blankText: 'Kolom ini harus diisi'
    }
});
k.crudEnum = {    
    selected: 0,
    newest: 1,
    edited: 2,
    deleted: 3
};

/*Ext.selection.CheckboxModel.override({
    hideCheckbox: function (grid) {
        this.tree.columns[0].setVisible(false);
        //var idx = cm.getIndexById(this.id);
        //cm.setHidden(idx, true);
    },
    showCheckbox: function () {
        var cm = this.grid.getColumnModel();
        var idx = cm.getIndexById(this.id);
        cm.setHidden(idx, false);
    }
});*/

Ext.form.field.ComboBox.override({
    setValueCustom: function (value, doSelect) {
        var me = this,
            valueNotFoundText = me.valueNotFoundText,
            inputEl = me.inputEl,
            i, len, record,
            dataObj,
            matchedRecords = [],
            displayTplData = [],
            processedValue = [];

        if (me.store.loading) {

            me.value = value;
            me.setHiddenValue(me.value);
            return me;
        }


        value = Ext.Array.from(value);

        for (i = 0, len = value.length; i < len; i++) {
            record = value[i];
            if (!record || !record.isModel) {
                //
                if (me.valueField === 'ID') {
                    me.store.each(function (rec) {
                        if (rec.data.ID === value)
                            record = rec;
                    });
                }
                else if (me.valueField === 'Name') {
                    me.store.each(function (rec) {
                        if (rec.data.ID === value)
                            record = rec;
                    });
                }
                else {
                    record = me.findRecordByValue(record);
                }
            }

            if (record) {
                matchedRecords.push(record);
                displayTplData.push(record.data);
                processedValue.push(record.get(me.valueField));
            }


            else {


                if (!me.forceSelection) {
                    processedValue.push(value[i]);
                    dataObj = {};
                    dataObj[me.displayField] = value[i];
                    displayTplData.push(dataObj);

                }

                else if (Ext.isDefined(valueNotFoundText)) {
                    displayTplData.push(valueNotFoundText);
                }
            }
        }


        me.setHiddenValue(processedValue);
        me.value = me.multiSelect ? processedValue : processedValue[0];
        if (!Ext.isDefined(me.value)) {
            me.value = null;
        }
        me.displayTplData = displayTplData;
        me.lastSelection = me.valueModels = matchedRecords;

        if (inputEl && me.emptyText && !Ext.isEmpty(value)) {
            inputEl.removeCls(me.emptyCls);
        }


        me.setRawValue(me.getDisplayValue());
        me.checkChange();

        if (doSelect !== false) {
            me.syncSelection();
        }
        me.applyEmptyText();

        return me;
    }
    , setMandatory: function () {
        if (this.inputEl == null)
            return;
        if (this.inputEl.dom == null)
            return;
        this.inputEl.dom.style.background = '#B5D0FF';
    }
    , blankText: k.defl.blankText
});
Ext.form.field.Trigger.override({
    setMandatory: function () {
        if (this.inputEl != null) this.inputEl.dom.style.background = '#B5D0FF';
    },
    blankText: k.defl.blankText
});
Ext.form.field.Base.override({
    msgTarget: 'side',
    mustUnique: false
});

Ext.form.field.Text.override({
    blankText: k.defl.blankText,
    initComponent: function () {
        this.callParent();
        if (this.mustUnique === true)
            this.fieldStyle = 'font-weight: bold; background-color: #ffe0e4 !important;';

    }
});

Ext.form.field.Date.override({
    blankText: k.defl.blankText
});

Ext.form.field.Number.override({
    blankText: k.defl.blankText,
    minValue: 0
});

Ext.form.field.File.override({
    blankText: k.defl.blankText
});

Ext.form.field.Checkbox.override({
    inputValue: '1',
    uncheckedValue: '0'
});

/*Ext.grid.Panel.override({
    columnLines: true,
    ui: 'green-panel-active'
});

Ext.form.Panel.override({
    ui: 'green-panel-active'
});

Ext.panel.Panel.override({
    ui: 'green-panel-active'
});

Ext.tab.Panel.override({
    ui: 'orange-tab-active'
});

Ext.window.Window.override({
    ui: 'red-window-active'
});*/

Ext.form.Panel.override({
    border: false,
    bodyPadding: '14',
    trackResetOnLoad: true,
    //collapsible : true,
    fieldDefaults: {
        msgTarget: 'side',
        labelWidth: 120
    }
});
Ext.form.FieldSet.override({
    collapsible : true
});
Ext.override(Ext.grid.Panel, {
    initComponent: function () {
        this.callParent();
    }
});

Ext.define('Ext.data.proxy.ServerOverride', {
    override: 'Ext.data.proxy.Server',

    processResponse: function (success, operation, request, response) {
        operation.serverResponse = response;
        this.callParent(arguments);
    }
});

Ext.define('Ext.form.field.LookupField', {
    extend: 'Ext.form.field.Text',
    alias: 'widget.lookupfield',
    trigger1Cls: Ext.baseCSSPrefix + 'form-clear-trigger',
    trigger2Cls: Ext.baseCSSPrefix + 'form-search-trigger',
    hasSearch: false,
    paramName: 'query',
    readOnly: true,
    showTriggerClear: false,
    triggers: {
        trigger1: {
            cls: Ext.baseCSSPrefix + 'form-search-trigger',
            handler: this.onTrigger1Click
        },
        bar: {
            cls: Ext.baseCSSPrefix + 'form-search-trigger',
            handler: function() {
                //console.log('bar trigger clicked');
                this.onTrigger1Click();
            }
        }
    },
    initComponent: function () {
        var me = this;

        me.callParent(arguments);
        me.on('specialkey', function (f, e) {
            if (e.getKey() === e.ENTER) {
                me.onTrigger2Click();
            }
        });


        // Set up the proxy to encode the filter in the simplest way as a name/value pair

        // If the Store has not been *configured* with a filterParam property, then use our filter parameter name
        /*if (!me.store.proxy.hasOwnProperty('filterParam')) {
            me.store.proxy.filterParam = me.paramName;
        }
        me.store.proxy.encodeFilters = function(filters) {
            return filters[0].value;
        };*/
    },

    afterRender: function () {
        this.callParent();
        if (this.showTriggerClear === false)
            this.triggerCell.item(0).setDisplayed(false);
    },

    onTrigger1Click: function () {
        var me = this;
        //console.log('trigger 1 clicked');
        if (me.hasSearch) {
            me.setValue('');
            //me.store.clearFilter();
            me.hasSearch = false;
            me.triggerCell.item(0).setDisplayed(false);
            me.updateLayout();
        }
    },

    onTrigger2Click: function () {
        var me = this,
            value = me.getValue();

        if (value.length > 0) {
            me.hasSearch = true;
            me.triggerCell.item(0).setDisplayed(true);
            me.updateLayout();
        }
    },
    getTriggerMarkup: function () {
        var me = this,
            i,
            hideTrigger = (/*me.readOnly ||*/ me.hideTrigger),
            triggerCls,
            triggerBaseCls = me.triggerBaseCls,
            triggerConfigs = [],
            unselectableCls = Ext.dom.Element.unselectableCls,
            style = 'width:' + me.triggerWidth + 'px;' + (hideTrigger ? 'display:none;' : ''),
            cls = me.extraTriggerCls + ' ' + Ext.baseCSSPrefix + 'trigger-cell ' + unselectableCls;

        // TODO this trigger<n>Cls API design doesn't feel clean, especially where it butts up against the
        // single triggerCls config. Should rethink this, perhaps something more structured like a list of
        // trigger config objects that hold cls, handler, etc.
        // triggerCls is a synonym for trigger1Cls, so copy it.
        if (!me.trigger1Cls) {
            me.trigger1Cls = me.triggerCls;
        }

        // Create as many trigger elements as we have trigger<n>Cls configs, but always at least one
        for (i = 0; (triggerCls = me['trigger' + (i + 1) + 'Cls']) || i < 1; i++) {
            triggerConfigs.push({
                tag: 'td',
                valign: 'top',
                cls: cls,
                style: style,
                cn: {
                    cls: [Ext.baseCSSPrefix + 'trigger-index-' + i, triggerBaseCls, triggerCls].join(' '),
                    role: 'button'
                }
            });
        }
        triggerConfigs[0].cn.cls += ' ' + triggerBaseCls + '-first';

        return Ext.DomHelper.markup(triggerConfigs);
    },
    onTriggerWrapClick: function () {
        var me = this,
            targetEl, match,
            triggerClickMethod;
        var event = arguments[me.triggerRepeater ? 1 : 0];
        if (event  /*!me.readOnly &&*/ /*!me.disabled*/) {
            targetEl = event.getTarget('.' + me.triggerBaseCls, null);
            match = targetEl && targetEl.className.match(me.triggerIndexRe);

            if (match) {
                triggerClickMethod = me['onTrigger' + (parseInt(match[1], 10) + 1) + 'Click'] || me.onTriggerClick;
                if (triggerClickMethod) {
                    triggerClickMethod.call(me, event);
                }
            }
        }
    },
    applyTriggers: function (triggers) {
        var me = this,
            hideAllTriggers = me.getHideTrigger(),
            readOnly = me.readOnly,
            orderedTriggers = me.orderedTriggers = [],
            repeatTriggerClick = me.repeatTriggerClick,
            id, triggerCfg, trigger, triggerCls, i;

        //<debug> 
        if (me.rendered) {
            Ext.raise("Cannot set triggers after field has already been rendered.");
        }

        // don't warn if we have both triggerCls and triggers, because picker field 
        // uses triggerCls to style the "picker" trigger. 
        if ((me.triggerCls && !triggers) || me.trigger1Cls) {
            Ext.log.warn("Ext.form.field.Text: 'triggerCls' and 'trigger<n>Cls'" +
                " are deprecated.  Use 'triggers' instead.");
        }
        //</debug> 

        if (!triggers) {
            // For compatibility with 4.x, transform the trigger<n>Cls configs into the 
            // new "triggers" config. 
            triggers = {};

            if (me.triggerCls && !me.trigger1Cls) {
                me.trigger1Cls = me.triggerCls;
            }

            // Assignment in conditional test is deliberate here 
            for (i = 1; (triggerCls = me['trigger' + i + 'Cls']) ; i++) { // jshint ignore:line 
                triggers['trigger' + i] = {
                    cls: triggerCls,
                    extraCls: Ext.baseCSSPrefix + 'trigger-index-' + i,
                    handler: 'onTrigger' + i + 'Click',
                    compat4Mode: true,
                    scope: me
                };
            }
        }
        for (id in triggers) {
            if (triggers.hasOwnProperty(id)) {
                triggerCfg = triggers[id];
                triggerCfg.field = me;
                triggerCfg.id = id;

                /*
                 * An explicitly-configured 'triggerConfig.hideOnReadOnly : false' allows {@link #hideTrigger} analysis
                 */
                if ((readOnly && triggerCfg.hideOnReadOnly !== false) || (hideAllTriggers && triggerCfg.hidden !== false)) {
                    triggerCfg.hidden = false;
                }
                if (repeatTriggerClick && (triggerCfg.repeatClick !== false)) {
                    triggerCfg.repeatClick = true;
                }

                trigger = triggers[id] = Ext.form.trigger.Trigger.create(triggerCfg);
                orderedTriggers.push(trigger);
            }
        }

        Ext.Array.sort(orderedTriggers, Ext.form.trigger.Trigger.weightComparator);

        return triggers;
    },
    setReadOnly: function (readOnly) {
        var me = this,
            triggers = me.getTriggers(),
            hideTriggers = me.getHideTrigger(),
            trigger,
            id;

        readOnly = !!readOnly;

        me.callParent([readOnly]);
        if (me.rendered) {
            me.setReadOnlyAttr(readOnly || !me.editable);
        }

        if (triggers) {
            for (id in triggers) {
                trigger = triggers[id];
                /*
                 * Controlled trigger visibility state is only managed fully when 'hideOnReadOnly' is falsy.
                 * Truth table:
                 *   - If the trigger is configured/defaulted as 'hideOnReadOnly : true', it's readOnly-visibility
                 *     is determined solely by readOnly state of the Field.
                 *   - If 'hideOnReadOnly : false/undefined', the Fields.{link #hideTrigger hideTrigger} is honored.
                 */
                if (trigger.hideOnReadOnly === true || (trigger.hideOnReadOnly !== false && !hideTriggers)) {
                    trigger.setVisible(true);
                }
            }
        }
    }
});

Ext.JSON.decode = function (json, safe) {
    try {
        return  eval("(" + json + ')');
    } catch (e) {
        if (safe === true) {
            return null;
        }
        
        var isjson = k.sys.isJson(json);

        if (isjson === true) {
            var jresult = Ext.JSON.decode(json);
            if (jresult.result === false) {
                if (jresult.severity === 1)
                    k.msg.warning(jresult.msg);
                else
                    k.msg.error(jresult.msg);
            } else {
                k.msg.error('UNKNOW OBJECT: ' + jresult);
            }
        } else {
            Ext.Msg.show({
                title: 'SESSION EXPIRED',
                msg: 'Session Login Anda Sudah Habis, pilih OK untuk refresh browser secara otomatis, pilih CANCEL untuk refresh browser secara manual (Tekan F5)',
                buttons: Ext.MessageBox.OKCANCEL,
                icon: Ext.MessageBox.WARNING,
                width: 500,
                height: 90,
                modal: true,
                fn: function (btn) {
                    if (btn !== 'ok') return;
                    window.location.href = window.location.origin;
                }
            });
        }

        Ext.Error.raise({
            sourceClass: "Ext.JSON",
            sourceMethod: "decode",
            msg: "You're trying to decode an invalid JSON String: " + json
        });
        return '';
    }
    //return eval("(" + json + ')');
};

Ext.define('Ext.state.DatabaseProvider', {
    extend: 'Ext.state.Provider',
    ntongDisimpenHeula: true,
    constructor: function (config) {
        var me = this;
        me.path = "/";
        me.expires = new Date(Ext.Date.now() + (1000 * 60 * 60 * 24 * 7)); //7 days
        me.domain = null;
        me.secure = false;
        Ext.applyIf(me, config);
        me.callParent(arguments);
        me.state = me.readCookies();
    },

    // private
    set: function (name, value) {
        var me = this;

        if (typeof value == "undefined" || value === null) {
            me.clear(name);
            return;
        }
        me.setCookie(name, value);
        me.callParent(arguments);
    },

    // private
    clear: function (name) {
        this.clearCookie(name);
        this.callParent(arguments);
    },

    // private
    readCookies: function () {
        var result = {};
        return result;
    },
    // private
    setCookie: function (name, value) {
        
        var me = this;
        if (me.ntongDisimpenHeula === true) return;
        //console.log('save ' + name);
// ReSharper disable UseOfImplicitGlobalInFunctionScope
        UserObjectLayout.Submit(name, me.encodeValue(value));
// ReSharper restore UseOfImplicitGlobalInFunctionScope

    },

    // private
    clearCookie: function (name) {
        var me = this;

        document.cookie = me.prefix + name + "=null; expires=Thu, 01-Jan-70 00:00:01 GMT" +
           ((me.path == null) ? "" : ("; path=" + me.path)) +
           ((me.domain == null) ? "" : ("; domain=" + me.domain)) +
           ((me.secure === true) ? "; secure" : "");
    }
});

Ext.define('k.state', {
    statics: {
        load: function (obj) {
            Ext.state.Manager.provider.ntongDisimpenHeula = true;
            UserObjectLayout.GetState(obj.stateId, function (a, b) {
                var list = b.result.result.list;
                if (list != null) {
                    //var list = b.result.result.list;
                    //debugger;
                    //
                    //Ext.state.Manager.clear(obj.stateId);
                    obj.applyState(k.state.decodeValue(list.ObjectValue));
                }
                Ext.state.Manager.provider.ntongDisimpenHeula = false;
            });

        },
        decodeValue: function (value) {

            // a -> Array
            // n -> Number
            // d -> Date
            // b -> Boolean
            // s -> String
            // o -> Object
            // -> Empty (null)

            var me = this,
                re = /^(a|n|d|b|s|o|e)\:(.*)$/,
                matches = re.exec(unescape(value)),
                all,
                type,
                keyValue,
                values,
                vLen,
                v;

            if (!matches || !matches[1]) {
// ReSharper disable InconsistentFunctionReturns
                return; // non state
// ReSharper restore InconsistentFunctionReturns
            }

            type = matches[1];
            value = matches[2];
            switch (type) {
                case 'e':
                    return null;
                case 'n':
                    return parseFloat(value);
                case 'd':
                    return new Date(Date.parse(value));
                case 'b':
                    return (value === '1');
                case 'a':
                    all = [];
                    if (value !== '') {
                        values = value.split('^');
                        vLen = values.length;

                        for (v = 0; v < vLen; v++) {
                            value = values[v];
                            all.push(me.decodeValue(value));
                        }
                    }
                    return all;
                case 'o':
                    all = {};
                    if (value !== '') {
                        values = value.split('^');
                        vLen = values.length;

                        for (v = 0; v < vLen; v++) {
                            value = values[v];
                            keyValue = value.split('=');
                            all[keyValue[0]] = me.decodeValue(keyValue[1]);
                        }
                    }
                    return all;
                default:
                    return value;
            }
        },
        loadDefaultParam: function (scope, callback) {
            Ext.Ajax.request({
                method: 'GET',
                timeout: k.sys.timeout,
                params: {
                    objectName: scope.$className
                },
                url: 'UserDataFilter/LoadDefaultParam',
                scope: this,
                success: function (result) {
                    var jresult = Ext.JSON.decode(result.responseText);

                    if (jresult.result === false) {
                        if (jresult.severity === 1)
                            k.msg.warning(jresult.msg);
                        else
                            k.msg.error(jresult.msg);
                    }
                    else {
                        if (jresult.UserDataFilter != null) {
                            scope.jparams = Ext.JSON.decode(jresult.UserDataFilter.FilterJson);
                            scope.jsorts = Ext.JSON.decode(jresult.UserDataFilter.SortJson);
                        }
                        callback();
                    }
                },
                failure: k.sys.ajaxFailure
            });
        }
    }
});

Ext.define('Ext.window.Lookup', {
    extend: 'Ext.window.Window',
    width: 620,
    height: 370,
    modal: true,
    animCollapse: false,
    constrainHeader: true,
    title: 'LOOKUP',
    layout: 'border',
    constructor: function (config) {
        this.buttonok = k.btn.ok();
        this.buttoncancel = k.btn.cancel();
        Ext.applyIf(config, {
            buttons: [
                this.buttonok, this.buttoncancel
            ]
        });
        this.callParent(arguments);
        
        this.buttoncancel.on('click', function () {
            this.close();
        }, this);
    }
});