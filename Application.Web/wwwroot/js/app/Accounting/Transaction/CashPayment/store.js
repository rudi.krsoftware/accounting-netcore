Ext.define('app.CashPayment.store', {
    extend: 'Ext.data.Store',
    pageSize: 20,
    remoteSort: true, 
	fields: [
		'RecordStatus',
		'Id',
		{ name: 'RowStatus', type: 'int' },
		'RowVersion',
		'CreatedBy',
		{ name: 'CreatedDate', type: 'date' },
		'ModifiedBy',
		{ name: 'ModifiedDate', type: 'date' },
		'StatusApproval',
		'EmployeeCode',
		'EmployeeName',
		'DivisionName',
		'Position',
		'Location',
		'Email',
		'CcEmail',
		'CashPaymentNo',
		{ name: 'CashPaymentDate', type: 'date' },
		'CashPaymentStatus',
		'PaymentDocumentNo',
		'PaymentDocumentType',
		{ name: 'PaymentDocumentEffectiveDate', type: 'date' },
		{ name: 'PaymentDocumentExpiredDate', type: 'date' },
		'PaymentDocumentRemark',
		'ReceiverType',
		'ReceiverCode',
		'ReceiverName',
		'BankCode',
		'BankName',
		'RefDocumentType',
		'RefDocumentNo',
		{ name: 'RefDocumentDate', type: 'date' },
		'Description',
		'CurrencyCode',
		'TotalPayment',
		'Notes',
        { name: 'Crud', type: 'int' }
    ],
    proxy: {
        type: 'ajax',
        url: k.app.apiAccountingUrl + 'api/cashpayment/listpaging',
        reader: {
            type: 'json',
            rootProperty: 'ListCashPayment',
            idProperty: 'Id',
            totalProperty: 'totalCount'
        },
        headers: {
            Authorization: 'Bearer ' + Ext.util.Cookies.get('KEY_LOGIN')
        }
    }
});

