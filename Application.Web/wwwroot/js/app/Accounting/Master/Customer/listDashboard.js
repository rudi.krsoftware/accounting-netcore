﻿Ext.define('app.Accounting.Master.Customer.listDashboard', {
    extend: 'base.view.MaintainForm',
    title: 'Customer',
    keytag: 'customer',
    keytag2: 'customershipment',
    layout: 'border',
    closable: true,
    autoScroll: true,
    initComponent: function () {
        var me = this;
        me.CustomerList = Ext.create('app.Accounting.Master.Customer.list', { isChild: true, state: me.state, keytag: me.keytag });
        me.CustomerList.initChildComponent();

        me.CustomerShipmentList = Ext.create('app.Accounting.Master.CustomerShipment.list', { isChild: true, state: me.state2, keytag: me.keytag2 });
        me.CustomerShipmentList.initChildComponent();

        Ext.applyIf(me, {
            items: [me.CustomerList.toolbar, me.CustomerList.cgrid, me.CustomerShipmentList.cgrid]
        });
        me.callParent(arguments);
        me.onAfterRender();
    },
    onAfterRender: function () {
        var me = this;
        me.CustomerList.toolbar.buttonRefresh.on('click', me.onRefreshCustomerClicked, me);
        me.CustomerList.cgrid.on('selectionchange', me.onitemselectionchange, me);
    },
    onRefreshCustomerClicked: function () {
        var me = this;
        me.CustomerShipmentList.cgrid.store.removeAll();
    },
    onitemselectionchange: function (a, selected) {
        var me = this;
        if (selected.length > 0) {
            me.cdata = selected[0];
            me.loadMaterialSupplier(me.cdata.data.Id);
        }
    },
    loadMaterialSupplier: function (id) {
        var me = this;
        var store = me.CustomerShipmentList.cgrid.store;
        var p1 = [{
            Kolom: 'CustomerId',
            DataType: 'guid',
            Operator: 0,
            Keyword1: id,
            LogicalOperator: 0
        }];
        var p2 = me.CustomerShipmentList.jsorts === null ? '' : Ext.JSON.encode(me.CustomerShipmentList.jsorts);
        store.getProxy().setExtraParam('params', Ext.JSON.encode(p1));
        store.getProxy().setExtraParam('sorters', p2);
        store.getProxy().url = k.app.apiAccountingUrl + 'api/accounting/customershipment/list';
        store.load({
            scope: this,
            callback: function (records, operation, success) {
                if (success === false) {
                    //var jresult = Ext.JSON.decode(operation.serverResponse.responseText);
                    k.msg.error(operation.serverResponse.statusText);
                }
            }
        });
        //try {
        //    Ext.Ajax.request({
        //        method: 'GET',
        //        timeout: k.sys.timeout,
        //        url: k.app.apiAccountingUrl + 'api/accounting/customershipment/list',
        //        headers: {
        //            Authorization: 'Bearer ' + Ext.util.Cookies.get('KEY_LOGIN')
        //        },
        //        params: {
        //            id: id
        //        },
        //        success: function (result) {
        //            var jresult = Ext.JSON.decode(result.responseText);
        //            if (jresult.result === false) {
        //                if (jresult.severity === 1)
        //                    k.msg.warning(jresult.msg);
        //                else
        //                    k.msg.error(jresult.msg);
        //            } else {
        //                me.CustomerShipmentList.cgrid.store.loadData(jresult.ListCustomerShipment);
        //            }
        //        },
        //        failure: k.sys.ajaxFailure
        //    });
        //} catch (error) {
        //    k.msg.error(error);
        //}
    }
});