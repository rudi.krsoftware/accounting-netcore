Ext.define('app.Accounting.Master.Supplier.editor', {
    extend: 'base.view.EditorForm',
    width: 750,
    height: 600,
    modal: true,
    collapsible: true,
    autoScroll: true,
    title: 'Supplier',
    initComponent: function () {
        var me = this;
        me.buttonOk = k.btn.ok();
        me.buttonClose = k.btn.close();
        
        me.objId = Ext.create('Ext.form.field.Hidden', { name: 'Id' });
        me.User = Ext.create('Ext.form.field.Hidden', { name: 'User', value: Ext.util.Cookies.get('Username') });

        me.CategorySupplierId = Ext.create('Ext.form.field.Hidden', { name: 'CategorySupplierId' });
        me.CategoryStore = Ext.create('app.Accounting.Master.CategorySupplier.store');
        me.CategoryStore.on('beforeload', function (store) {
            store.getProxy().url = k.app.apiAccountingUrl + 'api/accounting/categorysupplier/list';
            store.getProxy().setExtraParam('params', 'null');
        }, me);
        me.CategorySupplierName = Ext.create('Ext.form.field.ComboBox', {
            enforceMaxLength: true, maxLength: 64, fieldLabel: 'Category', allowBlank: false, name: 'CategorySupplierName', anchor: '98%', editable: false,
            queryMode: 'remote',
            typeAhead: false,
            hideTrigger: false,
            forceSelection: false,
            enableKeyEvents: true,
            valueField: 'CategoryName',
            displayField: 'CategoryName',
            store: me.CategoryStore,
            listeners: {
                change: function (ele, newValue, oldValue) {
                    if (newValue !== oldValue && newValue !== ele.lastSelectEvent) {
                        this.store.data.items.forEach(function (e) {
                            if (e.data.CategoryName === newValue) {
                                dum = e.data.Id;
                                return;
                            }
                        });
                        me.CategorySupplierId.setValue(dum);
                    }
                }
            }
        });

		me.SupplierCode = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 24, fieldLabel: 'Supplier Code', allowBlank: false, name: 'SupplierCode', anchor: '96%' });
		me.SupplierName = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 128, fieldLabel: 'Supplier Name', allowBlank: false, name: 'SupplierName', anchor: '96%' });
		me.Country = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 128, fieldLabel: 'Country', allowBlank: false, name: 'Country', anchor: '96%' });
		me.Province = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 128, fieldLabel: 'Province', allowBlank: false, name: 'Province', anchor: '96%' });
		me.City = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 128, fieldLabel: 'City', allowBlank: false, name: 'City', anchor: '96%' });
		me.Address = Ext.create('Ext.form.field.TextArea', { enforceMaxLength: true, maxLength: 256, fieldLabel: 'Address', allowBlank: false, name: 'Address', anchor: '98%' });
		me.ZipCode = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 24, fieldLabel: 'Zip Code', allowBlank: false, name: 'ZipCode', anchor: '96%' });
		me.Phone = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 24, fieldLabel: 'Phone 1', allowBlank: false, name: 'Phone', anchor: '96%' });
		me.Phone2 = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 24, fieldLabel: 'Phone 2', allowBlank: true, name: 'Phone2', anchor: '96%' });
		me.Fax = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 64, fieldLabel: 'Fax', allowBlank: true, name: 'Fax', anchor: '96%' });
		me.Email = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 256, fieldLabel: 'Email', allowBlank: false, name: 'Email', anchor: '96%' });
		me.WebAddress = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 256, fieldLabel: 'Web Address', allowBlank: true, name: 'WebAddress', anchor: '98%' });
		me.ContactPersonName = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 64, fieldLabel: 'Contact Person Name', allowBlank: true, name: 'ContactPersonName', anchor: '96%' });
        me.ContactPersonPhone = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 24, fieldLabel: 'Contact Person Phone', allowBlank: true, name: 'ContactPersonPhone', anchor: '96%' });

		me.DefaultCurrency = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 24, fieldLabel: 'Default Currency', allowBlank: false, name: 'DefaultCurrency', anchor: '96%' });
		me.DefaultPaymentType = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 24, fieldLabel: 'Default Payment Type', allowBlank: false, name: 'DefaultPaymentType', anchor: '96%' });
		me.DefaultVAT = Ext.create('Ext.form.field.Number', { enforceMaxLength: true, fieldLabel: 'Default VAT', allowBlank: false, name: 'DefaultVAT', anchor: '96%' });
        me.DefaultOtherTax = Ext.create('Ext.form.field.Number', { enforceMaxLength: true, fieldLabel: 'Default Other Tax', allowBlank: false, name: 'DefaultOtherTax', anchor: '96%' });
        me.DefaultOtherTaxRemark = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 128, fieldLabel: 'Default Other Tax Remark', allowBlank: false, name: 'DefaultOtherTaxRemark', anchor: '96%' });
        me.DefaultFreightCost = Ext.create('Ext.form.field.Number', { enforceMaxLength: true, fieldLabel: 'Default Freight Cost', allowBlank: false, name: 'DefaultFreightCost', anchor: '96%' });
        me.DefaultOtherCost = Ext.create('Ext.form.field.Number', { enforceMaxLength: true, fieldLabel: 'Default Other Cost', allowBlank: false, name: 'DefaultOtherCost', anchor: '96%' });
        me.DefaultOtherCostRemark = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 128, fieldLabel: 'Default Other Cost Remark', allowBlank: false, name: 'DefaultOtherCostRemark', anchor: '96%' });
        me.IsActive = Ext.create('Ext.form.field.Checkbox', { fieldLabel: 'Is Active', name: 'IsActive', anchor: '98%', inputValue: true, uncheckedValue: false, checked: true });

        me.Notes = Ext.create('Ext.form.field.TextArea', { enforceMaxLength: true, maxLength: 512, allowBlank: true, name: 'Notes', anchor: '98%' });
        
        me.cpanel = Ext.create('Ext.form.Panel', {
            xtype: 'form',
            border: false,
            fieldDefaults: {
                msgTarget: 'side',
                labelWidth: 100
            },
            autoScroll: true,
            defaults: {
                anchor: '100%'
            },
            items: [me.objId, me.User, me.CategorySupplierId, /*me.RowStatus, me.RowVersion,*/ {
                xtype: 'fieldset',
                title: 'Intruction',
                collapsible: true,
                defaults: {
                    anchor: '100%'
                },
                html: "How To Input Form Supplier"
            }, {
                xtype: 'fieldset',
                title: 'Informasi Supplier',
                defaultType: 'textfield',
                items: [me.CategorySupplierName, {
                    xtype: 'container',
                    layout: 'hbox',
                    items: [{
                        xtype: 'container',
                        flex: 1,
                        border: false,
                        layout: 'anchor',
                        defaultType: 'textfield',
                        items: [me.SupplierCode, me.ContactPersonName]
                    }, {
                        xtype: 'container',
                        flex: 1,
                        border: false,
                        layout: 'anchor',
                        defaultType: 'textfield',
                        items: [me.SupplierName, me.ContactPersonPhone]
                    }]
                }, me.IsActive]
            }, {
                xtype: 'fieldset',
                title: 'Informasi Alamat',
                defaultType: 'textfield',
                items: [me.Address, {
                    xtype: 'container',
                    layout: 'hbox',
                    items: [{
                        xtype: 'container',
                        flex: 1,
                        border: false,
                        layout: 'anchor',
                        defaultType: 'textfield',
                        items: [me.City, me.Country, me.Phone, me.Email]
                    }, {
                        xtype: 'container',
                        flex: 1,
                        border: false,
                        layout: 'anchor',
                        defaultType: 'textfield',
                        items: [me.Province, me.ZipCode, me.Phone2, me.Fax]
                    }]
                }, me.WebAddress]
            }, {
                xtype: 'fieldset',
                title: 'Informasi Payment',
                defaultType: 'textfield',
                items: [{
                    xtype: 'container',
                    layout: 'hbox',
                    items: [{
                        xtype: 'container',
                        flex: 1,
                        border: false,
                        layout: 'anchor',
                        defaultType: 'textfield',
                        items: [me.DefaultCurrency, me.DefaultFreightCost, me.DefaultOtherTax, me.DefaultOtherTaxRemark]
                    }, {
                        xtype: 'container',
                        flex: 1,
                        border: false,
                        layout: 'anchor',
                        defaultType: 'textfield',
                        items: [me.DefaultPaymentType, me.DefaultVAT, me.DefaultOtherCost, me.DefaultOtherCostRemark]
                    }]
                }]
            }, {
                xtype: 'fieldset',
                title: 'Catatan',
                defaultType: 'textfield',
                defaults: {
                    anchor: '100%'
                },
                items: [me.Notes]
            }],
            buttons: [me.buttonOk, me.buttonClose]
        });

        Ext.applyIf(me, {
            items: [me.cpanel]
        });
        me.callParent(arguments);
        me.on('afterrender', me.onAfterRender, me);
        me.buttonOk.on('click', me.onButtonOkClicked, me);
        me.buttonClose.on('click', me.closeForm, me);
    },
    closeForm: function () {
        var me = this;
        if (me.cpanel !== null && me.cpanel.getForm !== null && me.cpanel.getForm().isDirty()) {
            k.msg.ask(me.cancelmsg, function (btn) {
                if (btn === 'ok') {
                    me.close();
                }
            });
        } else {
            me.close();
        }
    },
    onAfterRender: function () {
        var me = this;
        me.insertUrl = k.app.apiAccountingUrl + 'api/accounting/supplier/submit';
    },

    create: function () {
        var me = this;
        me.show();
        me.setTitle('Input Supplier');
        me.cancelmsg = 'Cancel Input Supplier ?';

        me.confirm = 'Save Data Supplier ?';
        me.buttonOk.setText('Save');
    },
    open: function (record) {
        var me = this;
        me.show();

        me.cancelmsg = 'Cancel Update Supplier ?';
        me.setTitle('Update Supplier');

        me.confirm = 'Update Data Supplier ?';
        me.buttonOk.setText('Update');
        me.loadData(record);
		me.SupplierCode.setReadOnly(true); 
    },
    openDisable: function (record) {
        var me = this;
        me.show();
        me.cancelmsg = 'Close Supplier ?';
        me.setTitle('Supplier');
        me.turnDisable();
        me.buttonOk.hide();
        me.loadData(record);
    },
    turnDisable: function () {
        var me = this;
		me.CategorySupplierId.setReadOnly(true); 
		me.SupplierCode.setReadOnly(true); 
		me.SupplierName.setReadOnly(true); 
		me.Country.setReadOnly(true); 
		me.Province.setReadOnly(true); 
		me.City.setReadOnly(true); 
		me.Address.setReadOnly(true); 
		me.ZipCode.setReadOnly(true); 
		me.Phone.setReadOnly(true); 
		me.Phone2.setReadOnly(true); 
		me.Fax.setReadOnly(true); 
		me.Email.setReadOnly(true); 
		me.WebAddress.setReadOnly(true); 
		me.ContactPersonName.setReadOnly(true); 
		me.ContactPersonPhone.setReadOnly(true); 
		me.DefaultCurrency.setReadOnly(true); 
		me.DefaultPaymentType.setReadOnly(true); 
		me.DefaultVAT.setReadOnly(true); 
		me.DefaultOtherTax.setReadOnly(true); 
		me.DefaultOtherTaxRemark.setReadOnly(true); 
		me.DefaultFreightCost.setReadOnly(true); 
		me.DefaultOtherCost.setReadOnly(true); 
		me.DefaultOtherCostRemark.setReadOnly(true); 
		me.IsActive.setReadOnly(true); 
		me.Notes.setReadOnly(true); 
    },
    onButtonOkClicked: function () {
        var me = this;
        if (!me.cpanel.getForm().isValid()) {
            k.msg.warning('Invalid Form');
            return;
        }
        k.msg.ask(me.confirm, function (btn) {
            if (btn === 'ok') {
                var params = me.cpanel.getValues();
                k.msg.wait('Please Waiting, Saving Data.....');
                try {
                    Ext.Ajax.request({
                        method: 'POST',
                        timeout: k.sys.timeout,
                        jsonData: JSON.stringify(params),
                        url: me.insertUrl,
                        headers: {
                            Authorization: 'Bearer ' + Ext.util.Cookies.get('KEY_LOGIN')
                        },
                        success: function (result) {
                            var jresult = Ext.JSON.decode(result.responseText);
                            if (jresult.result === false) {
                                if (jresult.severity === 1)
                                    k.msg.warning(jresult.msg);
                                else
                                    k.msg.error(jresult.msg);
                            } else {
                                k.msg.info(jresult.msg);
                                // ReSharper disable once Html.EventNotResolved
                                me.fireEvent("afterInsertSuccessEvent");
                                me.close();
                            }
                        },
                        failure: k.sys.ajaxFailure
                    });
                } catch (err) {
                    k.msg.error(err);
                }

            }
        });
    },
    loadData: function (record) {
        var me = this;
        try {
            k.msg.wait('Please Waiting, Loading Data.....');
            Ext.Ajax.request({
                method: 'GET',
                timeout: k.sys.timeout,
                url: k.app.apiAccountingUrl + 'api/accounting/supplier/get',
                params: {
                    id: record.data.Id
                },
                headers: {
                    Authorization: 'Bearer ' + Ext.util.Cookies.get('KEY_LOGIN')
                },
                scope: this,
                success: function (result) {
                    var jresult = Ext.JSON.decode(result.responseText);
                    if (jresult.result === false) {
                        if (jresult.severity === 1)
                            k.msg.warning(jresult.msg);
                        else
                            k.msg.error(jresult.msg);
                    } else {
                        me.cpanel.getForm().setValues(jresult.Supplier);
                        k.msg.hide();
                    }
                },
                failure: k.sys.ajaxFailure
            });
        } catch (err) {
            k.msg.error(err);
        }
    }
});


