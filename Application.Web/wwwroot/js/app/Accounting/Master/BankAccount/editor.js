Ext.define('app.Accounting.Master.BankAccount.editor', {
    extend: 'base.view.EditorForm',
    width: 750,
    height: 400,
    modal: true,
    collapsible: true,
    autoScroll: true,
    title: 'Bank Account',
    initComponent: function () {
        var me = this;
        me.buttonOk = k.btn.ok();
        me.buttonClose = k.btn.close();
        
        me.objId = Ext.create('Ext.form.field.Hidden', { name: 'Id' });
        me.User = Ext.create('Ext.form.field.Hidden', { name: 'User', value: Ext.util.Cookies.get('Username') });

        me.BankId = Ext.create('Ext.form.field.Hidden', { name: 'BankId' });
        me.BankStore = Ext.create('app.Accounting.Master.Bank.store');
        me.BankStore.on('beforeload', function (store) {
            store.getProxy().url = k.app.apiAccountingUrl + 'api/accounting/bank/list';
            store.getProxy().setExtraParam('params', 'null');
        }, me);
        me.BankName = Ext.create('Ext.form.field.ComboBox', {
            enforceMaxLength: true, maxLength: 64, fieldLabel: 'Bank', allowBlank: false, name: 'BankName', anchor: '98%', editable: false,
            queryMode: 'remote',
            typeAhead: false,
            hideTrigger: false,
            forceSelection: false,
            enableKeyEvents: true,
            valueField: 'BankName',
            displayField: 'BankName',
            store: me.BankStore,
            listeners: {
                change: function (ele, newValue, oldValue) {
                    if (newValue !== oldValue && newValue !== ele.lastSelectEvent) {
                        this.store.data.items.forEach(function (e) {
                            if (e.data.BankName === newValue) {
                                dum = e.data.Id;
                                return;
                            }
                        });
                        me.BankId.setValue(dum);
                    }
                }
            }
        });


		me.AccountNumber = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 64, fieldLabel: 'Account Number', allowBlank: false, name: 'AccountNumber', anchor: '98%' });
        me.AccountName = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 128, fieldLabel: 'Account Name', allowBlank: false, name: 'AccountName', anchor: '98%' });
        me.IsActive = Ext.create('Ext.form.field.Checkbox', { fieldLabel: 'Is Active', name: 'IsActive', anchor: '98%', inputValue: true, uncheckedValue: false, checked: true });
		me.Notes = Ext.create('Ext.form.field.TextArea', { enforceMaxLength: true, maxLength: 512, fieldLabel: 'Notes', allowBlank: true, name: 'Notes', anchor: '98%' });
        
        me.cpanel = Ext.create('Ext.form.Panel', {
            xtype: 'form',
            border: false,
            fieldDefaults: {
                msgTarget: 'side',
                labelWidth: 100
            },
            autoScroll: true,
            defaults: {
                anchor: '100%'
            },
            items: [me.objId, me.User, /*me.RowStatus, me.RowVersion,*/ {
                xtype: 'fieldset',
                title: 'Intruction',
                collapsible: true,
                defaults: {
                    anchor: '100%'
                },
                html: "How To Input Form Bank Account"
            }, {
                xtype: 'fieldset',
                title: 'Bank Account Information',
                defaultType: 'textfield',
                defaults: {
                    anchor: '100%'
                },
                items: [me.BankId, me.BankName, me.AccountNumber, me.AccountName, me.IsActive, me.Notes, ]
            }],
            buttons: [me.buttonOk, me.buttonClose]
        });

        Ext.applyIf(me, {
            items: [me.cpanel]
        });
        me.callParent(arguments);
        me.on('afterrender', me.onAfterRender, me);
        me.buttonOk.on('click', me.onButtonOkClicked, me);
        me.buttonClose.on('click', me.closeForm, me);
    },
    closeForm: function () {
        var me = this;
        if (me.cpanel !== null && me.cpanel.getForm !== null && me.cpanel.getForm().isDirty()) {
            k.msg.ask(me.cancelmsg, function (btn) {
                if (btn === 'ok') {
                    me.close();
                }
            });
        } else {
            me.close();
        }
    },
    onAfterRender: function () {
        var me = this;
        me.insertUrl = k.app.apiAccountingUrl + 'api/accounting/bankaccount/submit';
    },

    create: function () {
        var me = this;
        me.show();
        me.setTitle('Input BankAccount');
        me.cancelmsg = 'Cancel Input Bank Account ?';

        me.confirm = 'Save Data Bank Account ?';
        me.buttonOk.setText('Save');
    },
    open: function (record) {
        var me = this;
        me.show();

        me.cancelmsg = 'Cancel Update Bank Account ?';
        me.setTitle('Update Bank Account');

        me.confirm = 'Update Data Bank Account ?';
        me.buttonOk.setText('Update');
        me.loadData(record);
    },
    openDisable: function (record) {
        var me = this;
        me.show();
        me.cancelmsg = 'Close BankAccount ?';
        me.setTitle('BankAccount');
        me.turnDisable();
        me.buttonOk.hide();
        me.loadData(record);
    },
    turnDisable: function () {
        var me = this;
		me.BankId.setReadOnly(true); 
        me.BankName.setReadOnly(true); 
		me.AccountNumber.setReadOnly(true); 
		me.AccountName.setReadOnly(true); 
		me.IsActive.setReadOnly(true); 
		me.Notes.setReadOnly(true); 
    },
    onButtonOkClicked: function () {
        var me = this;
        if (!me.cpanel.getForm().isValid()) {
            k.msg.warning('Invalid Form');
            return;
        }
        k.msg.ask(me.confirm, function (btn) {
            if (btn === 'ok') {
                var params = me.cpanel.getValues();
                k.msg.wait('Please Waiting, Saving Data.....');
                try {
                    Ext.Ajax.request({
                        method: 'POST',
                        timeout: k.sys.timeout,
                        jsonData: JSON.stringify(params),
                        url: me.insertUrl,
                        headers: {
                            Authorization: 'Bearer ' + Ext.util.Cookies.get('KEY_LOGIN')
                        },
                        success: function (result) {
                            var jresult = Ext.JSON.decode(result.responseText);
                            if (jresult.result === false) {
                                if (jresult.severity === 1)
                                    k.msg.warning(jresult.msg);
                                else
                                    k.msg.error(jresult.msg);
                            } else {
                                k.msg.info(jresult.msg);
                                // ReSharper disable once Html.EventNotResolved
                                me.fireEvent("afterInsertSuccessEvent");
                                me.close();
                            }
                        },
                        failure: k.sys.ajaxFailure
                    });
                } catch (err) {
                    k.msg.error(err);
                }

            }
        });
    },
    loadData: function (record) {
        var me = this;
        try {
            k.msg.wait('Please Waiting, Loading Data.....');
            Ext.Ajax.request({
                method: 'GET',
                timeout: k.sys.timeout,
                url: k.app.apiAccountingUrl + 'api/accounting/bankaccount/get',
                params: {
                    id: record.data.Id
                },
                headers: {
                    Authorization: 'Bearer ' + Ext.util.Cookies.get('KEY_LOGIN')
                },
                scope: this,
                success: function (result) {
                    var jresult = Ext.JSON.decode(result.responseText);
                    if (jresult.result === false) {
                        if (jresult.severity === 1)
                            k.msg.warning(jresult.msg);
                        else
                            k.msg.error(jresult.msg);
                    } else {
                        me.cpanel.getForm().setValues(jresult.BankAccount);
                        k.msg.hide();
                    }
                },
                failure: k.sys.ajaxFailure
            });
        } catch (err) {
            k.msg.error(err);
        }
    }
});


