Ext.define('app.Accounting.Master.CategoryCustomer.store', {
    extend: 'Ext.data.Store',
    pageSize: 20,
    remoteSort: true,
    fields: [
        'RecordStatus',
        'Id',
        { name: 'RowStatus', type: 'int' },
        'RowVersion',
        'CreatedBy',
        { name: 'CreatedDate', type: 'date' },
        'ModifiedBy',
        { name: 'ModifiedDate', type: 'date' },
        'CategoryName',
        { name: 'Priority', type: 'int' },
        'Notes',
        { name: 'Crud', type: 'int' }
    ],
    proxy: {
        type: 'ajax',
        url: k.app.apiAccountingUrl + 'api/accounting/categorycustomer/listpaging',
        reader: {
            type: 'json',
            rootProperty: 'ListCategoryCustomer',
            idProperty: 'Id',
            totalProperty: 'totalCount'
        },
        headers: {
            Authorization: 'Bearer ' + Ext.util.Cookies.get('KEY_LOGIN')
        }
    }
});

