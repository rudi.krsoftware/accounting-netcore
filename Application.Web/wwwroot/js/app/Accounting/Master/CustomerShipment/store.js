Ext.define('app.Accounting.Master.CustomerShipment.store', {
    extend: 'Ext.data.Store',
    pageSize: 20,
    remoteSort: true,
    fields: [
        'RecordStatus',
        'Id',
        { name: 'RowStatus', type: 'int' },
        'RowVersion',
        'CreatedBy',
        { name: 'CreatedDate', type: 'date' },
        'ModifiedBy',
        { name: 'ModifiedDate', type: 'date' },
        'CustomerId',
        'IsDefaultShipment',
        'Carrier',
        'Consignee',
        'PortOfOrigin',
        'PortOfDestination',
        'Country',
        'Province',
        'City',
        'Address',
        'ZipCode',
        'Notes',
        { name: 'Crud', type: 'int' }
    ],
    proxy: {
        type: 'ajax',
        url: k.app.apiAccountingUrl + 'api/accounting/customershipment/listpaging',
        reader: {
            type: 'json',
            rootProperty: 'ListCustomerShipment',
            idProperty: 'Id',
            totalProperty: 'totalCount'
        },
        headers: {
            Authorization: 'Bearer ' + Ext.util.Cookies.get('KEY_LOGIN')
        }
    }
});

