Ext.define('app.Accounting.Master.CustomerShipment.list', {
    extend: 'base.view.MaintainForm',
    title: 'CustomerShipment',
    keytag: 'customershipment',
    //layout: 'border',
    closable: true,
    initComponent: function () {
        var me = this;
        me.cstore = Ext.create('app.Accounting.Master.CustomerShipment.store');
        me.cgrid = Ext.create('Ext.grid.Panel', {
            region: 'center',
            store: me.cstore,
            stateful: true,
            stateId: this.keytag,
            columns: [
				//{ text: 'Id', width: 100, dataIndex: 'Id', hidden: true },
				//{ text: 'CustomerId', width: 100, dataIndex: 'CustomerId' },
				{ text: 'Is Default Shipment', width: 100, dataIndex: 'IsDefaultShipment' },
				{ text: 'Carrier', width: 100, dataIndex: 'Carrier' },
				{ text: 'Consignee', width: 100, dataIndex: 'Consignee' },
				{ text: 'Port Of Origin', width: 100, dataIndex: 'PortOfOrigin' },
				{ text: 'Port Of Destination', width: 100, dataIndex: 'PortOfDestination' },
				{ text: 'Country', width: 100, dataIndex: 'Country' },
				{ text: 'Province', width: 100, dataIndex: 'Province' },
				{ text: 'City', width: 100, dataIndex: 'City' },
				{ text: 'Address', width: 100, dataIndex: 'Address' },
				{ text: 'Zip Code', width: 100, dataIndex: 'ZipCode' },
				{ text: 'Notes', width: 100, dataIndex: 'Notes' },
				{ text: 'Created By', width: 100, dataIndex: 'CreatedBy' },
				{ text: 'Created Date', width: 145, dataIndex: 'CreatedDate', renderer: k.format.renderdatetime },
				{ text: 'Modified By', width: 100, dataIndex: 'ModifiedBy' },
				{ text: 'Modified Date', width: 145, dataIndex: 'ModifiedDate', renderer: k.format.renderdatetime }
            ],
            viewConfig: {
                stripeRows: true,
                loadMask: true,
                loadingText: 'Loading....'
            },
            bbar: Ext.create('Ext.PagingToolbar', {
                store: me.cstore,
                displayInfo: true,
                displayMsg: 'View CustomerShipment {0} - {1} of {2}',
                emptyMsg: 'CustomerShipment Not Found'
            })
        });
        me.toolbar = Ext.create('base.view.MaintainToolbar');
        if (me.state) me.cgrid.applyState(me.state);
        Ext.applyIf(me, {
            items: [me.toolbar, me.cgrid]
        });
        me.callParent(arguments);
        me.on('afterrender', me.onAfterRender, me);
    },
    onAfterRender: function () {
        var me = this;
        me.toolbar.buttonRefresh.on('click', this.onRefreskClicked, this);
        me.toolbar.buttonFilter.on('click', this.onFilterClicked, this);
        me.toolbar.buttonCreate.on('click', this.onCreateClicked, this);
        me.toolbar.buttonOpen.on('click', this.onButtonOpenClicked, this);
        me.toolbar.buttonDelete.on('click', this.onButtonDeleteClicked, this);
        me.toolbar.buttonEdit.hide();
        me.toolbar.buttonFilter.show();
        me.toolbar.searchBar.on('specialkey', me.keyPressSearch, me);
        me.toolbar.buttonSearch.on('click', me.searchData, me);
        me.cgrid.on('sortchange', this.onsortchange, this);
        me.cgrid.on('celldblclick', me.onGridPanelCellDoubleClicked, me);
        me.cgrid.on('selectionchange', me.onitemselectionchange, me);
        if (!me.isChild) me.onRefreskClicked();
    },
    onCreateClicked: function () {
        var me = this;
        me.editor = Ext.create('app.Accounting.Master.CustomerShipment.editor');
        me.editor.create();
        me.editor.on('afterInsertSuccessEvent', me.onRefreskClicked, me);
    },
    onButtonOpenClicked: function () {
        var me = this;
        if (me.cdata === null) return;
        me.editor = Ext.create('app.Accounting.Master.CustomerShipment.editor');
        me.editor.open(me.cdata);
        me.editor.on('afterInsertSuccessEvent', me.onRefreskClicked, me);
    },
    onButtonDeleteClicked: function () {
        var me = this;
        if (me.cdata === null) return;
        k.msg.ask('Hapus data ' + me.cdata.data.Carrier + ' ?', function (btn) {
            if (btn === 'ok') {
                k.msg.wait('Please Waiting, Deleting Process.....');
                try {
                    Ext.Ajax.request({
                        method: 'GET',
                        timeout: k.sys.timeout,
                        jsonData: JSON.stringify({
                            Id: me.cdata.data.Id,
                            User: Ext.util.Cookies.get('Username')
                        }),
                        url: k.app.apiAccountingUrl + 'api/accounting/customershipment/delete',
                        headers: {
                            Authorization: 'Bearer ' + Ext.util.Cookies.get('KEY_LOGIN')
                        },
                        scope: this,
                        success: function (result) {
                            var jresult = Ext.JSON.decode(result.responseText);

                            if (jresult.result === false) {
                                if (jresult.severity === 1)
                                    k.msg.warning(jresult.msg);
                                else
                                    k.msg.error(jresult.msg);
                            } else {
                                k.msg.info(jresult.msg);
                                me.onRefreskClicked();
                            }
                        },
                        failure: k.sys.ajaxFailure
                    });
                } catch (err) {
                    k.msg.error(err);
                }
            }
        });
    },
    onRefreskClicked: function () {
        var store = this.cgrid.store;
        var p1 = this.jparams === null ? '' : Ext.JSON.encode(this.jparams);
        var p2 = this.jsorts === null ? '' : Ext.JSON.encode(this.jsorts);
        store.getProxy().setExtraParam('params', p1);
        store.getProxy().setExtraParam('sorters', p2);
        store.load({
            scope: this,
            callback: function (records, operation, success) {
                if (success === false) {
                    //var jresult = Ext.JSON.decode(operation.serverResponse.responseText);
                    k.msg.error(operation.serverResponse.statusText);
                }
            }
        });
        this.cdata = null;
        this.cgrid.getSelectionModel().deselectAll();
    },
    onsortchange: function () {
        var me = this;
        me.jsorts = null;
    },
    keyPressSearch: function (field, e) {
        if (e.getKey() === Ext.EventObject.ENTER) {
            this.searchData();
        }
    },
    searchData: function () {
        var me = this;
        var t = me.toolbar;
        me.jparams = new Array();
        if (me.defaultParams) {
            me.jparams = me.jparams.slice(0, me.jparams.length).concat(me.defaultParams).concat(me.jparams.slice(me.jparams.length));
        }
        me.jparams.push({
            Kolom: 'Carrier',
            DataType: 'string',
            Operator: 4,
            StringKeyword: t.searchBar.getValue(),
            LogicalOperator: 1
        }, {
            Kolom: 'Consignee',
            DataType: 'string',
            Operator: 4,
            StringKeyword: t.searchBar.getValue(),
            LogicalOperator: 1
        }, {
            Kolom: 'PortOfOrigin',
            DataType: 'string',
            Operator: 4,
            StringKeyword: t.searchBar.getValue(),
            LogicalOperator: 1
        }, {
            Kolom: 'PortOfDestination',
            DataType: 'string',
            Operator: 4,
            StringKeyword: t.searchBar.getValue(),
            LogicalOperator: 0
        });
        me.onRefreskClicked();
    },
    onitemselectionchange: function (a, selected) {
        if (selected.length > 0)
            this.cdata = selected[0];
    },
    onGridPanelCellDoubleClicked: function () {
        if (this.islookup === true) {
            this.onButtonOkClicked();
            return;
        }
        this.onButtonOpenClicked();
    },
    onFilterClicked: function () {
        var me = this;
        var a = Ext.create('base.view.FormParameter', {
            dataColumn: [
				['CustomerId', 'CustomerId', 'string'],
				['IsDefaultShipment', 'IsDefaultShipment', 'string'],
				['Carrier', 'Carrier', 'string'],
				['Consignee', 'Consignee', 'string'],
				['PortOfOrigin', 'PortOfOrigin', 'string'],
				['PortOfDestination', 'PortOfDestination', 'string'],
				['Country', 'Country', 'string'],
				['Province', 'Province', 'string'],
				['City', 'City', 'string'],
				['Address', 'Address', 'string'],
				['ZipCode', 'ZipCode', 'string'],
				['Notes', 'Notes', 'string'],
                ['Created By', 'CreatedBy', 'string'],
                ['Created Date', 'CreatedDate', 'DateTime'],
                ['Modifie dBy', 'ModifiedBy', 'string'],
                ['Modified Date', 'ModifiedDate', 'DateTime']
            ],
            title: 'Smart Filter - CustomerShipment',
            sortData: me.sortData,
            paramData: me.paramData,
            jparent: me
        });
        a.bok.on('click', function () {
            me.jparams = a.getJsonParam();
            me.jsorts = a.getJsonSort();
            me.sortData = a.getArraySort();
            me.paramData = a.getArrayParam();
            me.onRefreskClicked();
        }, a);
        a.show();
    },
    lookup: function (params) {
        var me = this;
        me.islookup = true;
        me.defaultParams = params;
        me.jparams = params;
        me.buttonOk = k.btn.ok();
        me.buttonOk.on('click', me.onButtonOkClicked, me);
        me.buttonCancel = k.btn.cancel();
        me.lookupForm = Ext.create('Ext.window.Window', {
            width: 620,
            height: 370,
            modal: true,
            animCollapse: false,
            constrainHeader: true,
            title: 'Lookup - CustomerShipment',
            layout: 'border',
            items: [me.toolbar, me.cgrid
            ],
            buttons: [me.buttonOk, me.buttonCancel]
        });
        me.lookupForm.show();
        me.onAfterRender();
        me.toolbar.buttonCreate.hide();
        me.toolbar.buttonOpen.hide();
        me.toolbar.buttonDelete.hide();
        me.toolbar.buttonFilter.hide();
        me.buttonCancel.on('click', function () {
            me.lookupForm.close();
        }, me);
    },
    onButtonOkClicked: function () {
        var me = this;
        // ReSharper disable once Html.EventNotResolved
        me.fireEvent('onSelectedItemLookup', me.cdata);
        me.lookupForm.close();
    },
    initChildComponent: function () {
        var me = this;
        me.cgrid.height = '30%';
        me.cgrid.region = 'south';
        //me.cgrid.dockedItems.items[1].hide();
        me.onAfterRender();
    }
});

