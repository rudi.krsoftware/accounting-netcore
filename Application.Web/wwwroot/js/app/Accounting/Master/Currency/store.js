Ext.define('app.Accounting.Master.Currency.store', {
    extend: 'Ext.data.Store',
    pageSize: 20,
    remoteSort: true, 
	fields: [
		'RecordStatus',
		'Id',
		{ name: 'RowStatus', type: 'int' },
		'RowVersion',
		'CreatedBy',
		{ name: 'CreatedDate', type: 'date' },
		'ModifiedBy',
		{ name: 'ModifiedDate', type: 'date' },
		'CurrencyCode',
		'CurrencyName',
		{ name: 'ExchangeDate', type: 'date' },
		{ name: 'BaseValue', type: 'float' },
		{ name: 'BaseRate', type: 'float' },
		'Notes',
        { name: 'Crud', type: 'int' }
    ],
    proxy: {
        type: 'ajax',
        url: k.app.apiAccountingUrl + 'api/accounting/currency/listpaging',
        reader: {
            type: 'json',
            rootProperty: 'ListCurrency',
            idProperty: 'Id',
            totalProperty: 'totalCount'
        },
        headers: {
            Authorization: 'Bearer ' + Ext.util.Cookies.get('KEY_LOGIN')
        }
    }
});

