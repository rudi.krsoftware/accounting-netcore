Ext.define('app.Accounting.Master.ChartOfAccount.store', {
    extend: 'Ext.data.Store',
    pageSize: 20,
    remoteSort: true, 
	fields: [
		'RecordStatus',
		'Id',
		{ name: 'RowStatus', type: 'int' },
		'RowVersion',
		'CreatedBy',
		{ name: 'CreatedDate', type: 'date' },
		'ModifiedBy',
		{ name: 'ModifiedDate', type: 'date' },
		'SchemaCode',
		'ParentId',
        'IsAccountDetail',
        'CategoryAccountId',
        'AccountCategory',
		'AccountTypeId',
		'AccountType',
        'AccountCode',
		'AccountName',
		{ name: 'AccountLevel', type: 'int' },
		{ name: 'DefaultValue', type: 'int' },
		'Notes',
        { name: 'Crud', type: 'int' }
    ],
    proxy: {
        type: 'ajax',
        url: k.app.apiAccountingUrl + 'api/accounting/chartofaccount/listpaging',
        reader: {
            type: 'json',
            rootProperty: 'ListChartOfAccount',
            idProperty: 'Id',
            totalProperty: 'totalCount'
        },
        headers: {
            Authorization: 'Bearer ' + Ext.util.Cookies.get('KEY_LOGIN')
        }
    }
});

