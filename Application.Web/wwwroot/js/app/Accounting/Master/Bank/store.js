Ext.define('app.Accounting.Master.Bank.store', {
    extend: 'Ext.data.Store',
    pageSize: 20,
    remoteSort: true, 
	fields: [
		'RecordStatus',
		'Id',
		{ name: 'RowStatus', type: 'int' },
		'RowVersion',
		'CreatedBy',
		{ name: 'CreatedDate', type: 'date' },
		'ModifiedBy',
		{ name: 'ModifiedDate', type: 'date' },
		'BankCode',
		'BankName',
		'Country',
		'Province',
		'City',
		'Address',
		'ZipCode',
		'Phone',
		'Phone2',
		'Fax',
		'Email',
		'WebAddress',
		'ContactPersonName',
		'ContactPersonPhone',
        { name: 'Crud', type: 'int' }
    ],
    proxy: {
        type: 'ajax',
        url: k.app.apiAccountingUrl + 'api/accounting/bank/listpaging',
        reader: {
            type: 'json',
            rootProperty: 'ListBank',
            idProperty: 'Id',
            totalProperty: 'totalCount'
        },
        headers: {
            Authorization: 'Bearer ' + Ext.util.Cookies.get('KEY_LOGIN')
        }
    }
});

