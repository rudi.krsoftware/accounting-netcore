Ext.define('app.Accounting.Master.FiscalYear.store', {
    extend: 'Ext.data.Store',
    pageSize: 20,
    remoteSort: true,
    fields: [
        'RecordStatus',
        'Id',
        { name: 'RowStatus', type: 'int' },
        'RowVersion',
        'CreatedBy',
        { name: 'CreatedDate', type: 'date' },
        'ModifiedBy',
        { name: 'ModifiedDate', type: 'date' },
        'FiscalYearCode',
        'FiscalYearName',
        { name: 'StartDate', type: 'date' },
        { name: 'EndDate', type: 'date' },
        { name: 'ClosingType', type: 'int' },
        'Notes',
        { name: 'Crud', type: 'int' }
    ],
    proxy: {
        type: 'ajax',
        url: k.app.apiAccountingUrl + 'api/accounting/fiscalyear/listpaging',
        reader: {
            type: 'json',
            rootProperty: 'ListFiscalYear',
            idProperty: 'Id',
            totalProperty: 'totalCount'
        },
        headers: {
            Authorization: 'Bearer ' + Ext.util.Cookies.get('KEY_LOGIN')
        }
    }
});

