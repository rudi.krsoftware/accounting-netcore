Ext.define('app.Accounting.Master.FiscalYear.editor', {
    extend: 'base.view.EditorForm',
    width: 750,
    height: 370,
    modal: true,
    collapsible: true,
    autoScroll: true,
    title: 'Tahun Buku',
    initComponent: function () {
        var me = this;
        me.buttonOk = k.btn.ok();
        me.buttonClose = k.btn.close();
        
        me.objId = Ext.create('Ext.form.field.Hidden', { name: 'Id' });
        me.User = Ext.create('Ext.form.field.Hidden', { name: 'User', value: Ext.util.Cookies.get('Username') });
        
		me.FiscalYearCode = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 24, fieldLabel: 'Kode Tahun Buku', allowBlank: false, name: 'FiscalYearCode', anchor: '96%' });
		me.FiscalYearName = Ext.create('Ext.form.field.Text', { enforceMaxLength: true, maxLength: 64, fieldLabel: 'Nama Tahun Buku', allowBlank: false, name: 'FiscalYearName', anchor: '96%' });
		me.StartDate = Ext.create('Ext.form.field.Date', { fieldLabel: 'Tgl. Awal', format: k.format.date, allowBlank: false, name: 'StartDate', anchor: '96%' });
        me.EndDate = Ext.create('Ext.form.field.Date', { fieldLabel: 'Tgl. Akhir', format: k.format.date, allowBlank: false, name: 'EndDate', anchor: '96%' }); 
        me.ClosingType = Ext.create('Ext.form.field.ComboBox',
            {
                enforceMaxLength: true,
                fieldLabel: 'Tipe Tutup Buku',
                allowBlank: false,
                name: 'ClosingType',
                anchor: '96%',
                editable: false,
                typeAhead: false,
                hideTrigger: false,
                forceSelection: false,
                enableKeyEvents: true,
                queryMode: 'local',
                valueField: 'id',
                displayField: 'value',
                store: Ext.create('Ext.data.ArrayStore',
                    {
                        fields: ['id', 'value'],
                        emptyText: '',
                        data: [
                            ['1', 'TUTUP PER 1 BULAN'],
                            ['2', 'TUTUP PER 1 TAHUN'],
                            ['3', 'TUTUP PER 6 BULAN']
                        ]
                    })
            });
		me.Notes = Ext.create('Ext.form.field.TextArea', { enforceMaxLength: true, maxLength: 512, allowBlank: true, name: 'Notes', anchor: '98%' });
        
        me.cpanel = Ext.create('Ext.form.Panel', {
            xtype: 'form',
            border: false,
            fieldDefaults: {
                msgTarget: 'side',
                labelWidth: 100
            },
            autoScroll: true,
            defaults: {
                anchor: '100%'
            },
            items: [me.objId, me.User, /*me.RowStatus, me.RowVersion,*/ {
                xtype: 'fieldset',
                title: 'Intruction',
                collapsible: true,
                defaults: {
                    anchor: '100%'
                },
                html: "How To Input Form Tahun Buku"
            }, {
                xtype: 'fieldset',
                title: 'Information Tahun Buku',
                defaultType: 'textfield',
                defaults: {
                    anchor: '100%'
                },
                items: [{
                    xtype: 'container',
                    layout: 'hbox',
                    items: [{
                        xtype: 'container',
                        flex: 1,
                        border: false,
                        layout: 'anchor',
                        defaultType: 'textfield',
                        items: [me.FiscalYearCode, me.StartDate, me.ClosingType]
                    }, {
                        xtype: 'container',
                        flex: 1,
                        layout: 'anchor',
                        defaultType: 'textfield',
                        items: [me.FiscalYearName, me.EndDate]
                    }]
                }]
            }, {
                xtype: 'fieldset',
                title: 'Notes',
                defaultType: 'textfield',
                defaults: {
                    anchor: '100%'
                },
                items: [me.Notes]
            }],
            buttons: [me.buttonOk, me.buttonClose]
        });

        Ext.applyIf(me, {
            items: [me.cpanel]
        });
        me.callParent(arguments);
        me.on('afterrender', me.onAfterRender, me);
        me.buttonOk.on('click', me.onButtonOkClicked, me);
        me.buttonClose.on('click', me.closeForm, me);
    },
    closeForm: function () {
        var me = this;
        if (me.cpanel !== null && me.cpanel.getForm !== null && me.cpanel.getForm().isDirty()) {
            k.msg.ask(me.cancelmsg, function (btn) {
                if (btn === 'ok') {
                    me.close();
                }
            });
        } else {
            me.close();
        }
    },
    onAfterRender: function () {
        var me = this;
        me.insertUrl = k.app.apiAccountingUrl + 'api/accounting/fiscalyear/submit';
    },

    create: function () {
        var me = this;
        me.show();
        me.setTitle('Input Tahun Buku');
        me.cancelmsg = 'Cancel Input Tahun Buku ?';

        me.confirm = 'Save Data Tahun Buku ?';
        me.buttonOk.setText('Save');
    },
    open: function (record) {
        var me = this;
        me.show();

        me.cancelmsg = 'Cancel Update Tahun Buku ?';
        me.setTitle('Update Tahun Buku');

        me.confirm = 'Update Data Tahun Buku ?';
        me.buttonOk.setText('Update');
        me.loadData(record);
    },
    openDisable: function (record) {
        var me = this;
        me.show();
        me.cancelmsg = 'Close Tahun Buku ?';
        me.setTitle('Tahun Buku');
        me.turnDisable();
        me.buttonOk.hide();
        me.loadData(record);
    },
    turnDisable: function () {
        var me = this;
		me.FiscalYearCode.setReadOnly(true); 
		me.FiscalYearName.setReadOnly(true); 
		me.StartDate.setReadOnly(true); 
		me.EndDate.setReadOnly(true); 
		me.ClosingType.setReadOnly(true); 
		me.Notes.setReadOnly(true); 
    },
    onButtonOkClicked: function () {
        var me = this;
        if (!me.cpanel.getForm().isValid()) {
            k.msg.warning('Invalid Form');
            return;
        }
        k.msg.ask(me.confirm, function (btn) {
            if (btn === 'ok') {
                var params = me.cpanel.getValues();
                k.msg.wait('Please Waiting, Saving Data.....');
                try {
                    Ext.Ajax.request({
                        method: 'POST',
                        timeout: k.sys.timeout,
                        jsonData: JSON.stringify(params),
                        url: me.insertUrl,
                        headers: {
                            Authorization: 'Bearer ' + Ext.util.Cookies.get('KEY_LOGIN')
                        },
                        success: function (result) {
                            var jresult = Ext.JSON.decode(result.responseText);
                            if (jresult.result === false) {
                                if (jresult.severity === 1)
                                    k.msg.warning(jresult.msg);
                                else
                                    k.msg.error(jresult.msg);
                            } else {
                                k.msg.info(jresult.msg);
                                // ReSharper disable once Html.EventNotResolved
                                me.fireEvent("afterInsertSuccessEvent");
                                me.close();
                            }
                        },
                        failure: k.sys.ajaxFailure
                    });
                } catch (err) {
                    k.msg.error(err);
                }

            }
        });
    },
    loadData: function (record) {
        var me = this;
        try {
            k.msg.wait('Please Waiting, Loading Data.....');
            Ext.Ajax.request({
                method: 'GET',
                timeout: k.sys.timeout,
                url: k.app.apiAccountingUrl + 'api/accounting/fiscalyear/get',
                params: {
                    id: record.data.Id
                },
                headers: {
                    Authorization: 'Bearer ' + Ext.util.Cookies.get('KEY_LOGIN')
                },
                scope: this,
                success: function (result) {
                    var jresult = Ext.JSON.decode(result.responseText);
                    if (jresult.result === false) {
                        if (jresult.severity === 1)
                            k.msg.warning(jresult.msg);
                        else
                            k.msg.error(jresult.msg);
                    } else {
                        me.cpanel.getForm().setValues(jresult.FiscalYear);
                        k.msg.hide();
                    }
                },
                failure: k.sys.ajaxFailure
            });
        } catch (err) {
            k.msg.error(err);
        }
    }
});


