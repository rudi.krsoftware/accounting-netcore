Ext.define('app.ReportIncomeStatement.store', {
    extend: 'Ext.data.Store',
    pageSize: 20,
    remoteSort: true, 
	fields: [
		'RecordStatus',
		'Id',
		{ name: 'RowStatus', type: 'int' },
		'RowVersion',
		'CreatedBy',
		{ name: 'CreatedDate', type: 'date' },
		'ModifiedBy',
		{ name: 'ModifiedDate', type: 'date' },
		'SessionId',
		{ name: 'LineNumber', type: 'int' },
		{ name: 'LineLevel', type: 'int' },
		'Description',
		'IsDetail',
		'Saldo',
        { name: 'Crud', type: 'int' }
    ],
    proxy: {
        type: 'ajax',
        url: k.app.apiAccountingUrl + 'api/reportincomestatement/listpaging',
        reader: {
            type: 'json',
            rootProperty: 'ListReportIncomeStatement',
            idProperty: 'Id',
            totalProperty: 'totalCount'
        },
        headers: {
            Authorization: 'Bearer ' + Ext.util.Cookies.get('KEY_LOGIN')
        }
    }
});

