﻿Ext.define('main.view.TabPanelView', {
    extend: 'Ext.tab.Panel',
    alias: 'widget.tabpanelview',
    activeTab: 0,
    closeable: true,
    region: 'center',
    id: 'tabpanelviewid',
    items: [

    ],
    listeners: {
        'beforeremove': function (panel) {

        },
        'afterrender': function (panel) {

        }
    }
});

