﻿
document.oncontextmenu = function () { return true; };
Ext.Loader.setConfig({
    enabled: true,
});
Ext.Loader.setPath('Ext.ux', '../js/ext-6.2.0/ux');
//Ext.BLANK_IMAGE_URL = '../Scripts/ext-6.2.0/default/s.gif';

Ext.require([
    'Ext.ux.layout.Center'
]);

Ext.onReady(function () {
    var me = this;
    me.doLogin = function () {
        k.msg.wait('Loading....');
        Ext.Ajax.request({
            url: k.app.apiBoUrl + 'api/authorization/userprofile/login',
            timeout: k.timeout,
            form: me.xform.getForm().id,
            method: 'POST',
            jsonData: JSON.stringify(me.xform.getValues()),
            success: function (result) {
                try {
                    var response = Ext.JSON.decode(result.responseText);
                    if (response.result === false) {
                        if (response.severity === 1)
                            k.msg.warning(response.msg);
                        else
                            k.msg.error(response.msg);
                        if (k.loginAttempt > 3) {

                            return;
                        }
                        k.loginAttempt += 1;
                    } else {
                        Ext.util.Cookies.set('KEY_LOGIN', response.token, new Date(response.expiredToken));
                        Ext.util.Cookies.set('Username', response.username, new Date(response.expiredToken));
                        window.location = "Home";
                        k.loginAttempt = 0;
                    }
                } catch (err) {
                    Ext.MessageBox.hide();
                    console.log(err);
                }
            },
            failure: k.util.ajaxFailure
        });

    };
    me.onKeyEnter = function (field, e) {
        if (e.getKey() === Ext.EventObject.ENTER) {
            var thisValue = this.getValue();
            if (thisValue !== '') {
                me.doLogin();
            }
        }
    };
    me.username = Ext.create('Ext.form.field.Text', {
        allowBlank: false,
        fieldLabel: 'Username',
        name: 'username',
        emptyText: 'Username',
        anchor: '96%',
        listeners: {
            'specialkey': me.onKeyEnter
        }
    });
    me.password = Ext.create('Ext.form.field.Text', {
        allowBlank: false,
        fieldLabel: 'Password',
        name: 'password',
        emptyText: 'Password',
        inputType: 'password',
        anchor: '96%',
        listeners: {
            'specialkey': me.onKeyEnter
        }
    });

    me.buttonDoLogin = Ext.create('Ext.button.Button', {
        text: 'Login',
        scale: 'medium',
        handler: function () {
            me.doLogin();
        }
    });
    me.buttonRegister = Ext.create('Ext.button.Button', { text: 'Register', scale: 'medium' });
    me.displayForgotPassword = Ext.create('Ext.form.field.Display', {
        name: 'displayForgotPassword',
        value: '<a href="#" class="terms">Forgot Password?</a>',
    });
    me.xform = Ext.create('Ext.form.Panel', {
        title: "Login",
        border: true,
        autoScroll: true,
        bodyPadding: '15',
        width: 550,
        height: 270,
        bodyStyle: 'padding:20px 0;',
        items: [me.username, me.password, me.displayForgotPassword],
        buttons: [me.buttonDoLogin]
    });
    Ext.create('Ext.container.Viewport', {
        layout: 'ux.center',
        items: [me.xform]
    });
});