﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Application.Web.Models;
using System.Dynamic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Application.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly string SESSION_KEY_LOGIN = "KEY_LOGIN";

        public IActionResult Index()
        {
            if (string.IsNullOrEmpty(Request.Cookies[SESSION_KEY_LOGIN]))
            {
                return Redirect("~/Expired");
            }
            return View();
        }
        public ActionResult LoginInfo()
        {
            dynamic result = new ExpandoObject();
            result.user = Request.Cookies["Username"];
            result.success = true;
            result.result = true;
            return Content(JsonConvert.SerializeObject(result, Formatting.Indented, new JavaScriptDateTimeConverter()));
        }
    }
}
