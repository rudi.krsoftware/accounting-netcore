﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Application.Web.Controllers
{
    public class ExpiredController : Controller
    {
        public IActionResult Index()
        {
            Response.Cookies.Delete("KEY_LOGIN");
            Response.Cookies.Delete("Username");
            return View();
        }
    }
}